//
//  cardTableViewCell.swift
//  Dnex
//
//  Created by Arka on 05/02/20.
//  Copyright © 2020 Arka. All rights reserved.
//

import UIKit

class cardTableViewCell: UITableViewCell {

    @IBOutlet weak var outView: UIView!
    
    @IBOutlet weak var cardNumber: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
