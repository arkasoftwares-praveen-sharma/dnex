//
//  sideBarTableViewCell.swift
//  Dnex
//
//  Created by Arka on 28/01/20.
//  Copyright © 2020 Arka. All rights reserved.
//

import UIKit

class sideBarTableViewCell: UITableViewCell {

    @IBOutlet weak var customImg: UIImageView!
    @IBOutlet weak var customLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
