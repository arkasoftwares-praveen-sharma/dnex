//
//  ItemListTableViewCell.swift
//  Dnex
//
//  Created by Arka on 03/02/20.
//  Copyright © 2020 Arka. All rights reserved.
//

import UIKit

class ItemListTableViewCell: UITableViewCell {

    @IBOutlet weak var itemView: UIView!
    @IBOutlet weak var itemImage: UIImageView!
    
    @IBOutlet weak var itemDesc: UILabel!
    @IBOutlet weak var itemName: UILabel!
    @IBOutlet weak var height: UILabel!
    @IBOutlet weak var width: UILabel!
    @IBOutlet weak var weight: UILabel!
    @IBOutlet weak var deleteBtn: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
