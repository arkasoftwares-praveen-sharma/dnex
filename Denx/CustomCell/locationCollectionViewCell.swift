//
//  locationCollectionViewCell.swift
//  Dnex
//
//  Created by Arka on 18/01/20.
//  Copyright © 2020 Arka. All rights reserved.
//

import UIKit

class locationCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var customView: UIView!
    @IBOutlet weak var lbl: UILabel!
    @IBOutlet weak var img: UIImageView!
}
