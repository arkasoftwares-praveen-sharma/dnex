//
//  savedAddressCollectionViewCell.swift
//  Dnex
//
//  Created by Arka on 18/03/20.
//  Copyright © 2020 Arka. All rights reserved.
//

import UIKit

class savedAddressCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var customView: UIView!
    @IBOutlet weak var customBtnOutlet: UIButton!
    @IBOutlet weak var customLbl: UILabel!
    
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var deleteBtn: UIButton!
    
}
