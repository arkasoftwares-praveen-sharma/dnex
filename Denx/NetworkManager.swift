//
//  NetworkManager.swift
//  Currency Converter
//
//  Created by Arka Softwares on 12/12/19.
//  Copyright © 2019 Arka Softwares. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

// import Reachability


class NetworkManager:NSObject {
    
    
    var ResponseStr:String = ""
    static var TokenDict = [String: String]()
    var dictionaryForStatus:[String] = []
    
    static let paypal_sandbox_url:String = "https://api.sandbox.paypal.com/";

    static let PROTOCOL:String = "http://";
//    103.207.168.164:8004/
    static let SUB_DOMAIN:String = "103.207.168.164:8008";
    
//    http://103.207.168.164:8008/api/APINAME
    
    static let DOMAIN:String = "/api/";//Production Service End
    static let API_DIR:String = "";
    
    static let SITE_URL = PROTOCOL + SUB_DOMAIN + DOMAIN;
    static var API_URL = SITE_URL + API_DIR;
    
    // Call Functions
    
    
    // POST
    
    
    static func callService(uiRef: UIViewController,Webservice: String, parameters:Parameters, completion:@escaping (JSON) -> Void){
        
        if Reachability.isConnectedToNetwork(){
 
            Alamofire.request("\(Webservice)",method:.post, parameters:parameters,encoding:JSONEncoding.default).responseJSON { response in
                print("Request: \(String(describing: response.request))")   // original url request
                print("Response: \(String(describing: response.response))") // http url response
                print("Result: \(response.result)")                         // response serialization result
                
                switch response.result{
                    
                case .failure(let error):
                    print("Error : \(error)" )
                    
                    self.showErrorAlert(uiRef: uiRef, message: "Internet connection appears to be offline")
                    //
                    break
                    
                case .success(_):
                    print("Success")
                    break
                }
                
           
                if let json = response.result.value {
                    print("Output: \(json)") // serialized json response
                    let sJSON = JSON(json)
                    
                    
                    if sJSON["status"].intValue == 200 {
                        // we're OK to parse!
                    }
                    else { // Error found
                        print("ErrorSERVICE: ")
                       
                        //                    sJSON["message"].stringValue)
                        
                        //                    print(sJSON["message"].stringValue)
                        
                        //                    NetworkManager.showErrorAlert(uiRef: uiRef,message:  sJSON["message"].stringValue)
                    }
                    completion (sJSON)
                }
                else{
                    CommonFunctions.delay(1.0){
                        GIFHUD.shared.dismiss()
                    }
                }
                
                
                //            NetworkManager.showErrorAlert(uiRef: uiRef,message:  "Something went wrong, Please try again")
            }
        }
            
        else {
           CommonFunctions.delay(1.0){
                     showErrorAlert(uiRef: uiRef, message: "Please check your internet connection")
                }
        }
    }
    
    
       static func callServiceHeader(uiRef: UIViewController,Webservice: String, parameters:Parameters, completion:@escaping (JSON) -> Void){
           
           if Reachability.isConnectedToNetwork(){
    
               Alamofire.request("\(Webservice)",method:.post, parameters:parameters,encoding:JSONEncoding.default,headers: TokenDict).responseJSON { response in
                   print("Request: \(String(describing: response.request))")   // original url request
                   print("Response: \(String(describing: response.response))") // http url response
                   print("Result: \(response.result)")                         // response serialization result
                   
                   switch response.result{
                       
                   case .failure(let error):
                       print("Error : \(error)" )
                       
                       self.showErrorAlert(uiRef: uiRef, message: "Internet connection appears to be offline")
                       //
                       break
                       
                   case .success(_):
                       print("Success")
                       break
                   }
                   
              
                   if let json = response.result.value {
                       print("Output: \(json)") // serialized json response
                       let sJSON = JSON(json)
                       
                       
                       if sJSON["status"].intValue == 200 {
                           // we're OK to parse!
                       }
                       else { // Error found
                           print("ErrorSERVICE: ")
                          
                           //                    sJSON["message"].stringValue)
                           
                           //                    print(sJSON["message"].stringValue)
                           
                           //                    NetworkManager.showErrorAlert(uiRef: uiRef,message:  sJSON["message"].stringValue)
                       }
                       completion (sJSON)
                   }
                   else{
                       CommonFunctions.delay(1.0){
                           GIFHUD.shared.dismiss()
                       }
                   }
                   
                   
                   //            NetworkManager.showErrorAlert(uiRef: uiRef,message:  "Something went wrong, Please try again")
               }
           }
               
           else {
              CommonFunctions.delay(1.0){
                        showErrorAlert(uiRef: uiRef, message: "Please check your internet connection")
                   }
           }
       }
    
    
    static func callServiceAddItem(url:String, parameters:Parameters,profile_pic:UIImage, completion:@escaping (JSON) -> Void){
        
        //        let TokenDict:HTTPHeaders  = ["Content-Type": "application/json"]
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                
                //                if let item:UIImage = profile_pic{
                
                //                guard let imageData = profile_pic.jpegData(compressionQuality: 0.5) else {
                //                    print("Could not get JPEG representation of UIImage")
                //                    return
                //
                //                }
                
                
                let imageData = profile_pic.jpegData(compressionQuality: 0.5)
                
                multipartFormData.append(imageData!, withName: "image",fileName:"photo.jpg" ,mimeType: "image/jpg")
                
                
                for (key, value) in parameters {
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
        }, to:  url, headers:TokenDict) { (encodingResult) in
            switch encodingResult {
            case .success(let upload,_ ,_):
                upload.responseJSON { response in
                    print("Request: \(String(describing: response.request))")   // original url request
                    print("Response: \(String(describing: response.response))") // http url response
                    print("Result: \(response.result.value)")                         // response serialization result
                    
                    
                    if let json = response.result.value {
                        print("Output: \(json)") // serialized json response
                        let sJSON = JSON(json)
                        
                        
                        if sJSON["status"].intValue == 200 {
                            // we're OK to parse!
                        }
                        else { // Error found
                            print("Error: ")
                            //                    sJSON["message"].stringValue)
                            
                            //                    print(sJSON["message"].stringValue)
                            
                            //                    NetworkManager.showErrorAlert(uiRef: uiRef,message:  sJSON["message"].stringValue)
                        }
                        completion (sJSON)
                    }     else {
                        
                        CommonFunctions.delay(1.0){
                            GIFHUD.shared.dismiss()
                        }
                    }
                    
                }
            case .failure(let encodingError):
                completion("Something went wrong!!")
            }
        }
    }
    
    static func callServicePayPal(uiRef: UIViewController,Webservice: String, parameters:Parameters, completion:@escaping (JSON) -> Void){
        if Reachability.isConnectedToNetwork(){
            print(parameters)
            Alamofire.request("\(Webservice)",method:.post, parameters:parameters,encoding:URLEncoding.default,headers: TokenDict).responseJSON { response in
                print("Request: \(String(describing: response.request))")   // original url request
                print("Response: \(String(describing: response.response))") // http url response
                print("Result: \(response.result)")                         // response serialization result
                
                
                if let json = response.result.value {
                    print("Output: \(json)") // serialized json response
                    let sJSON = JSON(json)
                    
                    
                    if sJSON["status_code"].intValue == 200 {
                        // we're OK to parse!
                    }
                    else { // Error found
                        print("Error: ")
                        //                                        sJSON["message"].stringValue)
                        //
                        //                                        print(sJSON["message"].stringValue)
                        
                        //                    NetworkManager.showErrorAlert(uiRef: uiRef,message:  sJSON["message"].stringValue)
                    }
                    completion (sJSON)
                }
                else{
                    CommonFunctions.delay(1.0){
                        GIFHUD.shared.dismiss()
                    }
                    //              NetworkManager.showErrorAlert(uiRef: uiRef,message:  "Something went wrong, Please try again")
                }
                //            NetworkManager.showErrorAlert(uiRef: uiRef,message:  "Something went wrong, Please try again")
            }
        }
        else {
           CommonFunctions.delay(1.0){
                   showErrorAlert(uiRef: uiRef, message: "Please check your internet connection")
                }
           
        }
    }
    
    
    static func callServiceVerify(uiRef: UIViewController,Webservice: String, parameters:Parameters, completion:@escaping (JSON) -> Void){
        if Reachability.isConnectedToNetwork(){
            Alamofire.request("\(Webservice)",method:.post, parameters:parameters,encoding:URLEncoding.default,headers: TokenDict).responseJSON { response in
                print("Request: \(String(describing: response.request))")   // original url request
                print("Response: \(String(describing: response.response))") // http url response
                print("Result: \(response.result)")                         // response serialization result
                
                
                if let json = response.result.value {
                    print("Output: \(json)") // serialized json response
                    let sJSON = JSON(json)
                    
                    
                    if sJSON["status_code"].intValue == 200 {
                        // we're OK to parse!
                    }
                    else { // Error found
                        print("Error: ")
                        //                                        sJSON["message"].stringValue)
                        //
                        //                                        print(sJSON["message"].stringValue)
                        
                        //                    NetworkManager.showErrorAlert(uiRef: uiRef,message:  sJSON["message"].stringValue)
                    }
                    completion (sJSON)
                }
                else{
                    CommonFunctions.delay(1.0){
                        GIFHUD.shared.dismiss()
                    }
                    //              NetworkManager.showErrorAlert(uiRef: uiRef,message:  "Something went wrong, Please try again")
                }
                //            NetworkManager.showErrorAlert(uiRef: uiRef,message:  "Something went wrong, Please try again")
            }
        }
        else {
           CommonFunctions.delay(1.0){
                   showErrorAlert(uiRef: uiRef, message: "Please check your internet connection")
                }
           
        }
    }
    
    
    static func callServiceNew(Webservice: String, parameters:Parameters, completion:@escaping (JSON) -> Void){
        if Reachability.isConnectedToNetwork(){
            Alamofire.request("\(Webservice)",method:.post, parameters:parameters,encoding:URLEncoding.default,headers: TokenDict).responseJSON { response in
                print("Request: \(String(describing: response.request))")   // original url request
                print("Response: \(String(describing: response.response))") // http url response
                print("Result: \(response.result)")                         // response serialization result
                
                
                if let json = response.result.value {
                    print("Output: \(json)") // serialized json response
                    let sJSON = JSON(json)
                    
                    
                    if sJSON["status_code"].intValue == 200 {
                        // we're OK to parse!
                    }
                    else { // Error found
                        print("Error: ")
                        //                                        sJSON["message"].stringValue)
                        //
                        //                                        print(sJSON["message"].stringValue)
                        
                        //                    NetworkManager.showErrorAlert(uiRef: uiRef,message:  sJSON["message"].stringValue)
                    }
                    completion (sJSON)
                }
                else{
                    CommonFunctions.delay(1.0){
                        GIFHUD.shared.dismiss()
                    }
                    //              NetworkManager.showErrorAlert(uiRef: uiRef,message:  "Something went wrong, Please try again")
                }
                //            NetworkManager.showErrorAlert(uiRef: uiRef,message:  "Something went wrong, Please try again")
            }
        }
        else {
           CommonFunctions.delay(1.0){
                    
                }
//               showErrorAlert(uiRef: uiRef, message: "Please check your internet connection")
        }
    }
    
    
    static func callServiceTimeline(url:String, parameters:Parameters,profile_pic:UIImage?, completion:@escaping (JSON) -> Void){
        
        //        let TokenDict:HTTPHeaders  = ["Content-Type": "application/json"]
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                
                //                if let item:UIImage = profile_pic{
                
                //                guard let imageData = profile_pic.jpegData(compressionQuality: 0.5) else {
                //                    print("Could not get JPEG representation of UIImage")
                //                    return
                //
                //                }
                
                
                let imageData = profile_pic?.jpegData(compressionQuality: 0.5)
                
                multipartFormData.append(imageData!, withName: "profile_pic",fileName:"photo.jpg" ,mimeType: "image/jpg")
                
                
                for (key, value) in parameters {
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
        }, to:  url, headers:TokenDict) { (encodingResult) in
            switch encodingResult {
            case .success(let upload,_ ,_):
                upload.responseJSON { response in
                    print("Request: \(String(describing: response.request))")   // original url request
                    print("Response: \(String(describing: response.response))") // http url response
                    print("Result: \(response.result.value)")                         // response serialization result
                    
                    
                    if let json = response.result.value {
                        print("Output: \(json)") // serialized json response
                        let sJSON = JSON(json)
                        
                        
                        if sJSON["status"].intValue == 200 {
                            // we're OK to parse!
                        }
                        else { // Error found
                            print("Error: ")
                            //                    sJSON["message"].stringValue)
                            
                            //                    print(sJSON["message"].stringValue)
                            
                            //                    NetworkManager.showErrorAlert(uiRef: uiRef,message:  sJSON["message"].stringValue)
                        }
                        completion (sJSON)
                    }     else {
                    }
                    
                }
            case .failure(let encodingError):
                completion("Something went wrong!!")
            }
        }
    }
    
    
    // GET
    
    static func callServiceGet(uiRef: UIViewController,Webservice: String, parameters:Parameters, completion:@escaping (JSON) -> Void){
        
        print(TokenDict)
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request("\(Webservice)", method: .get, parameters: parameters, encoding: URLEncoding.default,headers: TokenDict).responseJSON
                {
                    response in
                    print("Request: \(String(describing: response.request))")   // original url request
                    print("Response: \(String(describing: response.response))") // http url response
                    print("Result: \(response.result)")                         // response serialization result
                    
                    switch(response.result)
                    {
                    case .success(let value): break
                    // completionHandler(value as? NSDictionary, nil)
                    case .failure(let error as NSError):
                        //  completionHandler(nil, error as NSError?)
                        print(error)
                          
                        
                        break
                    }
                    
                    if let json = response.result.value {
                        print("Output: \(json)") // serialized json response
                        let sJSON = JSON(json)
                        
                        
                        if sJSON["status"].intValue == 200 {
                            // we're OK to parse!
                        }
                        else { // Error found
                            print("Error: ")
                           
                            //                    sJSON["message"].stringValue)
                            
                            //                    print(sJSON["message"].stringValue)
                            
                            //  NetworkManager.showErrorAlert(uiRef: uiRef,message:  sJSON["message"].stringValue)
                        }
                        
                        completion (sJSON)
                    }
                    else{
                        CommonFunctions.delay(1.0){
                            GIFHUD.shared.dismiss()
                        }
                    }
            }
        }
        else {
            
            CommonFunctions.delay(1.0){
                showErrorAlert(uiRef: uiRef, message: "Please check your internet connection")
            }
            
        }
    }
    
    
    
    
    
    static func callServiceDelete(uiRef: UIViewController,Webservice: String, parameters:Parameters, completion:@escaping (JSON) -> Void){
        
        print(TokenDict)
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request("\(Webservice)", method: .delete, parameters: parameters, encoding: URLEncoding.default,headers: TokenDict).responseJSON
                {
                    response in
                    print("Request: \(String(describing: response.request))")   // original url request
                    print("Response: \(String(describing: response.response))") // http url response
                    print("Result: \(response.result)")                         // response serialization result
                    
                    switch(response.result)
                    {
                    case .success(let value): break
                    // completionHandler(value as? NSDictionary, nil)
                    case .failure(let error as NSError):
                        //  completionHandler(nil, error as NSError?)
                        print(error)
                          
                        
                        break
                    }
                    
                    if let json = response.result.value {
                        print("Output: \(json)") // serialized json response
                        let sJSON = JSON(json)
                        
                        
                        if sJSON["status"].intValue == 200 {
                            // we're OK to parse!
                        }
                        else { // Error found
                            print("Error: ")
                           
                            //                    sJSON["message"].stringValue)
                            
                            //                    print(sJSON["message"].stringValue)
                            
                            //  NetworkManager.showErrorAlert(uiRef: uiRef,message:  sJSON["message"].stringValue)
                        }
                        
                        completion (sJSON)
                    }
                    else{
                        CommonFunctions.delay(1.0){
                            GIFHUD.shared.dismiss()
                        }
                    }
            }
        }
        else {
            
            CommonFunctions.delay(1.0){
                showErrorAlert(uiRef: uiRef, message: "Please check your internet connection")
            }
            
        }
    }
    
    static func login(uiRef: UIViewController,Webservice: String,country_code:String,mobile:String,password:String,completion: @escaping (JSON) -> Void) {
        
        let API_Call = API_URL + Webservice
        
        let parameters: Parameters = ["country_code":country_code,"mobile":mobile,"password":password,"user_type":"2"]
        
        print(API_Call)
        print(parameters)
        
        NetworkManager.callServiceVerify(uiRef:uiRef,Webservice: API_Call, parameters:parameters)  { (JSON) in
            completion(JSON)
        }
    }
    

    
    static func sendOTP(uiRef: UIViewController,Webservice: String,mobile: String,country_code:String,otp_type:String,email:String,user_type:String,completion: @escaping (JSON) -> Void) {
        
        let API_Call = API_URL + Webservice
        
        
        let parameters: Parameters = [
            "mobile": mobile,
            "country_code":country_code,
            "otp_type" : otp_type,
            "email" : email,
            "user_type" : user_type
        ]
        
        print(API_Call)
        print(parameters)
        
        NetworkManager.callService(uiRef:uiRef,Webservice: API_Call, parameters:parameters)  { (JSON) in
            completion(JSON)
        }
    }
    
    
    static func resetPassword(uiRef: UIViewController,Webservice: String,mobile: String,country_code:String,otp:String,password:String,created_at:String,completion: @escaping (JSON) -> Void) {
         
         let API_Call = API_URL + Webservice
         
         
         let parameters: Parameters = [
             "mobile": mobile,
             "country_code":country_code,
             "otp" : otp,
             "password" : password,
             "created_at" : created_at
         ]
         
         print(API_Call)
         print(parameters)
         
         NetworkManager.callService(uiRef:uiRef,Webservice: API_Call, parameters:parameters)  { (JSON) in
             completion(JSON)
         }
     }

    
    static func verifyOTP(uiRef: UIViewController,Webservice: String,mobile: String,country_code:String,otp_type:String,otp:String,created_at:String,completion: @escaping (JSON) -> Void) {
        
        let API_Call = API_URL + Webservice
        
        
        let parameters: Parameters = [
            "mobile": mobile,
            "country_code":country_code,
            "otp_type" : otp_type,
            "otp" : otp,
            "created_at" : created_at
        ]
        
        print(API_Call)
        print(parameters)
        
        NetworkManager.callService(uiRef:uiRef,Webservice: API_Call, parameters:parameters)  { (JSON) in
            completion(JSON)
        }
    }
    
    
    static func signUp(uiRef: UIViewController,Webservice: String,mobile: String,country_code:String,name:String,email:String,password:String,user_type:String,completion: @escaping (JSON) -> Void) {
        
        let API_Call = API_URL + Webservice
        
        
        let parameters: Parameters = [
            "mobile": mobile,
            "country_code":country_code,
            "email" : email,
            "password" : password,
            "user_type" : user_type,
            "name" : name
        ]
        
        print(API_Call)
        print(parameters)
        
        NetworkManager.callService(uiRef:uiRef,Webservice: API_Call, parameters:parameters)  { (JSON) in
            completion(JSON)
        }
    }
    
    static func getProfile(uiRef: UIViewController,Webservice: String,completion: @escaping (JSON) -> Void) {
          
          let API_Call = API_URL + Webservice
          let tokenToSend:String = UserDefaults.standard.string(forKey: "token")!
          
          print(tokenToSend)
          
          TokenDict = ["Authorization": tokenToSend]
          
          let parameters: Parameters = [:]
          
          print(API_Call)
          print(parameters)
          
          NetworkManager.callServiceVerify(uiRef:uiRef,Webservice: API_Call, parameters:parameters)  { (JSON) in
              completion(JSON)
          }
      }
    
    static func changePassword(uiRef: UIViewController,Webservice: String,old_password:String,new_password:String,completion: @escaping (JSON) -> Void) {
            
            let API_Call = API_URL + Webservice
            let tokenToSend:String = UserDefaults.standard.string(forKey: "token")!
            
            print(tokenToSend)
            
            TokenDict = ["Authorization": tokenToSend]
            
            let parameters: Parameters = ["old_password":old_password,
                                          "new_password":new_password]
            
            print(API_Call)
            print(parameters)
            
            NetworkManager.callServiceVerify(uiRef:uiRef,Webservice: API_Call, parameters:parameters)  { (JSON) in
                completion(JSON)
            }
        }
    
    
    static func fetchLocation(uiRef: UIViewController,Webservice: String,order_id:String,completion: @escaping (JSON) -> Void) {
               
               let API_Call = API_URL + Webservice
               let tokenToSend:String = UserDefaults.standard.string(forKey: "token") ?? ""
               
               print(tokenToSend)
               
               TokenDict = ["Authorization": tokenToSend]
               
               let parameters: Parameters = ["order_id":order_id]
               
               print(API_Call)
               print(parameters)
               
               NetworkManager.callServiceVerify(uiRef:uiRef,Webservice: API_Call, parameters:parameters)  { (JSON) in
                   completion(JSON)
               }
           }
      
    
    static func BankDetails(uiRef: UIViewController,Webservice: String,bank_details_id:String?,bank_name:String,account_holder_name:String,account_number:String,iban_number:String,completion: @escaping (JSON) -> Void) {
               
               let API_Call = API_URL + Webservice
               let tokenToSend:String = UserDefaults.standard.string(forKey: "token")!
               
               print(tokenToSend)
               
               TokenDict = ["Authorization": tokenToSend]
               
//        bank_details_id
//        bank_name
//        account_holder_name
//        account_number
//        iban_number
        
        
        let parameters: Parameters = ["bank_details_id":bank_details_id ?? "",
                                        "bank_name":bank_name,"account_holder_name":account_holder_name,
                                             "account_number":account_number,"iban_number":iban_number]
               
               print(API_Call)
               print(parameters)
               
               NetworkManager.callServiceVerify(uiRef:uiRef,Webservice: API_Call, parameters:parameters)  { (JSON) in
                   completion(JSON)
               }
           }
    
    
    
    static func EditProfile(webservice:String,email:String,media:UIImage?,name:String,country_code:String,mobile:String, completion: @escaping (Any) -> Void) {
        
        
        let API_Call = API_URL + webservice
        
        let tokenToSend:String = UserDefaults.standard.string(forKey: "token")!
        
        print(tokenToSend)
        
        TokenDict = ["Authorization": tokenToSend]
        
        
        let parameters: Parameters = [
            "name": name,
            "email" : email,
            "country_code" : country_code,
            "mobile" : mobile
        ]
        
        print(parameters)
        
        
        NetworkManager.callServiceTimeline(url: API_Call, parameters: parameters, profile_pic: media) { (JSON) in
            completion(JSON)
        }
    }
    
    
    static func initiateOrder(uiRef: UIViewController,Webservice: String,type: String,pickup_lat:String,pickup_lng:String,pickup_address:String,pickup_date_time:String,receiver_name:String,receiver_country_code:String,receiver_mobile:String,receiver_lat:String,receiver_lng:String,receiver_address:String,completion: @escaping (JSON) -> Void) {
        
        let API_Call = API_URL + Webservice
        
        let tokenToSend:String = UserDefaults.standard.string(forKey: "token")!
        
        print(tokenToSend)
        
        TokenDict = ["Authorization": tokenToSend]
        
        let parameters: Parameters = [
            "type": type,
            "pickup_lat":pickup_lat,
            "pickup_lng" : pickup_lng,
            "pickup_address" : pickup_address,
            "pickup_date_time" : pickup_date_time,
            "receiver_name" : receiver_name,
            "receiver_country_code" : receiver_country_code,
            "receiver_mobile" : receiver_mobile,
            "receiver_lat" : receiver_lat,
            "receiver_lng" : receiver_lng,
            "receiver_address" : receiver_address
        ]
        
        print(API_Call)
        print(parameters)
        
        NetworkManager.callServiceVerify(uiRef:uiRef,Webservice: API_Call, parameters:parameters)  { (JSON) in
            completion(JSON)
        }
    }
    
    
    static func addItem(webservice:String,order_id:String,name:String,media:UIImage,description:String,height:String,width:String,weight:String, completion: @escaping (Any) -> Void) {
        
        
        let API_Call = API_URL + webservice
        
        let tokenToSend:String = UserDefaults.standard.string(forKey: "token")!
        
        print(tokenToSend)
        
        TokenDict = ["Authorization": tokenToSend]
        
        
        let parameters: Parameters = [
            "order_id": order_id,
            "name" : name,
            "description" : description,
            "height" : height,
            "width" : width,
            "weight" : weight
        ]
        
        print(parameters)
        
        
        NetworkManager.callServiceAddItem(url: API_Call, parameters: parameters, profile_pic: media) { (JSON) in
            completion(JSON)
        }
    }
    
    static func addDropOff(uiRef: UIViewController,webservice:String,order_id:String,lat:String,long:String,address:String, completion: @escaping (Any) -> Void) {
        
        
        let API_Call = API_URL + webservice
        
        let tokenToSend:String = UserDefaults.standard.string(forKey: "token")!
        
        print(tokenToSend)
        
        TokenDict = ["Authorization": tokenToSend]
        
        
        let parameters: Parameters = [
            "order_id": order_id,
            "receiver_lat" : lat,
            "receiver_lng" : long,
            "receiver_address" : address
           
        ]
        
        print(parameters)
        
        
        NetworkManager.callServiceVerify(uiRef:uiRef,Webservice: API_Call, parameters:parameters) { (JSON) in
            completion(JSON)
        }
    }
    
    static func fetchUser(uiRef: UIViewController,webservice:String,order_id:String, completion: @escaping (Any) -> Void) {
          
          
          let API_Call = API_URL + webservice
          
          let tokenToSend:String = UserDefaults.standard.string(forKey: "token")!
          
          print(tokenToSend)
          
          TokenDict = ["Authorization": tokenToSend]
          
          
          let parameters: Parameters = [
              "order_id": order_id
          ]
          
          print(parameters)
          
          
          NetworkManager.callService(uiRef:uiRef,Webservice: API_Call, parameters:parameters) { (JSON) in
              completion(JSON)
          }
      }
    
    
    static func updateOrder(uiRef: UIViewController,webservice:String,order_id:String,delivery_type:String,promo:String,payment_type:String,total:String,sell_amount:String,loyalty_point:String, completion: @escaping (Any) -> Void) {
           
           
           let API_Call = API_URL + webservice
           
           let tokenToSend:String = UserDefaults.standard.string(forKey: "token")!
           
           print(tokenToSend)
           
           TokenDict = ["Authorization": tokenToSend]
           
           
           let parameters: Parameters = [
               "order_id": order_id,
               "delivery_type": delivery_type,
               "coupon_code" : promo,
               "payment_type" : payment_type,
               "amount" : total,
               "sell_amount" : sell_amount,
               "loyality_point" : loyalty_point
           ]
           
           print(parameters)
           
           
           NetworkManager.callServiceVerify(uiRef:uiRef,Webservice: API_Call, parameters:parameters) { (JSON) in
               completion(JSON)
           }
       }
  
    
    static func updateStatus(uiRef: UIViewController,Webservice: String,order_id:String,order_status:Int,comment:String,completion: @escaping (JSON) -> Void) {
        
        let API_Call = API_URL + Webservice
        let tokenToSend:String = UserDefaults.standard.string(forKey: "token")!
        
        print(tokenToSend)
        
        TokenDict = ["Authorization": tokenToSend]
        
        let parameters: Parameters = ["order_id":order_id,
                                      "order_status":order_status,"comment":comment]
        
        print(API_Call)
        print(parameters)
        
        NetworkManager.callServiceVerify(uiRef:uiRef,Webservice: API_Call, parameters:parameters)  { (JSON) in
            completion(JSON)
        }
    }
    
    
    static func fetch_token(uiRef: UIViewController,completion: @escaping (JSON) -> Void) {
           
           let API_Call = paypal_sandbox_url + "v1/oauth2/token"
//           let tokenToSend:String = UserDefaults.standard.string(forKey: "paypal_token")!
//
//           print(tokenToSend)
          
        let user = "ASYSkpgQzGRyQLaw5up35-bcRyQ7F29sCeRQywQMWVXJ8uE9V1zgdmL7jQrEFhEvqz7bNkXbXBX7hbj-"

        let password = "EEClg-27MKgJJ9Dc4Hdlrz5wkIbeNZqN9dIgy2t8NH-dahbs6TgOSMjrhWfZllqZe2r_8-M0SWzjpCXN"
        
        let credentialData = "\(user):\(password)".data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!
        let base64Credentials = credentialData.base64EncodedString()

        
        TokenDict = ["Authorization": "Basic \(base64Credentials)","Content-Type":"application/x-www-form-urlencoded"]
           
           let parameters: Parameters = ["grant_type":"client_credentials"]
           
           print(API_Call)
           print(parameters)
           
//        ,"ASYSkpgQzGRyQLaw5up35-bcRyQ7F29sCeRQywQMWVXJ8uE9V1zgdmL7jQrEFhEvqz7bNkXbXBX7hbj-":"EEClg-27MKgJJ9Dc4Hdlrz5wkIbeNZqN9dIgy2t8NH-dahbs6TgOSMjrhWfZllqZe2r_8-M0SWzjpCXN"
        
           NetworkManager.callServicePayPal(uiRef:uiRef,Webservice: API_Call, parameters:parameters)  { (JSON) in
               completion(JSON)
           }
       }
    
    
    static func save_card(uiRef: UIViewController,card_number:String,month:String,year:String,cvv:String,type:String,first_name:String,last_name:String,completion: @escaping (JSON) -> Void) {
            
            let API_Call = paypal_sandbox_url + "v1/vault/credit-cards/"
            
            
            let tokenToSend:String = UserDefaults.standard.string(forKey: "paypal_token")!
            
            print(tokenToSend)
            
            TokenDict = ["Authorization": "Bearer \(tokenToSend)","Content-Type":"application/json"]
            
        let parameters: Parameters = ["number":card_number,"expire_month":month,"expire_year":year,"cvv2":cvv,"type":type,"first_name":first_name,"last_name":last_name]
            
            print(API_Call)
            print(parameters)
            
            //        ,"ASYSkpgQzGRyQLaw5up35-bcRyQ7F29sCeRQywQMWVXJ8uE9V1zgdmL7jQrEFhEvqz7bNkXbXBX7hbj-":"EEClg-27MKgJJ9Dc4Hdlrz5wkIbeNZqN9dIgy2t8NH-dahbs6TgOSMjrhWfZllqZe2r_8-M0SWzjpCXN"
            
               NetworkManager.callServiceHeader(uiRef:uiRef,Webservice: API_Call, parameters:parameters)  { (JSON) in
                   completion(JSON)
               }
           }
    
    
    static func payment(uiRef: UIViewController,payer:Dictionary<String, Any>,transaction:Array<Any>,completion: @escaping (JSON) -> Void) {
              
              let API_Call = paypal_sandbox_url + "v1/payments/payment"
              
              
              let tokenToSend:String = UserDefaults.standard.string(forKey: "paypal_token")!
              
              print(tokenToSend)
              
              TokenDict = ["Authorization": "Bearer \(tokenToSend)","Content-Type":"application/json"]
              
        let parameters: Parameters = ["intent":"sale","payer":payer,"transactions":transaction]
              
              print(API_Call)
              print(parameters)
              
              //        ,"ASYSkpgQzGRyQLaw5up35-bcRyQ7F29sCeRQywQMWVXJ8uE9V1zgdmL7jQrEFhEvqz7bNkXbXBX7hbj-":"EEClg-27MKgJJ9Dc4Hdlrz5wkIbeNZqN9dIgy2t8NH-dahbs6TgOSMjrhWfZllqZe2r_8-M0SWzjpCXN"
              
                 NetworkManager.callServiceHeader(uiRef:uiRef,Webservice: API_Call, parameters:parameters)  { (JSON) in
                     completion(JSON)
                 }
             }
    
    
    static func fetch_card(uiRef: UIViewController,card_id:String,completion: @escaping (JSON) -> Void) {
        
        let API_Call = paypal_sandbox_url + "v1/vault/credit-cards/\(card_id)"
        let parameters: Parameters = [:]
        
        print(API_Call)
        print(parameters)
        
        let tokenToSend:String = UserDefaults.standard.string(forKey: "paypal_token")!
        
        print(tokenToSend)
        
        TokenDict = ["Authorization": "Bearer \(tokenToSend)","Content-Type":"application/json"]
        
        NetworkManager.callServiceGet(uiRef:uiRef,Webservice: API_Call, parameters:parameters)  { (JSON) in
            completion(JSON)
        }
    }
    
    static func delete_card(uiRef: UIViewController,card_id:String,completion: @escaping (JSON) -> Void) {
        
        let API_Call = paypal_sandbox_url + "v1/vault/credit-cards/\(card_id)"
        let parameters: Parameters = [:]
        
        print(API_Call)
        print(parameters)
        
        let tokenToSend:String = UserDefaults.standard.string(forKey: "paypal_token")!
        
        print(tokenToSend)
        
        TokenDict = ["Authorization": "Bearer \(tokenToSend)","Content-Type":"application/json"]
        
        NetworkManager.callServiceDelete(uiRef:uiRef,Webservice: API_Call, parameters:parameters)  { (JSON) in
            completion(JSON)
        }
    }
 
    static func fetch_notification(uiRef: UIViewController,Webservice:String,completion: @escaping (JSON) -> Void) {
        
        let API_Call = API_URL + Webservice
        let parameters: Parameters = [:]
        
        print(API_Call)
        print(parameters)
        
        let tokenToSend:String = UserDefaults.standard.string(forKey: "token") ?? ""
        
        print(tokenToSend)
        
        TokenDict = ["Authorization": tokenToSend]
        
        NetworkManager.callServiceGet(uiRef:uiRef,Webservice: API_Call, parameters:parameters)  { (JSON) in
            completion(JSON)
        }
    }
    
    static func save_card_id(uiRef: UIViewController,Webservice:String,card_id:String,completion: @escaping (JSON) -> Void) {
        
        let API_Call = API_URL + Webservice
        let parameters: Parameters = ["card_id":card_id]
        
        print(API_Call)
        print(parameters)
        
        let tokenToSend:String = UserDefaults.standard.string(forKey: "token")!
        
        print(tokenToSend)
        
         TokenDict = ["Authorization": tokenToSend]
        
        NetworkManager.callServiceVerify(uiRef:uiRef,Webservice: API_Call, parameters:parameters)  { (JSON) in
            completion(JSON)
        }
    }
    
    static func save_address(uiRef: UIViewController,Webservice:String,address_type:String,address:String,latitude:String,longitude:String,city:String,state:String,country:String,pincode:String,address_id:String,completion: @escaping (JSON) -> Void) {
        
        let API_Call = API_URL + Webservice
        let parameters: Parameters = ["address_type":address_type,"address":address,"latitude":latitude,"longitude":longitude,"city":city,"state":state,"country":country,"pincode":pincode,"address_id":address_id]
        
        print(API_Call)
        print(parameters)
        
        let tokenToSend:String = UserDefaults.standard.string(forKey: "token")!
        
        print(tokenToSend)
        
         TokenDict = ["Authorization": tokenToSend]
        
        NetworkManager.callServiceVerify(uiRef:uiRef,Webservice: API_Call, parameters:parameters)  { (JSON) in
            completion(JSON)
        }
    }
    
    
    static func update_device_id(uiRef: UIViewController,Webservice:String,device_token:String,completion: @escaping (JSON) -> Void) {
           
           let API_Call = API_URL + Webservice
           let parameters: Parameters = ["device_token":device_token]
           
           print(API_Call)
           print(parameters)
           
           let tokenToSend:String = UserDefaults.standard.string(forKey: "token")!
           
           print(tokenToSend)
           
           TokenDict = ["Authorization": tokenToSend]
           
           NetworkManager.callServiceVerify(uiRef:uiRef,Webservice: API_Call, parameters:parameters)  { (JSON) in
               completion(JSON)
           }
       }
    
    static func fetch_address(uiRef: UIViewController,Webservice:String,completion: @escaping (JSON) -> Void) {
        
        let API_Call = API_URL + Webservice
        let parameters: Parameters = [:]
        
        print(API_Call)
        print(parameters)
        
        let tokenToSend:String = UserDefaults.standard.string(forKey: "token")!
        
        print(tokenToSend)
        
        TokenDict = ["Authorization": tokenToSend]
        
        NetworkManager.callServiceVerify(uiRef:uiRef,Webservice: API_Call, parameters:parameters)  { (JSON) in
            completion(JSON)
        }
    }
    
    static func fetch_card_id(uiRef: UIViewController,Webservice:String,completion: @escaping (JSON) -> Void) {
           
           let API_Call = API_URL + Webservice
           let parameters: Parameters = [:]
           
           print(API_Call)
           print(parameters)
           
           let tokenToSend:String = UserDefaults.standard.string(forKey: "token")!
           
           print(tokenToSend)
           
           TokenDict = ["Authorization": tokenToSend]
           
           NetworkManager.callServiceVerify(uiRef:uiRef,Webservice: API_Call, parameters:parameters)  { (JSON) in
               completion(JSON)
           }
       }
    
    static func RemoveItem(uiRef: UIViewController,Webservice: String,product_id : String,completion: @escaping (JSON) -> Void) {
        
        let API_Call = API_URL + Webservice
        
        let tokenToSend:String = UserDefaults.standard.string(forKey: "token")!
        
        print(tokenToSend)
        
        TokenDict = ["Authorization": tokenToSend]
        
        
        let parameters: Parameters = ["product_id": product_id]
        
        print(API_Call)
        print(parameters)
        
        NetworkManager.callServiceVerify(uiRef:uiRef,Webservice: API_Call, parameters:parameters)  { (JSON) in
            completion(JSON)
        }
    }
    
    static func RemoveAddress(uiRef: UIViewController,Webservice: String,address_id : String,completion: @escaping (JSON) -> Void) {
        
        let API_Call = API_URL + Webservice
        
        let tokenToSend:String = UserDefaults.standard.string(forKey: "token")!
        
        print(tokenToSend)
        
        TokenDict = ["Authorization": tokenToSend]
        
        
        let parameters: Parameters = ["address_id": address_id]
        
        print(API_Call)
        print(parameters)
        
        NetworkManager.callServiceVerify(uiRef:uiRef,Webservice: API_Call, parameters:parameters)  { (JSON) in
            completion(JSON)
        }
    }
    
    
    static func applyPromo(uiRef: UIViewController,Webservice: String,coupon_code : String,completion: @escaping (JSON) -> Void) {
         
         let API_Call = API_URL + Webservice
         
         let tokenToSend:String = UserDefaults.standard.string(forKey: "token")!
         
         print(tokenToSend)
         
         TokenDict = ["Authorization": tokenToSend]
         
         
         let parameters: Parameters = ["coupon_code": coupon_code]
         
         print(API_Call)
         print(parameters)
         
         NetworkManager.callServiceVerify(uiRef:uiRef,Webservice: API_Call, parameters:parameters)  { (JSON) in
             completion(JSON)
         }
     }
    
    static func myOrders(uiRef: UIViewController,Webservice: String,completion: @escaping (JSON) -> Void) {
            
            let API_Call = API_URL + Webservice
            
            let tokenToSend:String = UserDefaults.standard.string(forKey: "token")!
            
            print(tokenToSend)
            
            TokenDict = ["Authorization": tokenToSend]
            
            
            let parameters: Parameters = [:]
            
            print(API_Call)
            print(parameters)
            
            NetworkManager.callServiceVerify(uiRef:uiRef,Webservice: API_Call, parameters:parameters)  { (JSON) in
                completion(JSON)
            }
        }
    
    static func rating(uiRef: UIViewController,Webservice: String,order_id:String,shipper_id:String,rating:String,feedback:String,completion: @escaping (JSON) -> Void) {
          
          let API_Call = API_URL + Webservice
          let tokenToSend:String = UserDefaults.standard.string(forKey: "token")!
          
          print(tokenToSend)
          
          TokenDict = ["Authorization": tokenToSend]
          
          let parameters: Parameters = ["order_id":order_id,
                                        "shipper_id":shipper_id, "rating":rating,"feedback":feedback]
          
          print(API_Call)
          print(parameters)
          
          NetworkManager.callServiceVerify(uiRef:uiRef,Webservice: API_Call, parameters:parameters)  { (JSON) in
              completion(JSON)
          }
      }
    
    static func receipt(uiRef: UIViewController,Webservice: String,order_id:String,completion: @escaping (JSON) -> Void) {
             
             let API_Call = API_URL + Webservice
             let tokenToSend:String = UserDefaults.standard.string(forKey: "token")!
             
             print(tokenToSend)
             
             TokenDict = ["Authorization": tokenToSend]
             
             let parameters: Parameters = ["order_id":order_id]
             
             print(API_Call)
             print(parameters)
             
             NetworkManager.callServiceVerify(uiRef:uiRef,Webservice: API_Call, parameters:parameters)  { (JSON) in
                 completion(JSON)
             }
         }
    

    
    static func showErrorAlert(uiRef: UIViewController, message :String){
        //Alert Message Code
        let alert = UIAlertController(title: "Message", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {action in
            
            
            
        }))
        
        
        uiRef.present(alert, animated: true, completion: nil)
        //End Alert Message Code
    }
}
