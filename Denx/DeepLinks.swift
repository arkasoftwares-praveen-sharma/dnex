//
//  DeepLinks.swift
//  DeepLinking
//
//




/// Represents presenting an image to the user.
/// Example - demoapp://show/profile?code=cat
struct ShowOrderId: DeepLink {
	static let template = DeepLinkTemplate()
		.term("open")
		.term("order")
		.queryStringParameters([
			.requiredString(named: "code"),
			])
	
	init(values: DeepLinkValues) {
		code = values.query["code"] as! String
	}
	
	let code: String
}


