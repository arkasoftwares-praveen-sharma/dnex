//
//  AppDelegate.swift
//  Denx
//
//  Created by Arka on 07/01/20.
//  Copyright © 2020 Arka. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import GoogleMaps
import GooglePlaces
import Firebase
import UserNotifications

@available(iOS 13.0, *)
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate {
 
    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        IQKeyboardManager.shared.enable = true
//        IQKeyboardManager.shared.
         GMSServices.provideAPIKey("AIzaSyDOCjcy-rgsNIbBNtcMXqfIIGxqtLTlAr8")
        let activityDictionary = launchOptions?[UIApplication.LaunchOptionsKey.userActivityDictionary] as? [AnyHashable: Any] ?? [AnyHashable: Any]()
        let activity = activityDictionary ["UIApplicationLaunchOptionsUserActivityKey"] as? NSUserActivity ?? nil
        
        if let finalString = activity?.webpageURL?.absoluteString {
//            remoteParmNumTxt = finalString
          
            print(finalString)
        }
        
//        AIzaSyA8ULcLzfoc3hPDqkqeMuhSWzoqNdVYO54
//        AIzaSyA5WNkRhEkykRaSppo0xjU-4x8Bx3DFeD4
        
        
//        AIzaSyDOCjcy-rgsNIbBNtcMXqfIIGxqtLTlAr8
         
        GMSPlacesClient.provideAPIKey("AIzaSyDOCjcy-rgsNIbBNtcMXqfIIGxqtLTlAr8")
        GMSServices.provideAPIKey("AIzaSyDOCjcy-rgsNIbBNtcMXqfIIGxqtLTlAr8")
        
        
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        
        application.registerForRemoteNotifications()

        
        let token = UserDefaults.standard.string(forKey: "sideIcon") ?? ""

        if token == ""{

            let walk = UserDefaults.standard.string(forKey: "walk") ?? ""
            if walk == ""{

                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let initialViewController = storyboard.instantiateViewController(withIdentifier: "walk")

                self.window?.rootViewController = initialViewController
                self.window?.makeKeyAndVisible()
            }
            else{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let initialViewController = storyboard.instantiateViewController(withIdentifier: "splash")

                self.window?.rootViewController = initialViewController
                self.window?.makeKeyAndVisible()
            }
        }
        else{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let initialViewController = storyboard.instantiateViewController(withIdentifier: "Home")

            self.window?.rootViewController = initialViewController
            self.window?.makeKeyAndVisible()
        }
        
        
        return true
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        application.applicationIconBadgeNumber = 0
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
         print(url)
        return true
    }

    
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        print("Continue User Activity: ")
        if userActivity.activityType == NSUserActivityTypeBrowsingWeb {
            let url = userActivity.webpageURL!
            print(url.absoluteString)
            //handle url
        }
        return true
    }
    
    
    
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        print("inside")
        guard let url = userActivity.webpageURL else { return false }
        
        // Create a recognizer with this app's custom deep link types.
        let recognizer = DeepLinkRecognizer(deepLinkTypes: [
            ShowOrderId.self,
          ])
        
        // Try to create a deep link object based on the URL.
        guard let deepLink = recognizer.deepLink(matching: url, filter: "sisu.arkasoftwares.co") else {
            print("Unable to match URL: \(url.absoluteString)")
            return false
        }
        
        // Navigate to the view or content specified by the deep link.
        switch deepLink {
       
        case let code as ShowOrderId:
            
            UserDefaults.standard.set(code.code, forKey: "order_id")
            
            let token = UserDefaults.standard.string(forKey: "token") ?? ""
            
            if token == ""{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let initialViewController = storyboard.instantiateViewController(withIdentifier: "splash")
                
                self.window?.rootViewController = initialViewController
                self.window?.makeKeyAndVisible()
            }
            else{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let initialViewController = storyboard.instantiateViewController(withIdentifier: "Home")
                
                self.window?.rootViewController = initialViewController
                self.window?.makeKeyAndVisible()
            }

            
            print(code.code)
                        
            return true
            
        default:
            print("Unsupported DeepLink: \(type(of: deepLink))")
            return false
        }
    }
    
    
    // the FCM registration token.
       func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
           print("APNs token retrieved: \(deviceToken)")
           
           print(String(decoding: deviceToken, as: UTF8.self))
           
           
           InstanceID.instanceID().instanceID { (result, error) in
               if let error = error {
                   print("Error fetching remote instange ID: \(error)")
               } else if let result = result {
                   print("Remote instance ID token: \(result.token)")
                      
               }
           }
           
           
           // With swizzling disabled you must set the APNs token here.
           // Messaging.messaging().apnsToken = deviceToken
       }
    
   

    // when App is active
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        
        //        playSound()
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        // Change this to your preferred presentation option
        completionHandler(.alert)
    }
    
    // when App is in Background
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo as Dictionary<AnyHashable, Any>
        // Print message ID.
        
        
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        
        
        //        let UserIDPush:String = (userInfo["gcm.notification.user_id"] as? String)!
        let Type:String = (userInfo["gcm.notification.type"] as? String)!
        //        let profileImg:String = (userInfo["gcm.notification.avatar"] as? String)!
        //        let name:String = (userInfo["gcm.notification.name"] as? String)!
        if Type == "order_status"{
            let OrderIDPush:String = (userInfo["gcm.notification.order_id"] as? String) ?? ""
            let ShipperIDPush:String = (userInfo["gcm.notification.user_id"] as? String) ?? ""
            UserDefaults.standard.set(OrderIDPush, forKey: "order") //setObject
            UserDefaults.standard.set(ShipperIDPush, forKey: "shipper_id") //setObject
            
            let status:String = (userInfo["gcm.notification.order_status"] as? String)!
            
            if status == "Completed"{
               
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let initialViewController = storyboard.instantiateViewController(withIdentifier: "receipt")
                self.window?.rootViewController = initialViewController
                self.window?.makeKeyAndVisible()
            
            }
            else{
            
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let initialViewController = storyboard.instantiateViewController(withIdentifier: "track")
                self.window?.rootViewController = initialViewController
                self.window?.makeKeyAndVisible()
            }
            
        }
        
        completionHandler()
    }
}

//@available(iOS 13, *)
//extension AppDelegate : UNUserNotificationCenterDelegate {
//
//
//    // when App is active
//
//    // Receive displayed notifications for iOS 10 devices.
//    func userNotificationCenter(_ center: UNUserNotificationCenter,
//                                willPresent notification: UNNotification,
//                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
//        let userInfo = notification.request.content.userInfo
//
//
//        //        playSound()
//        // With swizzling disabled you must let Messaging know about the message, for Analytics
//        // Messaging.messaging().appDidReceiveMessage(userInfo)
//        // Print message ID.
//        if let messageID = userInfo[gcmMessageIDKey] {
//            print("Message ID: \(messageID)")
//        }
//
//        // Print full message.
//        print(userInfo)
//
//
////           let UserIDPush:String = (userInfo["gcm.notification.user_id"] as? String)!
//                let Type:String = (userInfo["gcm.notification.type"] as? String)!
//        //        let profileImg:String = (userInfo["gcm.notification.avatar"] as? String)!
//        //        let name:String = (userInfo["gcm.notification.name"] as? String)!
//                if Type == "order_status"{
//                    let OrderIDPush:String = (userInfo["gcm.notification.order_id"] as? String)!
//                    UserDefaults.standard.set(OrderIDPush, forKey: "order") //setObject
//
//
//                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                    let initialViewController = storyboard.instantiateViewController(withIdentifier: "track")
//
//                    self.window?.rootViewController = initialViewController
//                    self.window?.makeKeyAndVisible()
//
//
//                }
//
//        // Change this to your preferred presentation option
//        completionHandler([])
//    }
//
//    // when App is in Background
//
//    func userNotificationCenter(_ center: UNUserNotificationCenter,
//                                didReceive response: UNNotificationResponse,
//                                withCompletionHandler completionHandler: @escaping () -> Void) {
//        let userInfo = response.notification.request.content.userInfo as Dictionary<AnyHashable, Any>
//        // Print message ID.
//
//
//        if let messageID = userInfo[gcmMessageIDKey] {
//            print("Message ID: \(messageID)")
//        }
//
//        // Print full message.
//        print(userInfo)
//
//
//
////        let UserIDPush:String = (userInfo["gcm.notification.user_id"] as? String)!
//        let Type:String = (userInfo["gcm.notification.type"] as? String)!
////        let profileImg:String = (userInfo["gcm.notification.avatar"] as? String)!
////        let name:String = (userInfo["gcm.notification.name"] as? String)!
//        if Type == "order_status"{
//            let OrderIDPush:String = (userInfo["gcm.notification.order_id"] as? String)!
//            UserDefaults.standard.set(OrderIDPush, forKey: "order") //setObject
//
//
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let initialViewController = storyboard.instantiateViewController(withIdentifier: "track")
//
//            self.window?.rootViewController = initialViewController
//            self.window?.makeKeyAndVisible()
//
//
//        }
//
////        UserDefaults.standard.set(UserIDPush, forKey: "userIdNotification") //setObject
////        UserDefaults.standard.set(Type, forKey: "typeNotification") //setObject
////        UserDefaults.standard.set(profileImg, forKey: "profileNotification") //setObject
////        UserDefaults.standard.set(name, forKey: "nameNotification") //setObject
////        UserDefaults.standard.set("push", forKey: "push") //setObject
////
////        print(Type)
////
////        if Type == "chat"{
////            let storyboard = UIStoryboard(name: "Main", bundle: nil)
////            let initialViewController = storyboard.instantiateViewController(withIdentifier: "chatDetail")
////
////            self.window?.rootViewController = initialViewController
////            self.window?.makeKeyAndVisible()
////
////        }
////
////
////
////        else{
////            let storyboard = UIStoryboard(name: "Main", bundle: nil)
////            let initialViewController = storyboard.instantiateViewController(withIdentifier: "notification")
////
////            self.window?.rootViewController = initialViewController
////            self.window?.makeKeyAndVisible()
////        }
//
//        completionHandler()
//    }
//}


@available(iOS 13.0, *)
extension AppDelegate : MessagingDelegate {
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        
        
        //        UserDefaults.standard.set(fcmToken, forKey: "Token") //setObject
        
        
        let dataDict:[String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    // [END refresh_token]
    // [START ios_10_data_message]
    // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
    // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
    // [END ios_10_data_message]
    
    

}
