//
//  notificationViewController.swift
//  Dnex
//
//  Created by Arka on 16/03/20.
//  Copyright © 2020 Arka. All rights reserved.
//

import UIKit
import SwiftyJSON

class notificationViewController: UIViewController {

    @IBOutlet weak var headerView: UIView!
    
    var message:[String] = []
    var time:[String] = []
    
    @IBOutlet weak var customTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        headerView.setShadow()
        
        
        NetworkManager.fetch_notification(uiRef: self, Webservice: "notification"){sJson in
            
            let sJSON = JSON(sJson)
            print(sJSON)
            
            self.message.removeAll()
            self.time.removeAll()
            
            if sJSON["status_code"].intValue == 200{
                for item in sJSON["data"].arrayValue{
                    let new_message = item["description"].stringValue
                    let time = item["time_ago"].stringValue
                    self.message.append(new_message)
                    self.time.append(time)
                }
                self.customTable.reloadData()
            }
            else{
                CommonFunctions.showErrorAlert(uiRef: self, message: sJSON["message"].stringValue)
            }
        }
    }
    
    @IBAction func backBtnClicked(_
        sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
}

extension notificationViewController:UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return message.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CELL") as! notificationTableViewCell
        
        cell.selectionStyle = .none
        cell.customView.layer.cornerRadius = 10
        cell.customView.layer.borderColor = UIColor().HexToColor(hexString: "97a8ab", alpha: 0.5).cgColor
        cell.customView.layer.borderWidth = 1
        
        cell.messageLbl.text = message[indexPath.row]
        cell.timeLbl.text = time[indexPath.row]
        return cell
    }
}
