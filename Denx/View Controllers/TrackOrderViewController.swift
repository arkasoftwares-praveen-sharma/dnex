//
//  TrackOrderViewController.swift
//  Dnex
//
//  Created by Praveen Sharma on 03/04/20.
//  Copyright © 2020 Arka. All rights reserved.
//

import UIKit
import GoogleMaps
import SwiftyJSON


class TrackOrderViewController: UIViewController,CLLocationManagerDelegate,GMSMapViewDelegate {
    @IBOutlet weak var mapView: GMSMapView!
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var recenterOutlet: UIButton!
    
    @IBOutlet weak var imageImg: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var rateBtn: UIButton!
    @IBOutlet weak var vehicle_no: UILabel!
    
    @IBOutlet weak var rightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var customView: UIView!
    @IBOutlet weak var itemView: UIView!
    @IBOutlet weak var cancelView: UIView!
    @IBOutlet weak var customTable: UITableView!
    
    @IBOutlet weak var cancelOrderView: UIView!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    
    
    
    @IBOutlet weak var send: UIButton!
    @IBOutlet weak var tooFar: UIButton!
    @IBOutlet weak var mistake: UIButton!
    @IBOutlet weak var other: UIButton!
    
    @IBOutlet weak var messageTxt: ACFloatingTextfield!
    @IBOutlet weak var cancelHeight: NSLayoutConstraint!
    
    @IBOutlet weak var cancelBtnOutlet: UIButton!
    
    @IBOutlet weak var receiver_name: UILabel!
    
    @IBOutlet weak var receiver_mobile: UILabel!
    
    
    @IBOutlet weak var receiver_Initials: UILabel!
    
    @IBOutlet weak var receiver_View: UIView!
    
    
    var reason = ""
    
    let locationManager = CLLocationManager()
    var destination: CLLocationCoordinate2D = CLLocationCoordinate2D()
    var receiver_loc: CLLocationCoordinate2D = CLLocationCoordinate2D()
    var sender_loc: CLLocationCoordinate2D = CLLocationCoordinate2D()
    var previous_loc: CLLocationCoordinate2D = CLLocationCoordinate2D()
    
    
    let marker = GMSMarker()
    let destMarker = GMSMarker()
    var dragged = ""
    
    var order_id = ""
    var sJSON = JSON()
    var TimerToFetchLocation = Timer()
    var item:[item_order] = []
    var shipper_number = ""
    var gif = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        receiver_Initials.layer.cornerRadius = receiver_Initials.frame.height/2
        receiver_Initials.clipsToBounds = true
        
         gif = UserDefaults.standard.string(forKey: "new_order") ?? ""
         UserDefaults.standard.removeObject(forKey: "new_order")
        
        if order_id == ""{
            let orderPush = UserDefaults.standard.string(forKey: "order") ?? ""
            self.order_id = orderPush
           
        }
        
        
        receiver_View.layer.cornerRadius = 5
        receiver_View.layer.borderColor = UIColor().HexToColor(hexString: "97a8ab", alpha: 0.5).cgColor
        receiver_View.layer.borderWidth = 1
        
        cancelBtnOutlet.layer.cornerRadius = cancelBtnOutlet.frame.height/2
        cancelHeight.constant = 420
           GIFHUD.shared.setGif(named: "loader.gif")
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            //            locationManager.startUpdatingLocation()
        }
        else{
            
            CommonFunctions.showErrorAlert(uiRef: self, message: "Please enable the location services from device settings".localized)
            
        }
        
        
        self.imageImg.layer.cornerRadius = self.imageImg.frame.height/2
        self.imageImg.clipsToBounds = true
        
        
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        // Do any additional setup after loading the view.
        headerView.setShadow()
        mapView.delegate = self
        //        submitBtnOutlet.layer.cornerRadius = submitBtnOutlet.frame.height/2
        //        mapView.isMyLocationEnabled = true
        //        mapView.settings.myLocationButton = true
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
        
        recenterOutlet.isHidden = true
        
        mapView.addSubview(recenterOutlet)

        
//           order_id = "290"
        TimerToFetchLocation = Timer.scheduledTimer(withTimeInterval: 10.0, repeats: true, block: { (_) in
            print("TIMER")
            DispatchQueue.global(qos: .background).async {
                print("This is run on the background queue")
                
                print(self.order_id)
                
                NetworkManager.fetchLocation(uiRef: self, Webservice: "get-shipper-location", order_id: self.order_id){sJson in
                    self.sJSON = JSON(sJson)
                    
                }
                DispatchQueue.main.async {
                    print("This is run on the main queue, after the previous code in outer block")
                    
                    if self.sJSON["status"].intValue == 200{
                        let Lat = self.sJSON["data"]["shipper_latitude"].stringValue
                        let Long = self.sJSON["data"]["shipper_longitude"].stringValue
                        
                        let status = self.sJSON["data"]["order_status"].stringValue
                        
                        let receiver_Lat = self.sJSON["data"]["receiver_latitude"].stringValue
                        let receiver_Long = self.sJSON["data"]["receiver_longitude"].stringValue
                        
                        
                        
                        if Lat == ""{
                            CommonFunctions.showErrorAlert(uiRef: self, message: "We are unable to track the shipper at the moment, Please try later !!".localized)
                        }
                        else{
                            
                            if status == "2"{
                                
                                self.locationManager.stopUpdatingLocation()
                                
                                self.previous_loc = self.destination
                                
                                self.destination.latitude = Double(Lat)!
                                self.destination.longitude = Double(Long)!
                                
                                
                                self.destMarker.position = self.destination
                                self.destMarker.rotation = self.locationManager.location?.course ?? 0
                                self.destMarker.icon = UIImage (named: "truck")
                                self.destMarker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
                                self.destMarker.rotation = CLLocationDegrees(self.getHeadingForDirection(fromCoordinate: self.previous_loc, toCoordinate: self.destination))
                                self.destMarker.map = self.mapView
                                
                                
                                self.receiver_loc.latitude = Double(receiver_Lat)!
                                self.receiver_loc.longitude = Double(receiver_Long)!
                                
                                self.marker.position = self.receiver_loc
                                self.drawPath()
                                
                                self.rightConstraint.constant = 25
                                
                            }
                            else{
                                
                                
                                self.previous_loc = self.destination
                                self.destination.latitude = Double(Lat)!
                                self.destination.longitude = Double(Long)!
                                
                                
                                self.destMarker.position = self.destination
                                self.destMarker.rotation = self.locationManager.location?.course ?? 0
                                self.destMarker.icon = UIImage (named: "truck")
                                self.destMarker.rotation = CLLocationDegrees(self.getHeadingForDirection(fromCoordinate: self.previous_loc, toCoordinate: self.destination))
                                self.destMarker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
                                self.destMarker.map = self.mapView
                                
                                
                                
                            }
                            
                            
                            
                            if self.dragged == ""{
                                
                                
                                var bounds = GMSCoordinateBounds()
                                bounds = bounds.includingCoordinate(self.marker.position)
                                bounds = bounds.includingCoordinate(self.destMarker.position)
                                //                                let camera: GMSCameraUpdate = GMSCameraUpdate.fit(bounds)
                                let cameraWithPadding: GMSCameraUpdate = GMSCameraUpdate.fit(bounds, withPadding: 50)
                                
                                let camera = self.mapView.camera(for: bounds, insets: UIEdgeInsets())!
                                
                                self.mapView.camera = camera
                                
                                self.mapView.moveCamera(cameraWithPadding)
                                
                                self.drawPath()
                            }
                            
                        }
                        
                        
                    }
                    else{
                        //                CommonFunctions.showErrorAlert(uiRef: self, message: self.sJSON["message"].stringValue)
                    }
                }
            }
            

            
            
        })
        
     
     //   GIFHUD.shared.show(withOverlay: true, duration: 2.0)
        
        NetworkManager.fetchLocation(uiRef: self, Webservice: "get-shipper-location", order_id: order_id){sJson in
            
            CommonFunctions.delay(1.0) {
                let sJSON = JSON(sJson)
                print(sJSON)
                if sJSON["status"].intValue == 200{
                    let Lat = sJSON["data"]["shipper_latitude"].stringValue
                    let Long = sJSON["data"]["shipper_longitude"].stringValue
                    
                    let status = sJSON["data"]["order_status"].stringValue
                    
                    let receiver_Lat = sJSON["data"]["receiver_latitude"].stringValue
                    let receiver_Long = sJSON["data"]["receiver_longitude"].stringValue
                
                    self.nameLbl.text = sJSON["data"]["shipper_details"]["name"].stringValue
                    
                    let image = "http://103.207.168.164:8008/storage/\(sJSON["data"]["shipper_details"]["profile_pic"].stringValue)"
                    self.imageImg.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "defaultProfile"))
                    self.vehicle_no.text = sJSON["data"]["vehicle_details"]["vehicle_no"].stringValue
                    self.rateBtn.setTitle("  \(sJSON["data"]["shipper_rating"].stringValue)", for: .normal)
                    
                    self.rateBtn.layer.cornerRadius = self.rateBtn.frame.height/2
                    self.shipper_number = sJSON["data"]["shipper_details"]["country_code"].stringValue + sJSON["data"]["shipper_details"]["mobile"].stringValue
                  
                    print(self.shipper_number)
                    
                    self.receiver_name.text = sJSON["data"]["order_data"]["receiver_name"].stringValue
                    self.receiver_mobile.text = sJSON["data"]["order_data"]["receiver_country_code"].stringValue + sJSON["data"]["order_data"]["receiver_mobile"].stringValue
                    
                    let sender_lat = sJSON["data"]["order_data"]["pickup_lat"].stringValue
                    let sender_long = sJSON["data"]["order_data"]["pickup_lng"].stringValue
                    
                    self.sender_loc.latitude = Double(sender_lat)!
                    self.sender_loc.longitude = Double(sender_long)!
                    
                    self.marker.position = self.sender_loc
                    self.marker.map = self.mapView
                    
                    if self.receiver_name.text!.contains(" "){
                        let initials = self.receiver_name.text!.components(separatedBy: " ").reduce("") { ($0 == "" ? "" : "\($0.first!)") + "\($1.first!)" }
                        
                        self.receiver_Initials.text = initials
                    }
                    else{
                        let initials = String(self.receiver_name.text!.prefix(1))
                        
                        self.receiver_Initials.text = initials
                    }
                    
                  
                    
                    for item_detail in sJSON["data"]["order_details"].arrayValue{
                        
                        let image = "http://103.207.168.164:8008/storage/\(item_detail["image"].stringValue)"
                        let name = item_detail["name"].stringValue
                        let height = "\(item_detail["height"].stringValue) cm"
                        let width = "\(item_detail["width"].stringValue) cm"
                        let weight = "\(item_detail["weight"].stringValue) kg"
                        let description_item = item_detail["description"].stringValue
                        
                        self.item.append(item_order.init(name: name, description: description_item, height: height, weight: weight, width: width, item_image:image ))
                    }
                    
                    
                    let constantHeight:CGFloat = CGFloat((124 * self.item.count) + 300)
                    print(constantHeight)

                    if constantHeight > self.view.frame.size.height - 50{
                        self.heightConstraint.constant = self.view.frame.size.height - 50
                    }
                    else{
                        self.heightConstraint.constant = constantHeight
                    }
                    
                    self.customTable.reloadData()
                    
                    if Lat == ""{
                        CommonFunctions.showErrorAlert(uiRef: self, message: "We are unable to track the shipper at the moment, Please try later !!".localized)
                    }
                    else{
                        
                        if status == "2"{
                            
                            self.locationManager.stopUpdatingLocation()
                            
                            self.previous_loc = self.destination
                            
                            self.destination.latitude = Double(Lat)!
                            self.destination.longitude = Double(Long)!
                            
                            
                            self.destMarker.position = self.destination
                            self.destMarker.rotation = self.locationManager.location?.course ?? 0
                            self.destMarker.icon = UIImage (named: "truck")
                            self.destMarker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
                            self.destMarker.rotation = CLLocationDegrees(self.getHeadingForDirection(fromCoordinate: self.previous_loc, toCoordinate: self.destination))
                            self.destMarker.map = self.mapView
                            
                            
                            self.receiver_loc.latitude = Double(receiver_Lat) ?? 0.0
                            self.receiver_loc.longitude = Double(receiver_Long) ?? 0.0
                            
                            self.marker.position = self.receiver_loc
                            self.drawPath()
                            
                            self.rightConstraint.constant = 25
                            
                        }
                        else{
                            
                            
                            self.previous_loc = self.destination
                            self.destination.latitude = Double(Lat)!
                            self.destination.longitude = Double(Long)!
                            
                            
                            self.destMarker.position = self.destination
                            self.destMarker.rotation = self.locationManager.location?.course ?? 0
                            self.destMarker.icon = UIImage (named: "truck")
                            self.destMarker.rotation = CLLocationDegrees(self.getHeadingForDirection(fromCoordinate: self.previous_loc, toCoordinate: self.destination))
                            self.destMarker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
                            self.destMarker.map = self.mapView
                        }
                        
                        
                        
                        if self.dragged == ""{
                            
                            
                            var bounds = GMSCoordinateBounds()
                            bounds = bounds.includingCoordinate(self.marker.position)
                            bounds = bounds.includingCoordinate(self.destMarker.position)
                            //                                let camera: GMSCameraUpdate = GMSCameraUpdate.fit(bounds)
                            let cameraWithPadding: GMSCameraUpdate = GMSCameraUpdate.fit(bounds, withPadding: 50)
                            
                            let camera = self.mapView.camera(for: bounds, insets: UIEdgeInsets())!
                            
                            self.mapView.camera = camera
                            
                            self.mapView.moveCamera(cameraWithPadding)
                            
                            self.drawPath()
                        }
                        
                    }
                    
                    
                    
                    
                    
                }
                else{
//                    CommonFunctions.showErrorAlert(uiRef: self, message: sJSON["message"].stringValue)
                }
                
            }
        }
          
    }
   
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        
        // if GoogleMap installed
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
            
            if let url = URL(string: "comgooglemaps://?saddr=&daddr=\(marker.position.latitude),\(marker.position.longitude)&directionsmode=driving") {
                UIApplication.shared.open(url, options: [:])
            }
            
        } else {
            
            //Open in browser
            if let urlDestination = URL.init(string: "https://www.google.co.in/maps/dir/?saddr=&daddr=\(marker.position.latitude),\(marker.position.longitude)&directionsmode=driving") {
                UIApplication.shared.open(urlDestination)
            }
            
           }
        return true
    }
    
    @IBAction func dontSend(_ sender: Any) {

            send.setImage(UIImage (named: "radioselect"), for: .normal)
            tooFar.setImage(UIImage (named: "radio"), for: .normal)
            mistake.setImage(UIImage (named: "radio"), for: .normal)
            other.setImage(UIImage (named: "radio"), for: .normal)

            messageTxt.isHidden = true
            cancelHeight.constant = 420
        
        reason = "Don't want to send"
            
        }
        
        @IBAction func tooFar(_ sender: Any) {
            
            send.setImage(UIImage (named: "radio"), for: .normal)
            tooFar.setImage(UIImage (named: "radioselect"), for: .normal)
            mistake.setImage(UIImage (named: "radio"), for: .normal)
            other.setImage(UIImage (named: "radio"), for: .normal)
            
            messageTxt.isHidden = true
            cancelHeight.constant = 420
            
            reason = "Delivery time too far"
        }
        
        @IBAction func mistakeBtn(_ sender: Any) {
            
            send.setImage(UIImage (named: "radio"), for: .normal)
            tooFar.setImage(UIImage (named: "radio"), for: .normal)
            mistake.setImage(UIImage (named: "radioselect"), for: .normal)
            other.setImage(UIImage (named: "radio"), for: .normal)
            
            messageTxt.isHidden = true
            cancelHeight.constant = 420
            
            reason = "Ordered by Mistake"
        }
        
        @IBAction func otherBtn(_ sender: Any) {
            
            send.setImage(UIImage (named: "radio"), for: .normal)
            tooFar.setImage(UIImage (named: "radio"), for: .normal)
            mistake.setImage(UIImage (named: "radio"), for: .normal)
            other.setImage(UIImage (named: "radioselect"), for: .normal)
            
            messageTxt.isHidden = false
            cancelHeight.constant = 490
            
            reason = "other"
        }
    
    @IBAction func itemInfoBtnClicked(_ sender: Any) {
        
        self.customView.isHidden = false
        self.itemView.isHidden = false
        self.customView.alpha = 0.0
        //          forgotEmailTxt.text = ""
        UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseInOut, animations: {
            self.customView.alpha = 1.0
        })
        
    }
    
    
    @IBAction func cancelBtnClicked(_ sender: Any) {
           
           self.cancelOrderView.isHidden = true
           self.customView.isHidden = true
           
        if reason == "other" && messageTxt.text == ""{
            CommonFunctions.showErrorAlert(uiRef: self, message: "Enter the reason of cancellation !!".localized)
        }
        else{
            
            NetworkManager.updateStatus(uiRef: self, Webservice: "update-order-status", order_id: order_id, order_status: 4,comment: reason){sJson in
                
                let sJSON = JSON(sJson)
                print(sJSON)
                
                if sJSON["status"].intValue == 200{
                    NotificationCenter.default.post(name: .order, object: nil)
                    self.showAlert(message: "Your order is cancelled !!".localized)
                }
                else{
                    CommonFunctions.showErrorAlert(uiRef: self, message: sJSON["message"].stringValue)
                }
            }
            
        }
        
//           self.dismiss(animated: true, completion: nil)
           
       }
    
    func showAlert( message: String) {
        let alertController = UIAlertController(title: "Message".localized, message:
            message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: {action in
            
             self.dismiss(animated: true, completion: nil)
            
        }))
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    @IBAction func callBtnAction(_ sender: Any) {
           

         guard let number = URL(string: "tel://" + shipper_number) else { return }
           UIApplication.shared.open(number)
           
       }
    
    @IBAction func cancelBtnAction(_ sender: Any) {
           
           self.customView.isHidden = false
           self.cancelOrderView.isHidden = false
           
           self.customView.alpha = 0.0
           self.cancelOrderView.alpha = 0.0
           
           UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseInOut, animations: {
            
               self.customView.alpha = 1.0
               self.cancelOrderView.alpha = 1.0
               
           })
       }
    
    
    @IBAction func closeBtnClicked(_ sender: Any) {
           customView.isHidden = true
        self.cancelOrderView.isHidden = true
        
        messageTxt.text = ""
        send.setImage(UIImage (named: "radioselect"), for: .normal)
        tooFar.setImage(UIImage (named: "radio"), for: .normal)
        mistake.setImage(UIImage (named: "radio"), for: .normal)
        other.setImage(UIImage (named: "radio"), for: .normal)
        
        messageTxt.isHidden = true
        cancelHeight.constant = 420
        
       }
    
    
    func drawPath()
    {
        let origin = "\(destMarker.position.latitude),\(destMarker.position.longitude)"
        let destination = "\(marker.position.latitude),\(marker.position.longitude)"
        
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=AIzaSyDOCjcy-rgsNIbBNtcMXqfIIGxqtLTlAr8"
        
        //        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=AIzaSyDOCjcy-rgsNIbBNtcMXqfIIGxqtLTlAr8"
        print(url)
        let task = URLSession.shared.dataTask(with: URL(string: url)!) { (data, response, error) -> Void in
            
            do {
                if data != nil {
                    let dic = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableLeaves) as!  [String:AnyObject]
                    print(dic)
                    
                    let status = dic["status"] as! String
                    var routesArray:String!
                    if status == "OK" {
                        routesArray = (((dic["routes"]!as! [Any])[0] as! [String:Any])["overview_polyline"] as! [String:Any])["points"] as! String
                        //                            print("routesArray: \(String(describing: routesArray))")
                        DispatchQueue.main.async {
                            
                            let path = GMSPath.init(fromEncodedPath: routesArray!)
                            let singleLine = GMSPolyline.init(path: path)
                            singleLine.strokeWidth = 2.0
                            singleLine.strokeColor = UIColor().HexToColor(hexString: "00BAE3")
                            singleLine.map = self.mapView
                            
//                            if self.dragged == ""{
//                                 var bounds = GMSCoordinateBounds()
//                                bounds = bounds.includingCoordinate(self.marker.position)
//                                bounds = bounds.includingCoordinate(self.destMarker.position)
//                                //                                let camera: GMSCameraUpdate = GMSCameraUpdate.fit(bounds)
//                                let cameraWithPadding: GMSCameraUpdate = GMSCameraUpdate.fit(bounds, withPadding: 50.0)
//
//                                self.mapView.animate(with: cameraWithPadding)
//                            }
                        }
                    }
                }
            } catch {
                print("Error")
            }
        }
        
        task.resume()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.TimerToFetchLocation.invalidate()
    }
    
     @IBAction func recenterBtnClicked(_ sender: Any) {
            
        recenterOutlet.isHidden = true
        var bounds = GMSCoordinateBounds()
        bounds = bounds.includingCoordinate(self.marker.position)
        bounds = bounds.includingCoordinate(self.destMarker.position)
        //                                let camera: GMSCameraUpdate = GMSCameraUpdate.fit(bounds)
        let cameraWithPadding: GMSCameraUpdate = GMSCameraUpdate.fit(bounds, withPadding: 50)
        
        let camera = self.mapView.camera(for: bounds, insets: UIEdgeInsets())!
        
        self.mapView.camera = camera
        
        self.mapView.moveCamera(cameraWithPadding)
        dragged = ""
        
    }
    
    
    
        @IBAction func closeItemBtnClicked(_ sender: Any) {
            
            self.itemView.alpha = 1.0
            //          forgotEmailTxt.text = ""
            UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseInOut, animations: {
                self.itemView.alpha = 0.0
                self.itemView.isHidden = true
                self.customView.isHidden = true
            })
    //        self.itemView.isHidden = true
        }
    
    @IBAction func backBtnClicked(_ sender: Any) {
        let orderPush = UserDefaults.standard.string(forKey: "order") ?? ""
        
        if orderPush != "" || gif != ""{
           
            self.performSegue(withIdentifier: "home", sender: nil)
        }
        else{
            self.dismiss(animated: true, completion: nil)
        }
    }
    

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations[0]
        
        //            let marker = GMSMarker()
      
        
        
//        if dragged == ""{
//            var bounds = GMSCoordinateBounds()
//            bounds = bounds.includingCoordinate(self.marker.position)
//            bounds = bounds.includingCoordinate(self.destMarker.position)
//            //                                let camera: GMSCameraUpdate = GMSCameraUpdate.fit(bounds)
//            let cameraWithPadding: GMSCameraUpdate = GMSCameraUpdate.fit(bounds, withPadding: 50)
//
//            let camera = self.mapView.camera(for: bounds, insets: UIEdgeInsets())!
//
//            self.mapView.camera = camera
//
//            self.mapView.moveCamera(cameraWithPadding)
//        }
        
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        if (gesture){
            print("dragged")
            recenterOutlet.isHidden = false
            dragged = "yes"
        }
    }
    
    func getHeadingForDirection(fromCoordinate fromLoc: CLLocationCoordinate2D, toCoordinate toLoc: CLLocationCoordinate2D) -> Float {

        let fLat: Float = Float((fromLoc.latitude).degreesToRadians)
        let fLng: Float = Float((fromLoc.longitude).degreesToRadians)
        let tLat: Float = Float((toLoc.latitude).degreesToRadians)
        let tLng: Float = Float((toLoc.longitude).degreesToRadians)
        let degree: Float = (atan2(sin(tLng - fLng) * cos(tLat), cos(fLat) * sin(tLat) - sin(fLat) * cos(tLat) * cos(tLng - fLng))).radiansToDegrees
        if degree >= 0 {
            return degree
        }
        else {
            return 360 + degree
        }
    }
}
extension Int {
    var degreesToRadians: Double { return Double(self) * .pi / 180 }
}
extension FloatingPoint {
    var degreesToRadians: Self { return self * .pi / 180 }
    var radiansToDegrees: Self { return self * 180 / .pi }
}
extension TrackOrderViewController:UITableViewDataSource,UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       print(item.count)
            return item.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CELL") as! ItemListTableViewCell
        
        cell.itemView.layer.cornerRadius = 10
        cell.itemView.layer.borderColor = UIColor().HexToColor(hexString: "97a8ab", alpha: 0.5).cgColor
        cell.itemView.layer.borderWidth = 1
        
        cell.itemImage.layer.cornerRadius = 10
        cell.itemImage.layer.borderColor = UIColor().HexToColor(hexString: "97a8ab", alpha: 0.5).cgColor
        cell.itemImage.layer.borderWidth = 1
        
            cell.itemName.text = item[indexPath.row].name
            cell.itemDesc.text = item[indexPath.row].description
            cell.height.text = item[indexPath.row].height
            cell.width.text = item[indexPath.row].width
            cell.weight.text = item[indexPath.row].weight
            
//            cell.itemImage.image = item[indexPath.row].item_image
        
        let image = "\(item[indexPath.row].item_image)"
        cell.itemImage.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "item_placeholder"))
        
        
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 124
        
    }
    

}
