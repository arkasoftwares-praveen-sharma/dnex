//
//  gifViewController.swift
//  Dnex
//
//  Created by Praveen Sharma on 06/04/20.
//  Copyright © 2020 Arka. All rights reserved.
//

import UIKit
import SwiftGifOrigin
import SwiftyJSON


class gifViewController: UIViewController {

    @IBOutlet weak var customImgView: UIImageView!
    
    var order_id = ""
    var timer = Timer()
    var sJSON = JSON()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        UserDefaults.standard.set("gif", forKey: "new_order")
        
        customImgView.image = UIImage.gif(name: "Search_loader")
        
        timer = Timer.scheduledTimer(withTimeInterval: 10.0, repeats: true, block: { (_) in
            print("TIMER")
            DispatchQueue.global(qos: .background).async {
                print("This is run on the background queue")
                
                print(self.order_id)
                
                NetworkManager.fetchLocation(uiRef: self, Webservice: "get-shipper-location", order_id: self.order_id){sJson in
                    self.sJSON = JSON(sJson)
                    
                }
            }
            
            if self.sJSON["status"].intValue == 200{
                
                self.performSegue(withIdentifier: "success", sender: nil)
            }
            else{
//                CommonFunctions.showErrorAlert(uiRef: self, message: self.sJSON["message"].stringValue)
            }
            
        })
        
        Timer.scheduledTimer(withTimeInterval: 300.0, repeats: false, block: { (_) in
                  print("TIMER")
            
            self.showAlert(message: "No shipper available at the moment !!".localized)
            
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "success"{
            let Object = segue.destination as! TrackOrderViewController
            Object.order_id = order_id
        }
    }
    
    func showAlert( message: String) {
        let alertController = UIAlertController(title: "Message".localized, message:
            message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Retry".localized, style: .default, handler: {action in
            
            self.timer = Timer.scheduledTimer(withTimeInterval: 10.0, repeats: true, block: { (_) in
                print("TIMER")
                DispatchQueue.global(qos: .background).async {
                    print("This is run on the background queue")
                    
                    
                    NetworkManager.fetchLocation(uiRef: self, Webservice: "get-shipper-location", order_id: self.order_id){sJson in
                        self.sJSON = JSON(sJson)
                        
                    }
                      DispatchQueue.main.async {
                        
                        if self.sJSON["status"].intValue == 200{
                            self.performSegue(withIdentifier: "success", sender: nil)
                        }
                        else{
                            CommonFunctions.showErrorAlert(uiRef: self, message: self.sJSON["message"].stringValue)
                        }
                    }
                }
            })
            
            
            
            Timer.scheduledTimer(withTimeInterval: 300.0, repeats: false, block: { (_) in
                print("TIMER")
                
                self.timer.invalidate()
                self.showAlert(message: "No shipper available at the moment !!".localized)
                
            })
            
        }))
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: .default, handler: {action in
            
            NetworkManager.updateStatus(uiRef: self, Webservice: "update-order-status", order_id: self.order_id, order_status: 4,comment: "No shipper available"){sJson in
                
                let sJSON = JSON(sJson)
                print(sJSON)
                
                if sJSON["status"].intValue == 200{
//                    self.showAlert(message: "Your order is cancelled !!")
                    
                    self.timer.invalidate()
                    self.performSegue(withIdentifier: "home", sender: nil)
                    
                }
                else{
                    CommonFunctions.showErrorAlert(uiRef: self, message: sJSON["message"].stringValue)
                }
            }
            
        }))
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    
}
