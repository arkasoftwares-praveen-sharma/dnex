//
//  SideBarViewController.swift
//  Dnex
//
//  Created by Arka on 28/01/20.
//  Copyright © 2020 Arka. All rights reserved.
//

import UIKit

class SideBarViewController: UIViewController {

    @IBOutlet weak var langBtnOutlet: UIButton!
    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var sideBtn: UIButton!
    @IBOutlet weak var sideProfileImg: UIImageView!

//,"Saved Addresses"
    
    var lblArr:[String] = ["Home", "My Order","Payment Card","Saved Addresses","Change Password","Help & FAQ","Terms & Privacy Policy","Contact Us","Logout"]
    
    var ImgArr:[String] = ["Home", "MyOrder","payment","saved","changePassword","help","terms","contact","logout"]

    
    
    var segueArr:[String] = ["home", "myOrder","payment","saved","changePassword","link","link","link"]

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
         sideProfileImg.layer.cornerRadius = sideProfileImg.frame.height/2
        
        langBtnOutlet.layer.cornerRadius = langBtnOutlet.frame.height/2
        langBtnOutlet.clipsToBounds = true
        
        langBtnOutlet.layer.borderColor = UIColor.white.cgColor
        langBtnOutlet.layer.borderWidth = 1
        
        let lang = UserDefaults.standard.string(forKey: "language") ?? ""
        
        if lang == "ar" {
            langBtnOutlet.setTitle("عربى", for: .normal)
           
        }
        else{
            langBtnOutlet.setTitle("English", for: .normal)
            
        }
        
        if self.revealViewController() != nil {
            
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            sideBtn.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            //            self.revealViewController().rearViewRevealWidth = self.view.frame.width
            
            self.view.frame.size.width = self.revealViewController().rearViewRevealWidth
            
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        nameLbl.text = UserDefaults.standard.string(forKey: "name")
        let image = UserDefaults.standard.string(forKey: "profile_pic") ?? ""
        self.sideProfileImg.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "defaultProfile"))

        self.view.frame.size.width = self.revealViewController().rearViewRevealWidth
        
        self.view.endEditing(true)
        
    }
    
    
    @IBAction func lang_BtnClicked(_ sender: Any) {
        
//        var lang = ""
        var lang = UserDefaults.standard.string(forKey: "language") ?? ""
              
           
        if lang == "ar"{
          
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            lang = "en"
            
        }
        else{
            
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            lang = "ar"

        }
        
         UserDefaults.standard.set(lang, forKey: "language")
     
        
        // Code for changing language to Arabic
        
        // This is done so that network calls now have the Accept-Language as "hi" (Using Alamofire) Check if you can remove these
        UserDefaults.standard.set([lang], forKey: "AppleLanguages")
        UserDefaults.standard.synchronize()
        
        // Update the language by swaping bundle
        Bundle.setLanguage(lang)
        
        
        
        //        Language.language = Language.german
        // Done to reintantiate the storyboards instantly
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let initialViewController = storyboard.instantiateViewController(withIdentifier: "Home")
        UIApplication.shared.windows[0].rootViewController = initialViewController
    }
    
   
    
}

extension SideBarViewController:UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lblArr.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CELL") as! sideBarTableViewCell
        
        if indexPath.row == lblArr.count - 1{
            cell.backgroundColor = UIColor.white
            cell.customLbl.textColor = UIColor().HexToColor(hexString: "00BAE3")
        }
        
        cell.customLbl.text = lblArr[indexPath.row].localized
        cell.customImg.image = UIImage (named: ImgArr[indexPath.row])
        cell.selectionStyle = .none
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == lblArr.count - 1{
            self.showAlertSuccess(message: "Are you sure you want to logout ?".localized)
        }
        else if segueArr[indexPath.row] != ""{
            
            if segueArr[indexPath.row] == "link"{
                
                var linktosave = ""
                
                if indexPath.row == 5{
                   linktosave = "http://103.207.168.164:8008/page/help-faq"
                }
                else if indexPath.row == 6{
                   linktosave = "http://103.207.168.164:8008/page/terms-privacy-policy"
                }
                else{
                    linktosave = "http://103.207.168.164:8008/page/contact-us"
                }
                
                UserDefaults.standard.set(linktosave, forKey: "link")
                
                let title = lblArr[indexPath.row]
                UserDefaults.standard.set(title, forKey: "title")
            }
            
            
            self.performSegue(withIdentifier: segueArr[indexPath.row], sender: nil)
        }
        
    }
    
    func showAlertSuccess( message: String) {
        let alertController = UIAlertController(title: "Message".localized, message:
            message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Yes".localized, style: .default, handler: {action in
            
            
            UserDefaults.standard.removeObject(forKey: "token")
            UserDefaults.standard.removeObject(forKey: "userID")
            UserDefaults.standard.removeObject(forKey: "sideIcon")

            //userID
            
            self.performSegue(withIdentifier: "logout", sender: nil)
        }))
        alertController.addAction(UIAlertAction(title: "No".localized, style: .default, handler: {action in
        }))
        self.present(alertController, animated: true, completion: nil)
        
    }
}
