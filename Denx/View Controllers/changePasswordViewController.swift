//
//  changePasswordViewController.swift
//  Dnex Shipper
//
//  Created by Arka on 17/03/20.
//  Copyright © 2020 Arka. All rights reserved.
//

import UIKit
import SwiftyJSON

class changePasswordViewController: UIViewController {

    @IBOutlet weak var submitOutlet: UIButton!
    @IBOutlet weak var sideBtn: UIButton!
    
    @IBOutlet weak var oldTxt: ACFloatingTextfield!
    @IBOutlet weak var newTxt: ACFloatingTextfield!
    @IBOutlet weak var confirmTxt: ACFloatingTextfield!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
//        if self.revealViewController() != nil {
//
//            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
//            sideBtn.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
//            self.revealViewController().rearViewRevealWidth = self.view.frame.width
//        }
        submitOutlet.layer.cornerRadius = submitOutlet.frame.height/2
        
    }
    
    
    @IBAction func submitBtnClicked(_ sender: Any) {
        
        if oldTxt.text == ""{
            CommonFunctions.showErrorAlert(uiRef: self, message: "Enter old password !!".localized)
        }
        else if newTxt.text == ""{
            CommonFunctions.showErrorAlert(uiRef: self, message: "Enter new password !!".localized)
        }
        else if confirmTxt.text == ""{
            CommonFunctions.showErrorAlert(uiRef: self, message: "Re-Enter password !!".localized)
        }
        else if confirmTxt.text != newTxt.text{
            confirmTxt.text = ""
            CommonFunctions.showErrorAlert(uiRef: self, message: "Passwords donot matched, please try again !!".localized)
        }
        else{
            NetworkManager.changePassword(uiRef: self, Webservice: "change-password", old_password: oldTxt.text!, new_password: newTxt.text!){sJson in
                
                let sJSON = JSON(sJson)
                print(sJSON)
                
                if sJSON["status"].intValue == 200{
                    self.showAlertSuccess(message: "Password updated successfully !!".localized)
                }
                else{
                    CommonFunctions.showErrorAlert(uiRef: self, message: sJSON["message"].stringValue)
                }
            }
        }
    }
    
    func showAlertSuccess( message: String) {
        let alertController = UIAlertController(title: "Message".localized, message:
                message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: {action in
                self.performSegue(withIdentifier: "homeSegue", sender: nil)
            }))
            self.present(alertController, animated: true, completion: nil)
            
        }
       
    
}
