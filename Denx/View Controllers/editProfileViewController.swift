//
//  editProfileViewController.swift
//  Dnex Shipper
//
//  Created by Arka on 17/03/20.
//  Copyright © 2020 Arka. All rights reserved.
//

import UIKit
import ADCountryPicker
import SwiftyJSON

class editProfileViewController: UIViewController,UITextFieldDelegate,ADCountryPickerDelegate {
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var profileBtnOutlet: UIButton!
    @IBOutlet weak var updateBtnOutlet: UIButton!
    
    
    @IBOutlet weak var nameTxt: ACFloatingTextfield!
    @IBOutlet weak var emailTxt: ACFloatingTextfield!
    @IBOutlet weak var mobileTxt: ACFloatingTextfield!
    @IBOutlet weak var codeTxt: ACFloatingTextfield!
    
    
    var nameStr = ""
    var emailStr = ""
    var mobileStr = ""
    var codeStr = ""
    var profile:UIImage = UIImage()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        
//        let countryDictionary = ["AF":"93", "AL":"355", "DZ":"213","AS":"1", "AD":"376", "AO":"244", "AI":"1","AG":"1","AR":"54","AM":"374","AW":"297","AU":"61","AT":"43","AZ":"994","BS":"1","BH":"973","BD":"880","BB":"1","BY":"375","BE":"32","BZ":"501","BJ":"229","BM":"1","BT":"975","BA":"387","BW":"267","BR":"55","IO":"246","BG":"359","BF":"226","BI":"257","KH":"855","CM":"237","CA":"1","CV":"238","KY":"345","CF":"236","TD":"235","CL":"56","CN":"86","CX":"61","CO":"57","KM":"269","CG":"242","CK":"682","CR":"506","HR":"385","CU":"53","CY":"537","CZ":"420","DK":"45","DJ":"253","DM":"1","DO":"1","EC":"593","EG":"20","SV":"503","GQ":"240","ER":"291","EE":"372","ET":"251","FO":"298","FJ":"679","FI":"358","FR":"33","GF":"594","PF":"689","GA":"241","GM":"220","GE":"995","DE":"49","GH":"233","GI":"350","GR":"30","GL":"299","GD":"1","GP":"590","GU":"1","GT":"502","GN":"224","GW":"245","GY":"595","HT":"509","HN":"504","HU":"36","IS":"354","IN":"91","ID":"62","IQ":"964","IE":"353","IL":"972","IT":"39","JM":"1","JP":"81","JO":"962","KZ":"77","KE":"254","KI":"686","KW":"965","KG":"996","LV":"371","LB":"961","LS":"266","LR":"231","LI":"423","LT":"370","LU":"352","MG":"261","MW":"265","MY":"60","MV":"960","ML":"223","MT":"356","MH":"692","MQ":"596","MR":"222","MU":"230","YT":"262","MX":"52","MC":"377","MN":"976","ME":"382","MS":"1","MA":"212","MM":"95","NA":"264","NR":"674","NP":"977","NL":"31","AN":"599","NC":"687","NZ":"64","NI":"505","NE":"227","NG":"234","NU":"683","NF":"672","MP":"1","NO":"47","OM":"968","PK":"92","PW":"680","PA":"507","PG":"675","PY":"595","PE":"51","PH":"63","PL":"48","PT":"351","PR":"1","QA":"974","RO":"40","RW":"250","WS":"685","SM":"378","SA":"966","SN":"221","RS":"381","SC":"248","SL":"232","SG":"65","SK":"421","SI":"386","SB":"677","ZA":"27","GS":"500","ES":"34","LK":"94","SD":"249","SR":"597","SZ":"268","SE":"46","CH":"41","TJ":"992","TH":"66","TG":"228","TK":"690","TO":"676","TT":"1","TN":"216","TR":"90","TM":"993","TC":"1","TV":"688","UG":"256","UA":"380","AE":"971","GB":"44","US":"1", "UY":"598","UZ":"998", "VU":"678", "WF":"681","YE":"967","ZM":"260","ZW":"263","BO":"591","BN":"673","CC":"61","CD":"243","CI":"225","FK":"500","GG":"44","VA":"379","HK":"852","IR":"98","IM":"44","JE":"44","KP":"850","KR":"82","LA":"856","LY":"218","MO":"853","MK":"389","FM":"691","MD":"373","MZ":"258","PS":"970","PN":"872","RE":"262","RU":"7","BL":"590","SH":"290","KN":"1","LC":"1","MF":"590","PM":"508","VC":"1","ST":"239","SO":"252","SJ":"47","SY":"963","TW":"886","TZ":"255","TL":"670","VE":"58","VN":"84","VG":"284","VI":"340"]
//
//
//        if let countryCodeGet = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
//            print(countryCodeGet)
//            codeTxt.text = "+" + (countryDictionary[countryCodeGet] ?? "")
//        }
        
//        [textView setContentInset:UIEdgeInsetsMake(-8, -8, -8, -8)];
//CGRectMake(0, 0, 15, self.nameTxt.frame.height
//        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: self.nameTxt.frame.height))
//        nameTxt.leftView = paddingView
//        nameTxt.leftViewMode = UITextField.ViewMode.always
//        
        updateBtnOutlet.layer.cornerRadius = updateBtnOutlet.frame.height/2
        profileBtnOutlet.layer.cornerRadius = profileBtnOutlet.frame.height/2
        profileImg.layer.cornerRadius = profileImg.frame.height/2

        profileBtnOutlet.layer.borderWidth = 5
        profileBtnOutlet.layer.borderColor = UIColor().HexToColor(hexString: "FFFFFF", alpha: 0.8).cgColor
        
        codeTxt.delegate = self
        codeTxt.isEnabled = false
        
        print(nameStr)
        
        nameTxt.text = nameStr
        emailTxt.text = emailStr
        codeTxt.text = codeStr
        mobileTxt.text = mobileStr
        profileImg.image = profile
    }
    
    func countryPicker(_ picker: ADCountryPicker, didSelectCountryWithName name: String, code: String, dialCode: String) {
        print(name)
        codeTxt.text = dialCode
        self.dismiss(animated: true, completion: nil)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
//            if textField == codeTxt{
//                codeTxt.endEditing(true)
//                let picker = ADCountryPicker()
//                picker.delegate = self
//
//                let pickerNavigationController = UINavigationController(rootViewController: picker)
//                self.present(pickerNavigationController, animated: true, completion: nil)
//            }
        }
    
    
    @IBAction func backBtnClicked(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
    @IBAction func uploadBtnClicked(_ sender: Any) {
         
        let alert = UIAlertController(title: "Choose Image".localized, message: nil, preferredStyle: .actionSheet)
         alert.addAction(UIAlertAction(title: "Camera".localized, style: .default, handler: { _ in
             self.openCamera()
         }))
         
        alert.addAction(UIAlertAction(title: "Gallery".localized, style: .default, handler: { _ in
             self.openGallery()
         }))
         
        alert.addAction(UIAlertAction.init(title: "Cancel".localized, style: .cancel, handler: nil))
         
         self.present(alert, animated: true, completion: nil)
         
     }
     
     
     
     func openCamera()
          {
              //        ANLoader.showLoading("Please wait", disableUI: false)
              
              if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
                  let imagePicker = UIImagePickerController()
                  imagePicker.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
                  imagePicker.sourceType = UIImagePickerController.SourceType.camera
                  imagePicker.allowsEditing = true
                  self.present(imagePicker, animated: true, completion: nil)
              }
              else
              {
                  let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
                  alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                  self.present(alert, animated: true, completion: nil)
              }
          }
          
          func openGallery()
          {
      //        ANLoader.showLoading("Please wait", disableUI: false)
              
              if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
                  let imagePicker = UIImagePickerController()
                  imagePicker.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
                  imagePicker.allowsEditing = true
                  imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
                  self.present(imagePicker, animated: true, completion: nil)
                  
                  
              }
              else
              {
                let alert  = UIAlertController(title: "Warning".localized, message: "You don't have permission to access gallery.".localized, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
                  self.present(alert, animated: true, completion: nil)
              }
          }
     
     
     @IBAction func submitBtnClicked(_ sender: Any) {
         
         if nameTxt.text == ""{
            CommonFunctions.showErrorAlert(uiRef: self, message: "Enter name !!".localized)
         }
         else if emailTxt.text == ""{
            CommonFunctions.showErrorAlert(uiRef: self, message: "Enter email id !!".localized)
         }
         else if mobileTxt.text == ""{
            CommonFunctions.showErrorAlert(uiRef: self, message: "Enter mobile number !!".localized)
         }
         else{
             self.view.endEditing(true)
            GIFHUD.shared.show(withOverlay: true, duration: 2)
             NetworkManager.EditProfile(webservice: "update-profile", email: emailTxt.text!, media: profileImg.image ?? nil, name: nameTxt.text!, country_code: codeTxt.text!, mobile: mobileTxt.text!){sJson in
                 GIFHUD.shared.dismiss()
                 let sJSON = JSON(sJson)
                 print(sJSON)
                 CommonFunctions.delay(1.0){
                     GIFHUD.shared.dismiss()
                     if sJSON["status"].intValue == 200{
                         
                         let name = sJSON["data"]["name"].stringValue
                         UserDefaults.standard.set(name, forKey: "name")
                         UserDefaults.standard.set("http://103.207.168.164:8008/storage/" + sJSON["data"]["profile_pic"].stringValue, forKey: "profile_pic")

                        self.showAlertSuccess(message: "Profile updated sucessfully".localized)
                         
                     }
                     else{
                         CommonFunctions.showErrorAlert(uiRef: self, message: sJSON["message"].stringValue)
                     }
                 }
             }
         }
         
     }
    
    
    
    
     func showAlertSuccess( message: String) {
        let alertController = UIAlertController(title: "Message".localized, message:
             message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: {action in
             NotificationCenter.default.post(name: .getProfile, object: nil)
             self.dismiss(animated: true, completion: nil)
             
         }))
         self.present(alertController, animated: true, completion: nil)
         
     }
    
}


extension Notification.Name {
   
    static let getProfile = Notification.Name("getProfile")
    
}


extension editProfileViewController:UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        dismiss(animated: true, completion: nil)
        //        ANLoader.showLoading("Please wait", disableUI: false)
        print(info)
        
        //        let pickedImg = info[.originalImage] as? UIImage
        //
        //        ProfileImg.image = pickedImg
        
        if let pickedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
           
   
            profileImg.image = pickedImage
            

            
        }
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
//        ANLoader.hide()
    }
    
  
}

