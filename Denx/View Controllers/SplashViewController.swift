//
//  SplashViewController.swift
//  Dnex
//
//  Created by Arka on 07/01/20.
//  Copyright © 2020 Arka. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {

    @IBOutlet weak var customView: UIView!
    @IBOutlet weak var bottom: NSLayoutConstraint!
    @IBOutlet weak var loginOutlet: UIButton!
    @IBOutlet weak var signUpOutlet: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        customView.layer.cornerRadius = customView.frame.height / 2
        loginOutlet.layer.cornerRadius = loginOutlet.frame.height / 2
        signUpOutlet.layer.cornerRadius = signUpOutlet.frame.height / 2
        
        self.customView.alpha = 0.0
        
        UIView.animate(withDuration: 1.0, delay: 0.5, options: .curveEaseInOut, animations: {
            self.customView.alpha = 1.0
        })
    }

    
    @IBAction func loginbtnClicked(_ sender: Any) {
      
        UIView.animate(withDuration: 0.5, animations: {
           
            self.loginOutlet.backgroundColor = UIColor().HexToColor(hexString: "00bae3", alpha: 0.2)
            self.signUpOutlet.backgroundColor = UIColor.clear
            
            self.loginOutlet.setTitleColor(UIColor().HexToColor(hexString: "00bae3"), for: .normal)
            self.signUpOutlet.setTitleColor(UIColor.black, for: .normal)
        }) { (done) in
            if done {
                self.performSegue(withIdentifier: "login", sender: nil)
            }
        }
    }
    
    @IBAction func signupAction(_ sender: Any) {

        UIView.animate(withDuration: 0.5, animations: {
           
           self.signUpOutlet.backgroundColor = UIColor().HexToColor(hexString: "00bae3", alpha: 0.2)
           self.loginOutlet.backgroundColor = UIColor.clear
            
            self.signUpOutlet.setTitleColor(UIColor().HexToColor(hexString: "00bae3"), for: .normal)
            self.loginOutlet.setTitleColor(UIColor.black, for: .normal)
           
        }) { (done) in
            if done {
                self.performSegue(withIdentifier: "signUp", sender: nil)
            }
        }
    }
}
