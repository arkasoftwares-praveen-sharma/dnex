//
//  receiptViewController.swift
//  Dnex Shipper
//
//  Created by Arka on 03/03/20.
//  Copyright © 2020 Arka. All rights reserved.
//

import UIKit
import SDWebImage
import Cosmos
import SwiftyJSON



class receiptViewController: UIViewController {

    @IBOutlet weak var sellTxt: UILabel!
    @IBOutlet weak var sell: UILabel!
    @IBOutlet weak var feedbackTxt: ACFloatingTextfield!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var pickUpMain: UIView!
    @IBOutlet weak var pickView: UIView!
    
    @IBOutlet weak var vehicleNo: UILabel!
    @IBOutlet weak var rateBtnOutlet: UIButton!
    @IBOutlet weak var initialLbl: UILabel!
    @IBOutlet weak var doneBtnOutlet: UIButton!
    @IBOutlet weak var starView: UIView!
    @IBOutlet weak var submitBtnOutlet: UIButton!
    @IBOutlet weak var customView: UIView!
    
    
    @IBOutlet weak var order_id: UILabel!
    @IBOutlet weak var pickUp: UILabel!
    @IBOutlet weak var senderImg: UIImageView!
    
    @IBOutlet weak var dateTime: UILabel!
    @IBOutlet weak var senderName: UILabel!
    @IBOutlet weak var senderRating: UIButton!
    
    @IBOutlet weak var receiverName: UILabel!
    
    @IBOutlet weak var receiverInitials: UILabel!
    
    @IBOutlet weak var receiverMobile: UILabel!
    
    @IBOutlet weak var receiverAddress: UILabel!
    
    @IBOutlet weak var deliveryType: UILabel!
    
    @IBOutlet weak var paymentMode: UILabel!
    
    @IBOutlet weak var subTotal: UILabel!
    
    
    @IBOutlet weak var total: UILabel!
    @IBOutlet weak var starRate: CosmosView!
    
    @IBOutlet weak var ratingTitle: UILabel!
    
    @IBOutlet weak var customTable: UITableView!
    
    
    var Index = 0
    var orders:[current] = []
    var itemList:[item_order] = []
    var order_idString = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
//        heightConstraint.constant = 1080
        
//        itemList.append(item.init(name: "chair", description: "Test description for chair", height: "20", weight: "10", width: "2", item_image: UIImage (named: "noPathCopy2")!))
//        itemList.append(item.init(name: "Table", description: "Test description for table", height: "20", weight: "10", width: "2", item_image: UIImage (named: "noPathCopy2")!))
//        itemList.append(item.init(name: "chair", description: "Test description for chair", height: "20", weight: "10", width: "2", item_image: UIImage (named: "noPathCopy2")!))
        
        
        senderImg.layer.cornerRadius = senderImg.frame.height/2
        heightConstraint.constant = CGFloat(850 + (130 * itemList.count))
        
        
        pickUpMain.layer.cornerRadius = pickUpMain.frame.height/2
        pickUpMain.layer.borderColor = UIColor().HexToColor(hexString: "97a8ab", alpha: 0.5).cgColor
        pickUpMain.layer.borderWidth = 1
        
        
        pickView.layer.cornerRadius = pickView.frame.height/2
        rateBtnOutlet.layer.cornerRadius = rateBtnOutlet.frame.height/2
        doneBtnOutlet.layer.cornerRadius = doneBtnOutlet.frame.height/2
        
        submitBtnOutlet.layer.cornerRadius = submitBtnOutlet.frame.height/2
        
        initialLbl.layer.cornerRadius = initialLbl.frame.height/2
        initialLbl.clipsToBounds = true
        
        
        starView.layer.cornerRadius = starView.frame.height/2
        starView.layer.borderColor = UIColor().HexToColor(hexString: "97a8ab", alpha: 0.5).cgColor
        starView.layer.borderWidth = 1
        
        var order_id = UserDefaults.standard.string(forKey: "order") ?? ""
        
        if order_id == ""{
            order_id = order_idString
        }
        
        NetworkManager.receipt(uiRef: self, Webservice: "order-receipt", order_id: order_id){sJson in
            
            let sJSON = JSON(sJson)
            print(sJSON)
            if sJSON["status"].intValue == 200{
                    
                self.order_id.text = "Order Id:".localized + " \(sJSON["data"]["order_id"].stringValue)"
                    self.dateTime.text = sJSON["data"]["date"].stringValue
                    self.pickUp.text = sJSON["data"]["pickup_address"].stringValue
//                    self.senderName.text = sJSON["data"]["get_user_details"]["name"].stringValue
//                    let sender_image = "http://103.207.168.164:8008/storage/\(sJSON["data"]["get_user_details"]["profile_pic"].stringValue)"
//                    self.senderImg.sd_setImage(with: URL(string: sender_image), placeholderImage: UIImage(named: "defaultProfile"))
                    self.receiverName.text = sJSON["data"]["receiver_name"].stringValue
                    let initials = self.receiverName.text!.components(separatedBy: " ").reduce("") { ($0 == "" ? "" : "\($0.first!)") + "\($1.first!)" }
                    
                    self.receiverInitials.text = initials
                    self.receiverMobile.text = sJSON["data"]["receiver_country_code"].stringValue + sJSON["data"]["receiver_mobile"].stringValue
                    self.receiverAddress.text = sJSON["data"]["receiver_address"].stringValue
                    self.deliveryType.text = sJSON["data"]["delivery_type"].stringValue
                    self.paymentMode.text = sJSON["data"]["get_payment_detail"]["payment_type"].stringValue
                    self.total.text = "AED \(sJSON["data"]["get_payment_detail"]["amount"].stringValue)"
                    
                    let discount_per = sJSON["data"]["get_applied_coupon"]["coupon_data"]["discount_percentage"].stringValue
                    let total = sJSON["data"]["get_payment_detail"]["amount"].stringValue
                    
                    
                    var totalString = ""
                    var discountStr = ""
                    if discount_per != ""{
                        let dis_per = Double(discount_per) ?? 0.0
                        let sub_totalFloat = Double(total) ?? 0.0
                        let discount_amt:Double = round(dis_per * (sub_totalFloat/100))
                        //                        discount_amt = Double(discount_amt.cleanValue)!
                        totalString = String(format:"%.2f",sub_totalFloat  + discount_amt)
                        totalString = totalString.replacingOccurrences(of: ".00", with: "")
                        discountStr = String(format:"%.2f",discount_amt)
                        discountStr = discountStr.replacingOccurrences(of: ".00", with: "")
                        
                    }
                    else{
                        discountStr = "0"
                        totalString = total
                    }
                
                 let shipper_name = sJSON["data"]["get_assign_shipper"]["get_shipper_details"]["name"].stringValue
                    
                self.subTotal.text = "AED \(totalString)"
                self.ratingTitle.text = "Tell us how was your experience with".localized + " \(shipper_name)"
                
                
                
                let shipper_image = "http://103.207.168.164:8008/storage/\(sJSON["data"]["get_assign_shipper"]["get_shipper_details"]["profile_pic"].stringValue)"
                
                self.senderImg.sd_setImage(with: URL(string: shipper_image), placeholderImage: UIImage(named: "defaultProfile"))

                
                let shipper_vehicle_number = sJSON["data"]["get_assign_shipper"]["get_shipper_details"]["get_vehicle_details"]["vehicle_no"].stringValue
                
                
                let rating = sJSON["data"]["getshipperRating"].stringValue
                
                self.rateBtnOutlet.setTitle("  \(rating)", for: .normal)
                
                self.senderName.text = shipper_name
                self.vehicleNo.text = shipper_vehicle_number
                let type = sJSON["data"]["type"].stringValue
                if type == "sell"{
                    self.sell.isHidden = false
                    self.sellTxt.isHidden = false
                    
                    self.sellTxt.text = "AED \(total)"
                }
                else{
                    
                    self.sell.isHidden = true
                    self.sellTxt.isHidden = true
                    
                }
                
                for item_detail in sJSON["data"]["get_product_details"].arrayValue{
                    
                    let image = "http://103.207.168.164:8008/storage/\(item_detail["image"].stringValue)"
                    let name = item_detail["name"].stringValue
                    let height = "\(item_detail["height"].stringValue) cm"
                    let width = "\(item_detail["width"].stringValue) cm"
                    let weight = "\(item_detail["weight"].stringValue) kg"
                    let description_item = item_detail["description"].stringValue
                    
                    self.itemList.append(item_order.init(name: name, description: description_item, height: height, weight: weight, width: width, item_image:image ))
                    
                }
                self.customTable.reloadData()
                
            }
            else{
                CommonFunctions.showErrorAlert(uiRef: self, message: sJSON["message"].stringValue)
            }
        }
    }

    
    
    @IBAction func doneBtnClicked(_ sender: Any) {
        
        let order_id = UserDefaults.standard.string(forKey: "order") ?? ""
        
        if order_id != ""{
            UserDefaults.standard.removeObject(forKey: "order")
            performSegue(withIdentifier: "home", sender: nil)
        }
        else{
            NotificationCenter.default.post(name: .order, object: nil)
            self.dismiss(animated: true, completion: nil)
        }
        
        
        
    }
    
    
    @IBAction func submitBtnClicked(_ sender: Any) {
        
        if starRate.rating == 0{
            CommonFunctions.showErrorAlert(uiRef: self, message: "Please provide a rating to the customer !!".localized)
        }
        else{
            
            var order_id = ""
            var sender_id = ""
            
            if orders.count == 0{
                order_id = UserDefaults.standard.string(forKey: "order") ?? ""
                sender_id = UserDefaults.standard.string(forKey: "shipper_id") ?? ""
            }
            else{
                order_id = orders[Index].id
                sender_id = orders[Index].sender_id
            }
                        
            let rating = String(starRate.rating)
            let feedback = feedbackTxt.text ?? ""
            
            NetworkManager.rating(uiRef: self, Webservice: "add-rating", order_id: order_id, shipper_id: sender_id, rating: rating, feedback: feedback){sJson in
                let sJSON = JSON(sJson)
                print(sJSON)
                if sJSON["status"].intValue == 200{
                    self.customView.alpha = 1.0
                    UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseInOut, animations: {
                        self.customView.alpha = 0.0
                    })
                }
                else{
                    CommonFunctions.showErrorAlert(uiRef: self, message: sJSON["message"].stringValue)
                }
            }
        }
    }
    

    @IBAction func closeRate(_ sender: Any) {
        
//         customView.isHidden = true
        
        self.customView.alpha = 1.0
        //          forgotEmailTxt.text = ""
        UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseInOut, animations: {
            self.customView.alpha = 0.0
        })
    }
}

extension receiptViewController:UITableViewDataSource,UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         heightConstraint.constant = CGFloat(850 + (130 * itemList.count))
        return itemList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CELL") as! ItemListTableViewCell
        
        cell.itemView.layer.cornerRadius = 10
        cell.itemView.layer.borderColor = UIColor().HexToColor(hexString: "97a8ab", alpha: 0.5).cgColor
        cell.itemView.layer.borderWidth = 1
        
        cell.itemImage.layer.cornerRadius = 10
        cell.itemImage.layer.borderColor = UIColor().HexToColor(hexString: "97a8ab", alpha: 0.5).cgColor
        cell.itemImage.layer.borderWidth = 1
        
        cell.itemName.text = itemList[indexPath.row].name
        cell.itemDesc.text = itemList[indexPath.row].description
        cell.height.text = itemList[indexPath.row].height
        cell.width.text = itemList[indexPath.row].width
        cell.weight.text = itemList[indexPath.row].weight
        
//        cell.itemImage.image = itemList[indexPath.row].item_image
          cell.itemImage.sd_setImage(with: URL(string: itemList[indexPath.row].item_image), placeholderImage: UIImage(named: "item_placeholder"))
        
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 124
        
    }
    
}
