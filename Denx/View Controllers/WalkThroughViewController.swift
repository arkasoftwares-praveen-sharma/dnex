//
//  WalkThroughViewController.swift
//  Dnex Shipper
//
//  Created by Praveen Sharma on 13/07/20.
//  Copyright © 2020 Arka. All rights reserved.
//

import UIKit
import WebKit

class WalkThroughViewController: UIViewController {

    
    @IBOutlet weak var nextBtnOutlet: UIButton!
    @IBOutlet weak var pageControl: UIPageControl!
    

    var linkArr:[String] = ["http://103.207.168.164:8008/cms/walk-through-screen-one","http://103.207.168.164:8008/cms/walk-through-screen-two","http://103.207.168.164:8008/cms/walk-through-screen-three","http://103.207.168.164:8008/cms/walk-through-screen-four"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        UserDefaults.standard.set("walk", forKey: "walk")
        nextBtnOutlet.layer.cornerRadius = nextBtnOutlet.frame.height/2
        
        GIFHUD.shared.setGif(named: "loader.gif")
        
    }
    @IBAction func nextBtnClicked(_ sender: Any) {
        
        self.performSegue(withIdentifier: "next", sender: nil)
        
    }
}

extension WalkThroughViewController:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CELL", for: indexPath) as! walkThroughCollectionViewCell
        
        
        let myURLString = linkArr[indexPath.row]
        let url = URL(string: myURLString)
        if (url != nil){
            
            let request = URLRequest(url: url!)
            
            cell.webView.navigationDelegate = self
            cell.webView.load(request)
            
        }
        
        return cell
         
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageIndex = round(scrollView.contentOffset.x/view.frame.width)
        pageControl.currentPage = Int(pageIndex)
        if pageControl.currentPage == 4{
            nextBtnOutlet.setTitle("SIGN UP", for: .normal)
        }
        else{
            nextBtnOutlet.setTitle("SKIP", for: .normal)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        return CGSize(width:collectionView.frame.size.width, height:collectionView.frame.size.height);
        
    }
    
}

extension WalkThroughViewController: WKNavigationDelegate {

    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print("Started to load")
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("Finished loading")
        
        GIFHUD.shared.dismiss()
        
    }

    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print(error.localizedDescription)
    }
}
