//
//  LoginViewController.swift
//  Dnex
//
//  Created by Arka on 07/01/20.
//  Copyright © 2020 Arka. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import SwiftyJSON
import ADCountryPicker


class LoginViewController: UIViewController,ADCountryPickerDelegate,UITextFieldDelegate {
    @IBOutlet weak var loginOutlet: UIButton!
    @IBOutlet weak var sugnUpOutlet: UIButton!
    @IBOutlet weak var logoView: UIView!
    @IBOutlet weak var forgotPasswordView: UIView!
    @IBOutlet weak var forgotEmailTxt: ACFloatingTextfield!
    @IBOutlet weak var submitBtnOutlet: UIButton!
    
    
    @IBOutlet weak var newPasswordView: UIView!
    @IBOutlet weak var verificationCodeTxt: ACFloatingTextfield!
    @IBOutlet weak var newPasswordTxt: ACFloatingTextfield!
    @IBOutlet weak var confirmPasswordTxt: ACFloatingTextfield!
    
    @IBOutlet weak var submitOutlet: UIButton!
    @IBOutlet weak var emailView: UIView!
    
    @IBOutlet weak var mobileTxt: ACFloatingTextfield!
    @IBOutlet weak var codetxt: ACFloatingTextfield!
    @IBOutlet weak var passwordTxt: ACFloatingTextfield!
    @IBOutlet weak var phoneCodeTxt: ACFloatingTextfield!
    
    
    
     var created_at = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        GIFHUD.shared.setGif(named: "loader.gif")
        
        logoView.setShadow()
        loginOutlet.layer.cornerRadius = loginOutlet.frame.height/2
        submitBtnOutlet.layer.cornerRadius = submitBtnOutlet.frame.height/2
        sugnUpOutlet.layer.cornerRadius = sugnUpOutlet.frame.height/2
        submitOutlet.layer.cornerRadius = submitOutlet.frame.height/2
        
        
        //        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name:UIResponder.keyboardDidShowNotification, object: nil)
        //        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardDidShowNotification, object: nil)
        
        
//        let countryDictionary = ["AF":"93", "AL":"355", "DZ":"213","AS":"1", "AD":"376", "AO":"244", "AI":"1","AG":"1","AR":"54","AM":"374","AW":"297","AU":"61","AT":"43","AZ":"994","BS":"1","BH":"973","BD":"880","BB":"1","BY":"375","BE":"32","BZ":"501","BJ":"229","BM":"1","BT":"975","BA":"387","BW":"267","BR":"55","IO":"246","BG":"359","BF":"226","BI":"257","KH":"855","CM":"237","CA":"1","CV":"238","KY":"345","CF":"236","TD":"235","CL":"56","CN":"86","CX":"61","CO":"57","KM":"269","CG":"242","CK":"682","CR":"506","HR":"385","CU":"53","CY":"537","CZ":"420","DK":"45","DJ":"253","DM":"1","DO":"1","EC":"593","EG":"20","SV":"503","GQ":"240","ER":"291","EE":"372","ET":"251","FO":"298","FJ":"679","FI":"358","FR":"33","GF":"594","PF":"689","GA":"241","GM":"220","GE":"995","DE":"49","GH":"233","GI":"350","GR":"30","GL":"299","GD":"1","GP":"590","GU":"1","GT":"502","GN":"224","GW":"245","GY":"595","HT":"509","HN":"504","HU":"36","IS":"354","IN":"91","ID":"62","IQ":"964","IE":"353","IL":"972","IT":"39","JM":"1","JP":"81","JO":"962","KZ":"77","KE":"254","KI":"686","KW":"965","KG":"996","LV":"371","LB":"961","LS":"266","LR":"231","LI":"423","LT":"370","LU":"352","MG":"261","MW":"265","MY":"60","MV":"960","ML":"223","MT":"356","MH":"692","MQ":"596","MR":"222","MU":"230","YT":"262","MX":"52","MC":"377","MN":"976","ME":"382","MS":"1","MA":"212","MM":"95","NA":"264","NR":"674","NP":"977","NL":"31","AN":"599","NC":"687","NZ":"64","NI":"505","NE":"227","NG":"234","NU":"683","NF":"672","MP":"1","NO":"47","OM":"968","PK":"92","PW":"680","PA":"507","PG":"675","PY":"595","PE":"51","PH":"63","PL":"48","PT":"351","PR":"1","QA":"974","RO":"40","RW":"250","WS":"685","SM":"378","SA":"966","SN":"221","RS":"381","SC":"248","SL":"232","SG":"65","SK":"421","SI":"386","SB":"677","ZA":"27","GS":"500","ES":"34","LK":"94","SD":"249","SR":"597","SZ":"268","SE":"46","CH":"41","TJ":"992","TH":"66","TG":"228","TK":"690","TO":"676","TT":"1","TN":"216","TR":"90","TM":"993","TC":"1","TV":"688","UG":"256","UA":"380","AE":"971","GB":"44","US":"1", "UY":"598","UZ":"998", "VU":"678", "WF":"681","YE":"967","ZM":"260","ZW":"263","BO":"591","BN":"673","CC":"61","CD":"243","CI":"225","FK":"500","GG":"44","VA":"379","HK":"852","IR":"98","IM":"44","JE":"44","KP":"850","KR":"82","LA":"856","LY":"218","MO":"853","MK":"389","FM":"691","MD":"373","MZ":"258","PS":"970","PN":"872","RE":"262","RU":"7","BL":"590","SH":"290","KN":"1","LC":"1","MF":"590","PM":"508","VC":"1","ST":"239","SO":"252","SJ":"47","SY":"963","TW":"886","TZ":"255","TL":"670","VE":"58","VN":"84","VG":"284","VI":"340"]
        
        
//        if let countryCodeGet = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
//            print(countryCodeGet)
            codetxt.text = "+" + "971"
            phoneCodeTxt.text = "+" + "971"
//        }
            
        codetxt.isEnabled = false
        phoneCodeTxt.isEnabled = false
        codetxt.delegate = self
        phoneCodeTxt.delegate = self
        
    }
    
  
    func countryPicker(_ picker: ADCountryPicker, didSelectCountryWithName name: String, code: String, dialCode: String) {
        print(name)
        codetxt.text = dialCode
        phoneCodeTxt.text = dialCode
        self.dismiss(animated: true, completion: nil)
    }
//      func textFieldDidBeginEditing(_ textField: UITextField) {
//        if textField == codetxt || textField == phoneCodeTxt{
//            codetxt.endEditing(true)
//            phoneCodeTxt.endEditing(true)
//            let picker = ADCountryPicker()
//            picker.delegate = self
//
//            let pickerNavigationController = UINavigationController(rootViewController: picker)
//            self.present(pickerNavigationController, animated: true, completion: nil)
//        }
//    }
    
    @IBAction func resendBtnClicked(_ sender: Any) {
        
        self.view.endEditing(true)
        
        GIFHUD.shared.show(withOverlay: true, duration: 2)
        NetworkManager.sendOTP(uiRef: self, Webservice: "send-otp", mobile: forgotEmailTxt.text!, country_code: codetxt.text!, otp_type: "2",email: "", user_type: "2"){sJson in
           CommonFunctions.delay(1.0){
                    
                }
            let sJSON = JSON(sJson)
            print(sJSON)
            self.created_at = sJSON["data"]["created_at"].stringValue
//            CommonFunctions.showErrorAlert(uiRef: self, message: sJSON["data"]["otp"].stringValue)
            
                CommonFunctions.showErrorAlert(uiRef: self, message: sJSON["message"].stringValue)
           
        }
    }
    
    @IBAction func loginBtnClicked(_ sender: Any) {
        self.view.endEditing(true)
        if codetxt.text == ""{
            CommonFunctions.showErrorAlert(uiRef: self, message: "Enter country code !!")
        }
        else if mobileTxt.text == ""{
            CommonFunctions.showErrorAlert(uiRef: self, message: "Enter your mobile number !!".localized)
        }
        else if mobileTxt.text!.count < 4 || mobileTxt.text!.count > 12{
            CommonFunctions.showErrorAlert(uiRef: self,message: "Mobile number must be of 4 - 12 digits !!".localized)
        }
        else if passwordTxt.text == ""{
            CommonFunctions.showErrorAlert(uiRef: self, message: "Enter your password !!".localized)
        }
        else{
            GIFHUD.shared.show(withOverlay: true, duration: 2)
            NetworkManager.login(uiRef: self, Webservice: "signin", country_code: codetxt.text!, mobile: mobileTxt.text!, password: passwordTxt.text!){sJson in
//              CommonFunctions.timerDismiss(uiRef: self)
                CommonFunctions.delay(1.0) {

                    let sJSON = JSON(sJson)
                    print(sJSON)
                    
                    if sJSON["status"].intValue == 200{
                        UserDefaults.standard.set(sJSON["data"]["name"].stringValue, forKey: "name")
                        UserDefaults.standard.set("Bearer \(sJSON["data"]["token"].stringValue)", forKey: "token")
                        UserDefaults.standard.set("http://103.207.168.164:8008/storage/" + sJSON["data"]["profile_pic"].stringValue, forKey: "profile_pic")

                        self.performSegue(withIdentifier: "login", sender: nil)
                    }
                    else{
                        CommonFunctions.showErrorAlert(uiRef: self, message: sJSON["message"].stringValue)
                    }
                }
            }
        }
    }
    
    
    @IBAction func submitBtnClicked(_ sender: Any) {
        
        self.view.endEditing(true)
        if verificationCodeTxt.text == ""{
            CommonFunctions.showErrorAlert(uiRef: self, message: "Enter the validation code !!".localized)
        }
        else if newPasswordTxt.text == ""{
            CommonFunctions.showErrorAlert(uiRef: self, message: "Enter new password !!".localized)
        }
        else if confirmPasswordTxt.text == ""{
            CommonFunctions.showErrorAlert(uiRef: self, message: "Re-enter new password !!".localized)
        }
        else if confirmPasswordTxt.text != newPasswordTxt.text{
            CommonFunctions.showErrorAlert(uiRef: self,message: "Password did not matched, Please try again !!".localized)
            passwordTxt.text = ""
            confirmPasswordTxt.text = ""
            passwordTxt.becomeFirstResponder()
        }
        else{
          
            NetworkManager.resetPassword(uiRef: self, Webservice: "reset-password", mobile: forgotEmailTxt.text!, country_code: phoneCodeTxt.text!, otp: verificationCodeTxt.text!, password: newPasswordTxt.text!,created_at: self.created_at){sJson in
                let sJSON = JSON(sJson)
                print(sJSON)
                if sJSON["status"].intValue == 200{
                    
                    self.showAlert(message: "Password updated successfully, Please login with your new credentials".localized)
                    
                }
                else{
                    CommonFunctions.showErrorAlert(uiRef: self, message: sJSON["message"].stringValue)
                }
            }
        }
    }
    @IBAction func codeBtnClicked(_ sender: Any) {
        self.view.endEditing(true)
        
        let picker = ADCountryPicker()
        picker.delegate = self
        
        let pickerNavigationController = UINavigationController(rootViewController: picker)
        self.present(pickerNavigationController, animated: true, completion: nil)
    }
    
    @IBAction func sendBtnClicked(_ sender: Any) {
        self.view.endEditing(true)
        if forgotEmailTxt.text == ""{
            CommonFunctions.showErrorAlert(uiRef: self, message: "Enter your registered mobile number !!".localized)
        }
        else{
            GIFHUD.shared.show(withOverlay: true, duration: 2)
            NetworkManager.sendOTP(uiRef: self, Webservice: "send-otp", mobile: forgotEmailTxt.text!, country_code: codetxt.text!, otp_type: "2", email: "", user_type: "2"){sJson in
               CommonFunctions.delay(1.0){
                    
                }
                let sJSON = JSON(sJson)
                print(sJSON)
                
                if sJSON["status"].intValue == 200{
                    
                    self.created_at = sJSON["data"]["created_at"].stringValue
//                    CommonFunctions.showErrorAlert(uiRef: self, message: sJSON["data"]["otp"].stringValue)
                    
                    self.newPasswordView.isHidden = false
                    self.newPasswordView.alpha = 0.0
                    
                    UIView.animate(withDuration: 1.0, delay: 0, options: .curveEaseInOut, animations: {
                        self.newPasswordView.alpha = 1.0
                        self.emailView.isHidden = true
                        self.view.endEditing(true)
//                        self.forgotEmailTxt.text = ""
                    })
                }
                else{
                    CommonFunctions.showErrorAlert(uiRef: self, message: sJSON["message"].stringValue)
                }
            }
        }
    }
    
    
    func showAlert( message: String) {
        let alertController = UIAlertController(title: "Message".localized, message:
            message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: {action in
           
            self.view.endEditing(true)
            self.forgotPasswordView.alpha = 1.0
            self.verificationCodeTxt.text = ""
            self.newPasswordTxt.text = ""
            self.confirmPasswordTxt.text = ""
            UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseInOut, animations: {
                self.forgotPasswordView.alpha = 0.0
                self.newPasswordView.isHidden = true
                self.forgotPasswordView.isHidden = true
            })
            
        }))
        self.present(alertController, animated: true, completion: nil)
        
    }

    
    @IBAction func closeBtnClicked(_ sender: Any) {
        self.view.endEditing(true)
        self.view.endEditing(true)
        self.forgotPasswordView.alpha = 1.0
        verificationCodeTxt.text = ""
        newPasswordTxt.text = ""
        confirmPasswordTxt.text = ""
        self.forgotEmailTxt.text = ""
        UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseInOut, animations: {
            self.forgotPasswordView.alpha = 0.0
            self.newPasswordView.isHidden = true
        })
        
        self.forgotPasswordView.isHidden = true
        
        
    }
    
    //   @objc func keyboardWillShow(notification: NSNotification) {
    //        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
    //            if self.view.frame.origin.y == 0 {
    //                self.view.frame.origin.y = 150
    //            }
    //        }
    //    }
    //
    //    @objc func keyboardWillHide(notification: NSNotification) {
    //        if self.view.frame.origin.y != 0 {
    //            self.view.frame.origin.y = 0
    //        }
    //    }
    
    
    @IBAction func createBtnClicked(_ sender: Any) {
        self.view.endEditing(true)
        self.forgotPasswordView.isHidden = false
        self.forgotPasswordView.alpha = 0.0
        self.emailView.isHidden = false
        UIView.animate(withDuration: 1.0, delay: 0, options: .curveEaseInOut, animations: {
            self.forgotPasswordView.alpha = 1.0
        })
    }
    
    @IBAction func cancelBtnClicked(_ sender: Any) {
        self.view.endEditing(true)
        self.view.endEditing(true)
        self.forgotPasswordView.alpha = 1.0
        forgotEmailTxt.text = ""
        UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseInOut, animations: {
            self.forgotPasswordView.alpha = 0.0
        })
        
        self.forgotPasswordView.isHidden = true
    }
}


