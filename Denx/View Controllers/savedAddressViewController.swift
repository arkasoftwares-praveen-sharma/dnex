//
//  savedAddressViewController.swift
//  Dnex
//
//  Created by Arka on 18/03/20.
//  Copyright © 2020 Arka. All rights reserved.
//

import UIKit
import SwiftyJSON


struct item_address {
    var address_type = ""
    var long = ""
    var lat = ""
    var address = ""
    var id = ""
    var state = ""
    var city = ""
    var country = ""
    var pincode = ""
    var locality = ""
}

class savedAddressViewController: UIViewController {
    
    @IBOutlet weak var addAddress: UIButton!
    
    @IBOutlet weak var customCollection: UICollectionView!
    var addressList:[item_address] = []
    var address_id = ""
    var customTag = 0
    
    

    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        addAddress.layer.cornerRadius = 10
        addAddress.layer.borderColor = UIColor().HexToColor(hexString: "97a8ab", alpha: 0.5).cgColor
        addAddress.layer.borderWidth = 1
        
         GIFHUD.shared.setGif(named: "loader.gif")
         GIFHUD.shared.show(withOverlay: true, duration: 2)
        
        NotificationCenter.default.addObserver(self, selector: #selector(setToPeru), name: .fetch_address, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(setToPeru), name: .address, object: nil)
        
        NetworkManager.fetch_address(uiRef: self, Webservice: "user-address-list"){sJson in
            
            let sJSON = JSON(sJson)
            print(sJSON)
            CommonFunctions.delay(1.0){
                if sJSON["status"].intValue == 200{
                    
                    for item in sJSON["data"].arrayValue{
                        
                        let Lat = item["latitude"].stringValue
                        let Long = item["longitude"].stringValue
                        let id = item["id"].stringValue
                        
                        let state = ", \(item["state"].stringValue)"
                        let city = ", \(item["city"].stringValue)"
                        let country = ", \(item["country"].stringValue)"
                        let pincode = ", \(item["pincode"].stringValue)"
                        let locality = item["address"].stringValue
                        let address_type = item["address_type"].stringValue + " "
                        
                        let address = locality + city + state + country + pincode
                        
                        self.addressList.append(item_address.init(address_type: address_type, long: Long, lat: Lat, address: address, id: id,state: state,city: city,country: country,pincode: pincode,locality: locality))
                        
                    }
                    self.customCollection.reloadData()
                    
                }
                else{
                    CommonFunctions.showErrorAlert(uiRef: self, message: sJSON["message"].stringValue)
                }
            }
        }
    }
    
     @objc func setToPeru(){
      
        GIFHUD.shared.show(withOverlay: true, duration: 2)
               
        NetworkManager.fetch_address(uiRef: self, Webservice: "user-address-list"){sJson in
            
            let sJSON = JSON(sJson)
            print(sJSON)
            CommonFunctions.delay(1.0){
                if sJSON["status"].intValue == 200{
                    
                    self.addressList.removeAll()
                    
                    for item in sJSON["data"].arrayValue{
                        
                        let Lat = item["latitude"].stringValue
                        let Long = item["longitude"].stringValue
                        let id = item["id"].stringValue
                        
                        let state = ",\(item["state"].stringValue)"
                        let city = ",\(item["city"].stringValue)"
                        let country = ",\(item["country"].stringValue)"
                        let pincode = ",\(item["pincode"].stringValue)"
                        let locality = item["address"].stringValue
                        let address_type = item["address_type"].stringValue + " "
                        
                        let address = locality + city + state + country + pincode
                        
                       self.addressList.append(item_address.init(address_type: address_type, long: Long, lat: Lat, address: address, id: id,state: state,city: city,country: country,pincode: pincode,locality: locality))
                        
                    }
                    self.customCollection.reloadData()
                }
                else{
                    CommonFunctions.showErrorAlert(uiRef: self, message: sJSON["message"].stringValue)
                }
            }
        }
    }
}

extension savedAddressViewController:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return addressList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CELL", for: indexPath) as! savedAddressCollectionViewCell
        GIFHUD.shared.dismiss()
        cell.customBtnOutlet.layer.cornerRadius = cell.customBtnOutlet.frame.height/2
        cell.customView.layer.cornerRadius = 10
        cell.customView.layer.borderColor = UIColor().HexToColor(hexString: "97a8ab", alpha: 0.5).cgColor
        cell.customView.layer.borderWidth = 1
        
        
        cell.customLbl.text = "\(addressList[indexPath.row].address)"
        cell.customBtnOutlet.setTitle("  \(addressList[indexPath.row].address_type)", for: .normal)
       
        cell.editBtn.tag = indexPath.row
        cell.editBtn.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        
        
        cell.deleteBtn.tag = indexPath.row
        cell.deleteBtn.addTarget(self, action: #selector(deletebuttonPressed), for: .touchUpInside)
        
        if cell.customBtnOutlet.title(for: .normal)?.lowercased() == "  home"{
            cell.customBtnOutlet.setImage(UIImage (named: "homeWhite"), for: .normal)
        }
        else if cell.customBtnOutlet.title(for: .normal)?.lowercased() == "  work" || cell.customBtnOutlet.title(for: .normal)?.lowercased() == "  office"{
            cell.customBtnOutlet.setImage(UIImage (named: "workWhite"), for: .normal)
        }
        else{
            cell.customBtnOutlet.setImage(UIImage (named: "otherWhite"), for: .normal)
        }

//        cell.customLbl.sizeToFit()
        
        return cell
        
    }
    
    @objc func buttonPressed(_ sender: UIButton) {
        customTag = sender.tag
        address_id = addressList[sender.tag].id
        self.performSegue(withIdentifier: "edit", sender: nil)
        
    }
    @objc func deletebuttonPressed(_ sender: UIButton) {
        customTag = sender.tag
        self.showAlert(message: "Are you sure you want to remove this address ?".localized)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 10
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width:(self.view.frame.size.width/2.3), height:130);
        
    }
    
    func deleteItem(){
        
        GIFHUD.shared.show(withOverlay: true, duration: 2)
        NetworkManager.RemoveAddress(uiRef: self, Webservice: "destroy-user-address", address_id:addressList[customTag].id){sJson in
            
            let sJSON = JSON(sJson)
            print(sJSON)
            CommonFunctions.delay(1.0){
                if sJSON["status"].intValue == 200{
                    self.view.endEditing(true)
                    self.addressList.remove(at: self.customTag)
                    
                    self.customCollection.reloadData()
                }
                else{
                    CommonFunctions.showErrorAlert(uiRef: self, message: sJSON["message"].stringValue)
                }
            }
        }
    }
    
    
    func showAlert( message: String) {
        let alertController = UIAlertController(title: "Message".localized, message:
               message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Yes".localized, style: .default, handler: {action in
               
               self.deleteItem()
               
           }))
           
        alertController.addAction(UIAlertAction(title: "No".localized, style: .default, handler: {action in
               
           }))
           self.present(alertController, animated: true, completion: nil)
           
       }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "edit"{
            let Object = segue.destination as! addAddressViewController
            Object.flag = address_id
            Object.city = addressList[customTag].city
            Object.state = addressList[customTag].state
            Object.country = addressList[customTag].country
            Object.locality = addressList[customTag].locality
            Object.address_type = addressList[customTag].address_type
            Object.pincode = addressList[customTag].pincode
        }
    }
}
