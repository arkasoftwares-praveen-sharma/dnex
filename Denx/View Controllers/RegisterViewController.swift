//
//  RegisterViewController.swift
//  Dnex
//
//  Created by Arka on 07/01/20.
//  Copyright © 2020 Arka. All rights reserved.
//

import UIKit
import ADCountryPicker
import GoogleMaps
import SwiftyJSON



class RegisterViewController: UIViewController,ADCountryPickerDelegate,UITextFieldDelegate {
    
    @IBOutlet weak var createBtnOulet: UIButton!
    @IBOutlet weak var verifyView: UIView!
    
    @IBOutlet weak var otpTxt: VPMOTPView!
    @IBOutlet weak var verifyOutlet: UIButton!
    @IBOutlet weak var pinView: SVPinView!
    @IBOutlet weak var countryCode: ACFloatingTextfield!
    
    
    @IBOutlet weak var NameTxt: ACFloatingTextfield!
    
    @IBOutlet weak var EmailTxt: ACFloatingTextfield!
    @IBOutlet weak var MobileNo: ACFloatingTextfield!
    @IBOutlet weak var PasswordTxt: ACFloatingTextfield!
    @IBOutlet weak var ConfirmPasswordTxt: ACFloatingTextfield!
    
    
    
    
    
    
    var enteredOtp = ""
   var created_at = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        
        GIFHUD.shared.setGif(named: "loader.gif")
        
        countryCode.delegate = self
        NameTxt.delegate = self
        EmailTxt.delegate = self
        PasswordTxt.delegate = self
        MobileNo.delegate = self
        ConfirmPasswordTxt.delegate = self
        
        createBtnOulet.layer.cornerRadius = createBtnOulet.frame.height/2
        verifyOutlet.layer.cornerRadius = verifyOutlet.frame.height/2
        
        otpTxt.otpFieldsCount = 4
        otpTxt.otpFieldDefaultBorderColor.withAlphaComponent(1.0)
        otpTxt.otpFieldDefaultBorderColor = UIColor.black
        otpTxt.otpFieldEnteredBorderColor = UIColor.black
        otpTxt.otpFieldErrorBorderColor = UIColor().HexToColor(hexString: "00BAE3")
        otpTxt.otpFieldBorderWidth = 2
        otpTxt.delegate = self
        otpTxt.shouldAllowIntermediateEditing = false
        
        //otpTxt.backgroundColor = UIColor.white
        otpTxt.alpha = 0.35
        otpTxt.otpFieldSize = 70
        //        otpTxt.frame.size.width = 50
        //        otpTxt.frame.size.height = 20
        otpTxt.cursorColor = UIColor.blue
        
        otpTxt.otpFieldDisplayType = .circular
        
        // Create the UI
//        otpTxt.initializeUI()
        otpTxt.becomeFirstResponder()
        //        configurePinView()
        
//        let countryDictionary = ["AF":"93", "AL":"355", "DZ":"213","AS":"1", "AD":"376", "AO":"244", "AI":"1","AG":"1","AR":"54","AM":"374","AW":"297","AU":"61","AT":"43","AZ":"994","BS":"1","BH":"973","BD":"880","BB":"1","BY":"375","BE":"32","BZ":"501","BJ":"229","BM":"1","BT":"975","BA":"387","BW":"267","BR":"55","IO":"246","BG":"359","BF":"226","BI":"257","KH":"855","CM":"237","CA":"1","CV":"238","KY":"345","CF":"236","TD":"235","CL":"56","CN":"86","CX":"61","CO":"57","KM":"269","CG":"242","CK":"682","CR":"506","HR":"385","CU":"53","CY":"537","CZ":"420","DK":"45","DJ":"253","DM":"1","DO":"1","EC":"593","EG":"20","SV":"503","GQ":"240","ER":"291","EE":"372","ET":"251","FO":"298","FJ":"679","FI":"358","FR":"33","GF":"594","PF":"689","GA":"241","GM":"220","GE":"995","DE":"49","GH":"233","GI":"350","GR":"30","GL":"299","GD":"1","GP":"590","GU":"1","GT":"502","GN":"224","GW":"245","GY":"595","HT":"509","HN":"504","HU":"36","IS":"354","IN":"91","ID":"62","IQ":"964","IE":"353","IL":"972","IT":"39","JM":"1","JP":"81","JO":"962","KZ":"77","KE":"254","KI":"686","KW":"965","KG":"996","LV":"371","LB":"961","LS":"266","LR":"231","LI":"423","LT":"370","LU":"352","MG":"261","MW":"265","MY":"60","MV":"960","ML":"223","MT":"356","MH":"692","MQ":"596","MR":"222","MU":"230","YT":"262","MX":"52","MC":"377","MN":"976","ME":"382","MS":"1","MA":"212","MM":"95","NA":"264","NR":"674","NP":"977","NL":"31","AN":"599","NC":"687","NZ":"64","NI":"505","NE":"227","NG":"234","NU":"683","NF":"672","MP":"1","NO":"47","OM":"968","PK":"92","PW":"680","PA":"507","PG":"675","PY":"595","PE":"51","PH":"63","PL":"48","PT":"351","PR":"1","QA":"974","RO":"40","RW":"250","WS":"685","SM":"378","SA":"966","SN":"221","RS":"381","SC":"248","SL":"232","SG":"65","SK":"421","SI":"386","SB":"677","ZA":"27","GS":"500","ES":"34","LK":"94","SD":"249","SR":"597","SZ":"268","SE":"46","CH":"41","TJ":"992","TH":"66","TG":"228","TK":"690","TO":"676","TT":"1","TN":"216","TR":"90","TM":"993","TC":"1","TV":"688","UG":"256","UA":"380","AE":"971","GB":"44","US":"1", "UY":"598","UZ":"998", "VU":"678", "WF":"681","YE":"967","ZM":"260","ZW":"263","BO":"591","BN":"673","CC":"61","CD":"243","CI":"225","FK":"500","GG":"44","VA":"379","HK":"852","IR":"98","IM":"44","JE":"44","KP":"850","KR":"82","LA":"856","LY":"218","MO":"853","MK":"389","FM":"691","MD":"373","MZ":"258","PS":"970","PN":"872","RE":"262","RU":"7","BL":"590","SH":"290","KN":"1","LC":"1","MF":"590","PM":"508","VC":"1","ST":"239","SO":"252","SJ":"47","SY":"963","TW":"886","TZ":"255","TL":"670","VE":"58","VN":"84","VG":"284","VI":"340"]
        
        
//        if let countryCodeGet = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
//            print(countryCodeGet)
            countryCode.text = "+" + "971"
//        }
        countryCode.isEnabled = false
       
    }
    
    @IBAction func countryBtnClicked(_ sender: Any) {
        
        self.view.endEditing(true)
        
        let picker = ADCountryPicker()
        picker.delegate = self
                    
        let pickerNavigationController = UINavigationController(rootViewController: picker)
        self.present(pickerNavigationController, animated: true, completion: nil)
        
    }
    
    func countryPicker(_ picker: ADCountryPicker, didSelectCountryWithName name: String, code: String, dialCode: String) {
        print(name)
        countryCode.text = dialCode
        self.dismiss(animated: true, completion: nil)
    }
//    func textFieldDidBeginEditing(_ textField: UITextField) {
//        if textField == countryCode{
//            countryCode.endEditing(true)
//            let picker = ADCountryPicker()
//            picker.delegate = self
//
////            let pickerNavigationController = UINavigationController(rootViewController: picker)
////            self.present(pickerNavigationController, animated: true, completion: nil)
//        }
//    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
//        if textField .isEqual(EmailTxt){
//              EmailTxt.endEditing(true)
//        }
           // EmailTxt.endEditing(true)
        
    }

    
    func didFinishEnteringPin(pin:String) {
        enteredOtp = pin
    }
    
    
    @IBAction func backBtnClicked(_ sender: Any) {
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }

    
    @IBAction func createBtnClicked(_ sender: Any) {
        
        let empty = NameTxt.text?.trimmingCharacters(in: .whitespaces)
        
        let providedEmailAddress = EmailTxt.text
        
        let isEmailAddressValid = isValidEmailAddress(emailAddressString: providedEmailAddress!)
        
        
        /// Check password for blank space
        
         let whitespace = NSCharacterSet.whitespaces
        let range = PasswordTxt.text!.rangeOfCharacter(from: whitespace)
        
        
        if NameTxt.text == "" || empty == ""{
            
            CommonFunctions.showErrorAlert(uiRef: self,message: "Please enter name !!".localized)
        }
        else if EmailTxt.text == ""{
            
            CommonFunctions.showErrorAlert(uiRef: self,message: "Please enter email id !!".localized)
            
        }else if !(isEmailAddressValid) {
            
            print("Email address is not valid")
            //showAlertSucces(message: "Email address is not valid")
            
            CommonFunctions.showErrorAlert(uiRef: self,message: "Email id is not valid !!".localized)
        }else if MobileNo.text == ""{
            
            CommonFunctions.showErrorAlert(uiRef: self,message: "Please enter mobile number !!".localized)
            
        }
        else if MobileNo.text!.count < 4 || MobileNo.text!.count > 12{
            
            CommonFunctions.showErrorAlert(uiRef: self,message: "Mobile number must be of 4 - 12 digits !!".localized)
            
        }
            
        else if PasswordTxt.text == "" {
            
            CommonFunctions.showErrorAlert(uiRef: self,message: "Please enter a password !!".localized)
            
        }
        else if PasswordTxt.text!.count < 6 || PasswordTxt.text!.count > 14{
            
            CommonFunctions.showErrorAlert(uiRef: self,message: "Password must be of 6 - 14 characters !!".localized)
            
        }
   
        else if range != nil {
            CommonFunctions.showErrorAlert(uiRef: self,message: "White space is not allowed in password !!".localized)
            }

        else if ConfirmPasswordTxt.text == "" {
            
            CommonFunctions.showErrorAlert(uiRef: self,message: "Please re-enter the password !!".localized)
            
        }
           
        else if PasswordTxt.text != ConfirmPasswordTxt.text{
            CommonFunctions.showErrorAlert(uiRef: self,message: "Password did not matched, Please try again !!".localized)
            PasswordTxt.text = ""
            ConfirmPasswordTxt.text = ""
            PasswordTxt.becomeFirstResponder()
        }
//        else if PasswordTxt.isPasswordValid(PasswordTxt.text!) == false {
//            CommonFunctions.showErrorAlert(uiRef: self,message: "Password must contain an upper case character, a number, a special character & should range from 8 to 10 characters")
//        }
            
        else{
            GIFHUD.shared.show(withOverlay: true, duration: 2)
            NetworkManager.sendOTP(uiRef: self, Webservice: "send-otp", mobile: MobileNo.text!, country_code: countryCode.text!, otp_type: "1", email: EmailTxt.text!, user_type: "2"){sJson in
               CommonFunctions.delay(1.0){
                    
                }
                let sJSON = JSON(sJson)
                print(sJSON)
                
                if sJSON["status"].intValue == 200{
                    
//                    CommonFunctions.showErrorAlert(uiRef: self, message: sJSON["data"]["otp"].stringValue)
                    self.created_at = sJSON["data"]["created_at"].stringValue
                    self.verifyView.isHidden = false
                    self.verifyView.alpha = 0.0
                    
                    UserDefaults.standard.set(sJSON["data"]["name"].stringValue, forKey: "name")
                    UserDefaults.standard.set("Bearer \(sJSON["data"]["token"].stringValue)", forKey: "token")

                    UIView.animate(withDuration: 1.0, delay: 0, options: .curveEaseInOut, animations: {
                        self.verifyView.alpha = 1.0
                    })
                    self.otpTxt.initializeUI()
//                    self.otpTxt.becomeFirstResponder()
                }
                else{
                    CommonFunctions.showErrorAlert(uiRef: self, message: sJSON["message"].stringValue)
                }
            }
        }
    }
    
    
   
    
    
    @IBAction func verifyBtnClicked(_ sender: Any) {
        
        
        GIFHUD.shared.show(withOverlay: true, duration: 2)
                
        NetworkManager.verifyOTP(uiRef: self, Webservice: "verify-otp", mobile: MobileNo.text!, country_code: countryCode.text!, otp_type: "1", otp: enteredOtp,created_at: self.created_at){sJson in
           
            let sJSON = JSON(sJson)
            print(sJSON)
            
            if sJSON["status"].intValue == 200{
                
                NetworkManager.signUp(uiRef: self, Webservice: "signup", mobile: self.MobileNo.text!, country_code: self.countryCode.text!, name: self.NameTxt.text!, email: self.EmailTxt.text!, password: self.PasswordTxt.text!, user_type: "2"){sJson in
                    CommonFunctions.delay(1.0){
                    
                }
                    
                    let sJSON = JSON(sJson)
                    print(sJSON)
                    
                    if sJSON["status"].intValue == 200{
                        UserDefaults.standard.set("Bearer" + " " + sJSON["data"]["token"].stringValue, forKey: "token")
                        UserDefaults.standard.set("http://103.207.168.164:8008/storage/" + sJSON["data"]["profile_pic"].stringValue, forKey: "profile_pic")
                        UserDefaults.standard.set(sJSON["data"]["name"].stringValue, forKey: "name")

                        self.performSegue(withIdentifier: "verify", sender: nil)
                    }
                    else{
                       
                        CommonFunctions.showErrorAlert(uiRef: self, message: sJSON["message"].stringValue)
                    }
                }
            }
            else{
                CommonFunctions.showErrorAlert(uiRef: self, message: sJSON["message"].stringValue)
            }
        }
    }
    
    
    @IBAction func resendBtnClciked(_ sender: Any) {
        
        NetworkManager.sendOTP(uiRef: self, Webservice: "send-otp", mobile: MobileNo.text!, country_code: countryCode.text!, otp_type: "1", email: EmailTxt.text!, user_type: "2"){sJson in
            
            let sJSON = JSON(sJson)
            print(sJSON)
             self.created_at = sJSON["data"]["created_at"].stringValue
//            CommonFunctions.showErrorAlert(uiRef: self, message: sJSON["data"]["otp"].stringValue)
            CommonFunctions.showErrorAlert(uiRef: self, message: sJSON["message"].stringValue)
            
        }
    }
    
    
    @IBAction func cancelBtnClicked(_ sender: Any) {
        
        self.verifyView.alpha = 1.0
        
        UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseInOut, animations: {
            self.verifyView.alpha = 0.0
        })
        
        self.verifyView.isHidden = false
        
        otpTxt.endEditing(true)
        
    }
    
}
extension RegisterViewController: VPMOTPViewDelegate {
    
    
    func hasEnteredAllOTP(hasEntered: Bool, customView: UIView) -> Bool {
        print("Has entered all OTP? \(hasEntered)")
        
        
        return enteredOtp == "12345"
    }
    
    
    func shouldBecomeFirstResponderForOTP(otpFieldIndex index: Int) -> Bool {
        return true
    }
    
    func enteredOTP(otpString: String, customOtpView: UIView) {
        enteredOtp = otpString
        print("OTPString: \(otpString)")
        
    }
    
    func isValidEmailAddress(emailAddressString: String) -> Bool {
        
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailAddressString as NSString
            let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        
        return  returnValue
    }
}


extension UIView {
    
    func isPasswordValid(_ password : String) -> Bool{
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*?&#])[A-Za-z\\d$@$!%*?&#]{8,10}")
        return passwordTest.evaluate(with: password)
    }
    
}
