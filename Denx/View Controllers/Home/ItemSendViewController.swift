//
//  ItemListViewController.swift
//  Dnex
//
//  Created by Arka on 03/02/20.
//  Copyright © 2020 Arka. All rights reserved.
//

import UIKit
import SwiftyJSON


struct item {
    var name = ""
    var description = ""
    var height = ""
    var weight = ""
    var width = ""
    var item_image = UIImage()
}


class ItemSendViewController: UIViewController {

    @IBOutlet weak var addBtnOutlet: UIButton!
    @IBOutlet weak var itemTableView: UITableView!
    
    @IBOutlet weak var itemConstraint: NSLayoutConstraint!
    @IBOutlet weak var deliveryTypeView: UIView!
    
    
    @IBOutlet weak var addPromo: UIButton!
    
    @IBOutlet weak var proceedOutlet: UIButton!
    @IBOutlet weak var itemView: UIView!
    @IBOutlet weak var paymentView: UIView!
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var profilePicOutlet: UIButton!
    
    @IBOutlet weak var havePromoLbl: UILabel!
    @IBOutlet weak var promoSideViewLbl: UILabel!
    @IBOutlet weak var promoDisLbl: UILabel!
    @IBOutlet weak var mainViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var customView: UIView!
    
    @IBOutlet weak var promoView: UIView!
    @IBOutlet weak var promoAppliedView: UIView!
    @IBOutlet weak var itemName: ACFloatingTextfield!
    @IBOutlet weak var ItemHeight: ACFloatingTextfield!
    
    @IBOutlet weak var promoTxt: ACFloatingTextfield!
    @IBOutlet weak var itemWidth: ACFloatingTextfield!
    @IBOutlet weak var itemWeight: ACFloatingTextfield!
    @IBOutlet weak var itemDescriptiion: UITextView!
    @IBOutlet weak var itemInfoOutlet: UIButton!
    
    @IBOutlet weak var addItemBtnOutlet: UIButton!
  
    @IBOutlet weak var addItemCustomView: UIView!
    @IBOutlet weak var promoCustomView: UIView!
    @IBOutlet weak var applyOutlet: UIButton!
    
    
    @IBOutlet weak var normalDeliveryOutlet: UIButton!
    @IBOutlet weak var fastDeliveryOutlet: UIButton!
    
    @IBOutlet weak var paymentCustomView: UIView!
    
    @IBOutlet weak var paymentHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var cashBtnOutlet: UIButton!
    @IBOutlet weak var cardBtnOutlet: UIButton!
    @IBOutlet weak var paymentTableView: UITableView!
    
    @IBOutlet weak var submitPaymentOutlet: UIButton!
    
    @IBOutlet weak var paymentTypeLbl: UILabel!
    @IBOutlet weak var totalAmountLbl: UILabel!
    
    
    @IBOutlet weak var sellView: UIView!
    @IBOutlet weak var sellAmtTxt: ACFloatingTextfield!
    

    @IBOutlet weak var amountLbl: UILabel!
    
    @IBOutlet weak var submitSellOutlet: UIButton!
    @IBOutlet weak var titleTxt: UILabel!
    
    @IBOutlet weak var sellHeight: NSLayoutConstraint!
    @IBOutlet weak var paymentTop: NSLayoutConstraint!
    
    
    var paymentType = "Cash"
    var DeliveryType = "Normal"
    
    var promoCode = ""
    
    var itemList:[item] = []
    var customTag = 0
    var itemIDArr:[String] = []
    
    var normalDelivery = ""
    var fastDelivery = ""
    
    var discount_amount = "0.0"
    var discount_percent = 0.0
    var height = ""
    var card_number = ""
    var order_id = ""
    var schedule = ""
    var deliveryTime = ""
    var type = ""
    
    var total = ""
    
    var loyalty_points = ""
    var loyalty_amount = ""
    
    var loyalty_used = true
    var total_amount = ""
     @objc func setToPeru(){
        
        
        GIFHUD.shared.show(withOverlay: true, duration: 2)
        NetworkManager.fetch_token(uiRef: self){sJson in
            
            let sJSON = JSON(sJson)
            
            print(sJSON)
            
            let access_token = sJSON["access_token"].stringValue
            
            if access_token != ""{
                UserDefaults.standard.set(access_token, forKey: "paypal_token")
                
                NetworkManager.fetch_card_id(uiRef: self, Webservice: "get-user-card-id"){sJson in
                    
                    let sJSON = JSON(sJson)
                    print(sJSON)
                    
                    if sJSON["status"].intValue == 200{
                        
                        
                        for item in sJSON["data"].arrayValue{
                            let card_id = item["card_id"].stringValue
                            
                            
                            NetworkManager.fetch_card(uiRef: self, card_id: card_id){sJson in
                                CommonFunctions.delay(0.6){
                                    let sJSON = JSON(sJson)
                                    print(sJSON)
                                    
                                    self.card_number = sJSON["number"].stringValue
                                    self.paymentTableView.reloadData()
                                    
                                }
                            }
                        }
                    }
                }
            }
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        fastDelivery = UserDefaults.standard.string(forKey: "fast") ?? ""
        normalDelivery = UserDefaults.standard.string(forKey: "normal") ?? ""
        
        GIFHUD.shared.setGif(named: "loader.gif")
         NotificationCenter.default.addObserver(self, selector: #selector(setToPeru), name: .card, object: nil)
        itemConstraint.constant = CGFloat((124 * itemList.count) + 80)
        addBtnOutlet.layer.cornerRadius = addBtnOutlet.frame.height / 2
        addPromo.layer.cornerRadius = addPromo.frame.height / 2
        proceedOutlet.layer.cornerRadius = proceedOutlet.frame.height / 2
        itemInfoOutlet.layer.cornerRadius = itemInfoOutlet.frame.height / 2
        applyOutlet.layer.cornerRadius = applyOutlet.frame.height / 2
        submitPaymentOutlet.layer.cornerRadius = submitPaymentOutlet.frame.height / 2
        submitSellOutlet.layer.cornerRadius = submitSellOutlet.frame.height / 2
        
        
        itemDescriptiion.layer.cornerRadius = 10
        itemDescriptiion.layer.borderWidth = 0.5
        itemDescriptiion.layer.borderColor = UIColor.lightGray.cgColor
        
        mainViewHeightConstraint.constant = mainViewHeightConstraint.constant - itemView.frame.height + itemConstraint.constant + 300
        
        //        mainView.frame.size.height = mainView.frame.size.height - itemView.frame.height + itemConstraint.constant
        //        mainView.frame.size.height = 50
        
        paymentHeightConstraint.constant = 300
        paymentTypeLbl.text = paymentType
        profilePicOutlet.setBackgroundImage(UIImage (named: "group33pick"), for: .normal)
        
        totalAmountLbl.text = " AED \(normalDelivery)"
        
        total_amount = totalAmountLbl.text!
        
        
        let type = UserDefaults.standard.string(forKey: "type") ?? ""
        
        UserDefaults.standard.removeObject(forKey: "type")
        
        if type == "Sell"{
            titleTxt.text = "SELL"
            mainViewHeightConstraint.constant = mainViewHeightConstraint.constant + 315
            sellHeight.constant = 100
            paymentTop.constant = 15
            
        }
        else{
            titleTxt.text = "SEND"
            mainViewHeightConstraint.constant = mainViewHeightConstraint.constant - 315
            sellHeight.constant = 0
            paymentTop.constant = 0
            
            sellView.clipsToBounds = true
        }
        
        NetworkManager.fetch_notification(uiRef: self, Webservice: "user-loyality-points"){sJson in
            
            let sJSON = JSON(sJson)
            print(sJSON)
            
            if sJSON["status"].intValue == 200{
                
                self.loyalty_amount = sJSON["data"]["total_amount"].stringValue
                self.loyalty_points = sJSON["data"]["total_point"].stringValue
                print(self.loyalty_amount)
                self.paymentTableView.reloadData()
            }
            else{
                CommonFunctions.showErrorAlert(uiRef: self, message: sJSON["message"].stringValue)
            }
        }
    }
    
    @IBAction func sellAmountAction(_ sender: Any) {
        
        self.sellView.isHidden = false
        self.customView.isHidden = false
        self.customView.alpha = 0.0
        //          forgotEmailTxt.text = ""
        UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseInOut, animations: {
            self.customView.alpha = 1.0
        })
    }
    
    
    @IBAction func submitSellBtnClicked(_ sender: Any) {
        
        sellView.isHidden = true
        customView.isHidden = true
        
        if sellAmtTxt.text == ""{
            amountLbl.text = ""
        }
        else{
            amountLbl.text = "AED \(sellAmtTxt.text ?? "")"
        }
     
        self.view.endEditing(true)
        
    }
    
    @IBAction func closeSell(_ sender: Any) {
    
        sellView.isHidden = true
        customView.isHidden = true
    
        sellAmtTxt.text = ""
        
    }
    
    @IBAction func backBtnClicked(_ sender: Any) {
        
        self.showAlertBack(message: "Do you want to create a new order ?".localized)
        
    }
    
    
    @IBAction func submitPayment(_ sender: Any) {
        
        total = totalAmountLbl.text!
        
        if submitPaymentOutlet.title(for: .normal) == "ADD CARD".localized{
            self.performSegue(withIdentifier: "card", sender: nil)
        }
        else{
            
            if loyalty_used == true {
                total = totalAmountLbl.text!
                
                if paymentType == "Cash"{
                    loyalty_used = false
                }
                    
                else{
                    
                    print(loyalty_amount)
                    print(totalAmountLbl.text!)
                    
                    let tot = totalAmountLbl.text?.replacingOccurrences(of: " AED ", with: "") ?? ""
                    
                    let loyaltyAmt = Double(loyalty_amount) ?? 0.0
                    let total = Double(tot) ?? 0.0
                    
                    let totalAmount = String(total - loyaltyAmt)
                    if totalAmount.contains("-"){
                        totalAmountLbl.text = "AED 0"
                    }
                    else{
                        totalAmountLbl.text = " AED \(totalAmount)"
                    }
                }
            }
            else{
                totalAmountLbl.text = total_amount
            }
            
  
            paymentTypeLbl.text = paymentType
            self.customView.alpha = 1.0
            //          forgotEmailTxt.text = ""
            UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseInOut, animations: {
                self.customView.alpha = 0.0
            })
            self.customView.isHidden = true
            self.paymentCustomView.isHidden = true
            paymentTableView.isHidden = true
            
        }
    }
    
    
    @IBAction func cashClicked(_ sender: Any) {
        cashBtnOutlet.setImage(UIImage (named: "radioselect"), for: .normal)
        cardBtnOutlet.setImage(UIImage (named: "radio"), for: .normal)
        paymentType = "Cash"
        paymentHeightConstraint.constant = 300
        paymentTableView.isHidden = true
        
        loyalty_used = false
        
        submitPaymentOutlet.setTitle("SUBMIT".localized, for: .normal)
    }
    
    @IBAction func cardClicked(_ sender: Any) {
        
        cashBtnOutlet.setImage(UIImage (named: "radio"), for: .normal)
        cardBtnOutlet.setImage(UIImage (named: "radioselect"), for: .normal)
        paymentType = "Card"
        paymentHeightConstraint.constant = 545
        paymentTableView.isHidden = false
        
        
        if card_number == ""{
            submitPaymentOutlet.setTitle("ADD CARD".localized, for: .normal)
        }

    }
    
    @IBAction func normalDeliveryClicked(_ sender: Any) {
        
        normalDeliveryOutlet.setImage(UIImage (named: "radioselect"), for: .normal)
        fastDeliveryOutlet.setImage(UIImage (named: "radio"), for: .normal)
        DeliveryType = "Normal"
        print(discount_amount)
        fastDelivery = UserDefaults.standard.string(forKey: "normal") ?? ""
        let fast_charge = Double(UserDefaults.standard.string(forKey: "normal") ?? "") ?? 0.0
        let dis_amount = Double(normalDelivery)!*((discount_percent)/100)
        self.promoDisLbl.text = "You got".localized + " AED \(dis_amount)" + " discount".localized
        let discount = Double(dis_amount)
        let dueAmt = fast_charge - discount
        totalAmountLbl.text = " AED \(dueAmt)"
        total_amount = totalAmountLbl.text!
    }
    
    @IBAction func fastDeliveryClicked(_ sender: Any) {
        
        normalDeliveryOutlet.setImage(UIImage (named: "radio"), for: .normal)
        fastDeliveryOutlet.setImage(UIImage (named: "radioselect"), for: .normal)
        DeliveryType = "Fast"
        
        let fast_charge = Double(UserDefaults.standard.string(forKey: "fast") ?? "") ?? 0.0
        let dis_amount = fast_charge*((discount_percent)/100)
        
        let discount = Double(dis_amount)
        self.promoDisLbl.text = "You got".localized + " AED \(discount)" + " discount".localized
        let dueAmt = fast_charge - discount
        print(fastDelivery)
         print(discount_amount)
        totalAmountLbl.text = " AED \(dueAmt)"
        total_amount = totalAmountLbl.text!
    }
    
    @IBAction func paymentModeBtnClicked(_ sender: Any) {
        
        self.customView.isHidden = false
        self.paymentCustomView.isHidden = false
        
        self.customView.alpha = 0.0
        
        UIView.animate(withDuration: 1.0, delay: 0, options: .curveEaseInOut, animations: {
            self.customView.alpha = 1.0
        })
        
        if paymentTypeLbl.text == "Card"{
            paymentHeightConstraint.constant = 545
            paymentTableView.isHidden = false
        }
        else{
            paymentHeightConstraint.constant = 300
            paymentTableView.isHidden = true
        }
        
        GIFHUD.shared.show(withOverlay: true, duration: 2)
        NetworkManager.fetch_token(uiRef: self){sJson in
            
            let sJSON = JSON(sJson)
            
            print(sJSON)
            
            let access_token = sJSON["access_token"].stringValue
            
            if access_token != ""{
                UserDefaults.standard.set(access_token, forKey: "paypal_token")
                
                NetworkManager.fetch_card_id(uiRef: self, Webservice: "get-user-card-id"){sJson in
                    
                    let sJSON = JSON(sJson)
                    print(sJSON)
                    
                    if sJSON["status"].intValue == 200{
                        
                        CommonFunctions.delay(0.6){
                            GIFHUD.shared.dismiss()
                        }
                        for item in sJSON["data"].arrayValue{
                            let card_id = item["card_id"].stringValue
                             GIFHUD.shared.show(withOverlay: true, duration: 2)
                            
                            NetworkManager.fetch_card(uiRef: self, card_id: card_id){sJson in
                                CommonFunctions.delay(0.6){
                                    GIFHUD.shared.dismiss()
                                    let sJSON = JSON(sJson)
                                    print(sJSON)
                                    
                                    self.card_number = sJSON["number"].stringValue
                                    self.paymentTableView.reloadData()
                                    
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func closePayment(_ sender: Any) {
        
        self.customView.alpha = 1.0
        //          forgotEmailTxt.text = ""
        UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseInOut, animations: {
            self.customView.alpha = 0.0
        })
        self.customView.isHidden = true
        self.paymentCustomView.isHidden = true
        paymentHeightConstraint.constant = 300
        paymentTableView.isHidden = true
        
        
        cashBtnOutlet.setImage(UIImage (named: "radioselect"), for: .normal)
        cardBtnOutlet.setImage(UIImage (named: "radio"), for: .normal)
        
        paymentType = "Cash".localized
        
    }
    
    
    @IBAction func addPromoBtnClicked(_ sender: Any) {
       
        if addPromo.title(for: .normal) == "ADD".localized{
            self.customView.isHidden = false
            self.promoCustomView.isHidden = false
            
            self.customView.alpha = 0.0
            
            UIView.animate(withDuration: 1.0, delay: 0, options: .curveEaseInOut, animations: {
                self.customView.alpha = 1.0
            })
        }
        else{
            promoCode = ""
            
            self.promoDisLbl.text = ""
            self.promoView.backgroundColor = UIColor.white
            self.promoSideViewLbl.backgroundColor = UIColor().HexToColor(hexString: "00BAE3")
            self.promoAppliedView.isHidden = true
            self.addPromo.setTitle("ADD".localized, for: .normal)
            self.addPromo.setTitleColor(UIColor.white, for: .normal)
            self.addPromo.backgroundColor = UIColor().HexToColor(hexString: "00BAE3")
            self.havePromoLbl.isHidden = false
            
            
            discount_percent = 0.00
            
            if DeliveryType == "Fast".localized{
                self.totalAmountLbl.text = " AED \(UserDefaults.standard.string(forKey: "fast") ?? "")"
            }
            else{
                self.totalAmountLbl.text = " AED \(UserDefaults.standard.string(forKey: "normal") ?? "")"
            }
        }
    }
        
    
    
    @IBAction func itenInfoAdd(_ sender: Any) {
        
        if itemList.count == 3{
            CommonFunctions.showErrorAlert(uiRef: self, message: "You cannot add more then 3 items in an order !!".localized)
        }
        else{
            
            self.customView.isHidden = false
            self.addItemCustomView.isHidden = false
            self.customView.alpha = 0.0
            
            UIView.animate(withDuration: 1.0, delay: 0, options: .curveEaseInOut, animations: {
                self.customView.alpha = 1.0
            })
        }
    }
    
    @IBAction func addItemBtn(_ sender: Any) {
        
        if profilePicOutlet.backgroundImage(for: .normal) == #imageLiteral(resourceName: "group33pick"){
            CommonFunctions.showErrorAlert(uiRef: self, message: "Upload an image for product !!".localized)
        }
       else if itemName.text == ""{
            CommonFunctions.showErrorAlert(uiRef: self, message: "Enter item name !!".localized)
        }
        else if itemDescriptiion.text == ""{
            CommonFunctions.showErrorAlert(uiRef: self, message: "Enter a short description !!".localized)
        }
        else if ItemHeight.text == "" && DeliveryType == "Fast"{
            CommonFunctions.showErrorAlert(uiRef: self, message: "Enter a item's height !!".localized)
        }
        else if itemWidth.text == "" && DeliveryType == "Fast"{
            CommonFunctions.showErrorAlert(uiRef: self, message: "Enter a item's width !!".localized)
        }
        else if itemWeight.text == "" && DeliveryType == "Fast"{
            CommonFunctions.showErrorAlert(uiRef: self, message: "Enter a item's weight !!".localized)
        }
            
        else{
            self.view.endEditing(true)
            GIFHUD.shared.show(withOverlay: true, duration: 2)
            let order_id = UserDefaults.standard.string(forKey: "id_order") ?? ""
            
            if addItemBtnOutlet.title(for: .normal) == "UPDATE".localized{
                NetworkManager.RemoveItem(uiRef: self, Webservice: "remove-order-product", product_id:itemIDArr[customTag]){sJson in
                    
                    let sJSON = JSON(sJson)
                    print(sJSON)
                    
                    if sJSON["status"].intValue == 200{
                        
                        self.itemList.remove(at: self.customTag)
                        self.itemTableView.reloadData()
                        
                        
                        NetworkManager.addItem(webservice: "order-add-product", order_id: order_id, name: self.itemName.text!, media: self.profilePicOutlet.backgroundImage(for: .normal)!, description: self.itemDescriptiion.text!, height: self.ItemHeight.text!, width: self.itemWidth.text!, weight: self.itemWeight.text!){sJson in
                            
                            CommonFunctions.delay(1.0){
                                GIFHUD.shared.dismiss()
                                let sJSON = JSON(sJson)
                                print(sJSON)
                                
                                if sJSON["status"].intValue == 200{
                                    
                                    
                                    self.itemIDArr.append(sJSON["data"]["id"].stringValue)
                                    
                                    
                                    self.customView.alpha = 1.0
                                    //          forgotEmailTxt.text = ""
                                    UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseInOut, animations: {
                                        self.customView.alpha = 0.0
                                    })
                                    self.customView.isHidden = true
                                    self.addItemCustomView.isHidden = true
                                    
                                    
                                    self.itemList.append(item.init(name: self.itemName.text!, description: self.itemDescriptiion.text!, height: self.ItemHeight.text!, weight: self.itemWeight.text!, width: self.itemWidth.text!,item_image: self.profilePicOutlet.backgroundImage(for: .normal)!))
                                    
                                    self.itemName.text = ""
                                    self.itemWidth.text = ""
                                    self.ItemHeight.text = ""
                                    self.itemWeight.text = ""
                                    self.itemDescriptiion.text = ""
                                    self.profilePicOutlet.setBackgroundImage(UIImage (named: "group33pick"), for: .normal)
                                    self.addItemBtnOutlet.setTitle("ADD", for: .normal)
                                    
                                    
                                    self.itemConstraint.constant = CGFloat((124 * self.itemList.count) + 80)
                                    self.mainViewHeightConstraint.constant = self.mainViewHeightConstraint.constant - self.itemView.frame.height + self.itemConstraint.constant
                                    //          mainView.frame.size.height = mainView.frame.size.height - itemView.frame.height + itemConstraint.constant
                                    self.itemTableView.reloadData()
                                    
                                }
                                else{
                                    CommonFunctions.showErrorAlert(uiRef: self, message: sJSON["message"].stringValue)
                                }
                            }
                        }
                    }
                    else{
//                        CommonFunctions.showErrorAlert(uiRef: self, message: sJSON["message"].stringValue)
                    }
                }
            }
            
            else{
                
                
                NetworkManager.addItem(webservice: "order-add-product", order_id: order_id, name: itemName.text!, media: profilePicOutlet.backgroundImage(for: .normal)!, description: itemDescriptiion.text!, height: ItemHeight.text!, width: itemWidth.text!, weight: itemWeight.text!){sJson in
                    
                    CommonFunctions.delay(1.0){
                        GIFHUD.shared.dismiss()
                        let sJSON = JSON(sJson)
                        print(sJSON)
                        
                        if sJSON["status"].intValue == 200{
                            
                            
                            self.itemIDArr.append(sJSON["data"]["id"].stringValue)
                            
                            
                            self.customView.alpha = 1.0
                            //          forgotEmailTxt.text = ""
                            UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseInOut, animations: {
                                self.customView.alpha = 0.0
                            })
                            self.customView.isHidden = true
                            self.addItemCustomView.isHidden = true
                            
                            
                            self.itemList.append(item.init(name: self.itemName.text!, description: self.itemDescriptiion.text!, height: self.ItemHeight.text!, weight: self.itemWeight.text!, width: self.itemWidth.text!,item_image: self.profilePicOutlet.backgroundImage(for: .normal)!))
                            
                            self.itemName.text = ""
                            self.itemWidth.text = ""
                            self.ItemHeight.text = ""
                            self.itemWeight.text = ""
                            self.itemDescriptiion.text = ""
                            self.profilePicOutlet.setBackgroundImage(UIImage (named: "group33pick"), for: .normal)
                            self.addItemBtnOutlet.setTitle("ADD".localized, for: .normal)
                            
                            
                            self.itemConstraint.constant = CGFloat((124 * self.itemList.count) + 80)
                            self.mainViewHeightConstraint.constant = self.mainViewHeightConstraint.constant - self.itemView.frame.height + self.itemConstraint.constant
                            //          mainView.frame.size.height = mainView.frame.size.height - itemView.frame.height + itemConstraint.constant
                            self.itemTableView.reloadData()
                            
                        }
                        else{
                            CommonFunctions.showErrorAlert(uiRef: self, message: sJSON["message"].stringValue)
                        }
                    }
                }
            }
//          mainView.frame.size.height = mainView.frame.size.height + 124
        }
    }
    
    @IBAction func proceedBtnClicked(_ sender: Any) {
        
//        if itemList.count == 0{
//            CommonFunctions.showErrorAlert(uiRef: self, message: "Add an item to proceed !!".localized)
//        }
//        else
        if type == "Sell" && sellAmtTxt.text == ""{
            CommonFunctions.showErrorAlert(uiRef: self, message: "Add selling amount !!".localized)
        }
        else {
            
            for item in itemList{
                if item.height == "" || item.width == "" || item.weight == ""{
                    CommonFunctions.showErrorAlert(uiRef: self, message: "Please add height, width & weight of".localized + " \(item.name) " + "for fast delivery".localized)
                    break
                }
            }
            
            if paymentType == "Card"{
               
                var card_id = ""
                
                NetworkManager.fetch_card_id(uiRef: self, Webservice: "get-user-card-id"){sJson in
                    
                    let sJSON = JSON(sJson)
                    print(sJSON)
                    
                    if sJSON["status"].intValue == 200{
                        
                        
                        for item in sJSON["data"].arrayValue{
                            card_id = item["card_id"].stringValue
                        }
                        
                        
                        /// PAYER Data
                        var dict = Dictionary<String, Any>()
                        var token_dict = Dictionary<String, Any>()
                        dict["payment_method"] = "credit_card"
                        
                        
                        var funding = Array<Any>()
//                        CARD-7HB0293848859733JL2GZ6GI
                        var credit_token = Dictionary<String, Any>()
                        credit_token["credit_card_id"] = card_id
                        
                        token_dict["credit_card_token"] = credit_token
                        
                        funding.append(token_dict)
                        
                        dict["funding_instruments"] = funding
                        
                        
                        /// Transaction data
                        var transaction_arr:[Any] = []
                        var transaction_dict = Dictionary<String,Any>()
                        var amount = Dictionary<String,Any>()
                        let total_amount = self.totalAmountLbl.text!.replacingOccurrences(of: " AED ", with: "")
                        
                        let amt = Double(total_amount) ?? 0.0
                        
                        let converted_amount = amt * 0.27
                        
                        let roundedString = String(format: "%.2f", converted_amount)

                        
                        amount["total"] = roundedString
                        amount["currency"] = "USD"
                        
                        
                        transaction_dict["amount"] = amount
                        transaction_dict["description"] = "Payment by vaulted credit card"
                        
                        transaction_arr.append(transaction_dict)
                        
                        print(dict)
                        print("================")
                        print(transaction_arr)
                        
                        GIFHUD.shared.show(withOverlay: true, duration: 2)
                        NetworkManager.payment(uiRef: self, payer: dict, transaction: transaction_arr){sJson in
                            CommonFunctions.delay(0.6){
                                
                                let sJSON = JSON(sJson)
                                print(sJSON)
                                if sJSON["state"] == "approved"{
                                    self.payment()
                                }
                                else{
                                    CommonFunctions.showErrorAlert(uiRef: self, message: "Unable to process your payment !!".localized)
                                }
                            }
                        }
                    }
                    else{
                        CommonFunctions.showErrorAlert(uiRef: self, message: sJSON["message"].stringValue)
                    }
                    
                }
            
            }
            else{
                payment()
            }
        }
    }
    
    
    func payment(){
        
        let order_id = UserDefaults.standard.string(forKey: "id_order")!
        let amount = totalAmountLbl.text!.replacingOccurrences(of: "AED ", with: "")
        GIFHUD.shared.show(withOverlay: true, duration: 2)
        
        if loyalty_used != true{
            loyalty_points = ""
        }
        
        NetworkManager.updateOrder(uiRef: self, webservice: "store-payment-data", order_id: order_id, delivery_type: DeliveryType, promo: promoCode, payment_type: paymentType, total: amount, sell_amount: sellAmtTxt.text!,loyalty_point: loyalty_points){sJson in
            CommonFunctions.delay(1.0){
                GIFHUD.shared.dismiss()
                let sJSON = JSON(sJson)
                print(sJSON)
                
                if sJSON["status"].intValue == 200{
                    self.view.endEditing(true)
                    
                    if self.schedule == "receiver"{
                        self.showAlertSchedule(message: "We have received your order, It will be assigned to the shipper when the receiver will enter the drop location !!".localized)
                    }
                    else if self.deliveryTime == "later"{
                        self.showAlertSchedule(message: "We have received your order, It will be assigned to the shipper on the selected date !!")
                    }
                    else{
                        self.order_id = sJSON["data"]["order_id"].stringValue
                        self.performSegue(withIdentifier: "gif", sender: nil)
                    }
//                  CommonFunctions.showErrorAlert(uiRef: self, message: sJSON["message"].stringValue)
                }
                else{
                    CommonFunctions.showErrorAlert(uiRef: self, message: sJSON["message"].stringValue)
                }
            }
        }
    }
    
    @IBAction func applyPromoBtn(_ sender: Any) {
        
        if promoTxt.text == ""{
            CommonFunctions.showErrorAlert(uiRef: self, message: "Enter a promo code !!".localized)
        }
        else{
            NetworkManager.applyPromo(uiRef: self, Webservice: "verify-coupon", coupon_code:promoTxt.text!){sJson in
                
                let sJSON = JSON(sJson)
                print(sJSON)
                
                if sJSON["status"].intValue == 200{
                    self.view.endEditing(true)
                    let totalAmt = self.totalAmountLbl.text!.replacingOccurrences(of: " AED ", with: "")
                    let dis_percent = Double(sJSON["data"]["discount_percentage"].stringValue)
                    
                    self.discount_percent = Double(dis_percent ?? 0.0)
                    
                    let dis_amount = Double(totalAmt)!*((dis_percent ?? 0.0)/100)
                    self.discount_amount = String(dis_amount)
                    self.totalAmountLbl.text = " AED \(String(Double(totalAmt)! - dis_amount))"
                    
                    self.promoDisLbl.text = "You got".localized + " AED \(dis_amount) " + "discount".localized
                    self.promoView.backgroundColor = UIColor().HexToColor(hexString: "18d293")
                    self.promoSideViewLbl.backgroundColor = UIColor.white
                    self.promoAppliedView.isHidden = false
                    self.addPromo.setTitle("REMOVE".localized, for: .normal)
                    self.addPromo.setTitleColor(UIColor.black, for: .normal)
                    
                    self.havePromoLbl.isHidden = true
                    
                    self.addPromo.backgroundColor = UIColor.white
                    
                    self.showAlertPromo(message: "Congratulations, You have availed a discount of".localized + " \(sJSON["data"]["discount_percentage"].stringValue)" + "% on current bill".localized)
                }
                else{
                    CommonFunctions.showErrorAlert(uiRef: self, message: sJSON["message"].stringValue)
                }
            }
        }
    }
  
    
    @IBAction func closeItem(_ sender: Any) {
        
        self.customView.alpha = 1.0
        //          forgotEmailTxt.text = ""
        UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseInOut, animations: {
            self.customView.alpha = 0.0
        })
        self.customView.isHidden = true
        self.addItemCustomView.isHidden = true
        
        
        itemName.text = ""
        itemWidth.text = ""
        ItemHeight.text = ""
        itemWeight.text = ""
        itemDescriptiion.text = ""
    }
    
    
    @IBAction func closePromo(_ sender: Any) {
        
        self.customView.alpha = 1.0
        //          forgotEmailTxt.text = ""
        UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseInOut, animations: {
            self.customView.alpha = 0.0
         
        })
        
        self.customView.isHidden = true
        self.promoCustomView.isHidden = true
        
    }
    @IBAction func uploadBtnClicked(_ sender: Any) {
        
        let alert = UIAlertController(title: "Choose Image".localized, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera".localized, style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery".localized, style: .default, handler: { _ in
            self.openGallery()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel".localized, style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func openCamera()
    {
        //        ANLoader.showLoading("Please wait", disableUI: false)
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallery()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have permission to access gallery.".localized, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "gif"{
            let Object = segue.destination as! gifViewController
            Object.order_id = order_id
        }
        else if segue.identifier == "card"{
            let Object = segue.destination as! AddCardViewController
            Object.flag = "payment"
        }
    }
}


extension ItemSendViewController:UITableViewDataSource,UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == itemTableView{
            return itemList.count
        }
        else{
            
            if card_number == ""{
               return 1
            }
            else{
                return 2
            }
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == itemTableView{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CELL") as! ItemListTableViewCell
        
        cell.itemView.layer.cornerRadius = 10
        cell.itemView.layer.borderColor = UIColor().HexToColor(hexString: "97a8ab", alpha: 0.5).cgColor
        cell.itemView.layer.borderWidth = 1
        
        cell.itemImage.layer.cornerRadius = 10
        cell.itemImage.layer.borderColor = UIColor().HexToColor(hexString: "97a8ab", alpha: 0.5).cgColor
        cell.itemImage.layer.borderWidth = 1
        
        cell.itemName.text = itemList[indexPath.row].name
        cell.itemDesc.text = itemList[indexPath.row].description
        cell.height.text = "\(itemList[indexPath.row].height) cm"
        cell.width.text = "\(itemList[indexPath.row].width) cm"
        cell.weight.text = "\(itemList[indexPath.row].weight) kg"
        
        height = itemList[indexPath.row].height
        cell.itemImage.image = itemList[indexPath.row].item_image
        
        cell.deleteBtn.tag = indexPath.row
        cell.deleteBtn.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        
        
        cell.selectionStyle = .none
        
        return cell
            
        }
        
        else{
            
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "loyalty") as! loyaltyTableViewCell
                cell.outView.layer.cornerRadius = 10
                cell.outView.layer.borderColor = UIColor().HexToColor(hexString: "97a8ab", alpha: 0.5).cgColor
                cell.outView.layer.borderWidth = 1
                
                
                cell.loyaltyLbl.text = loyalty_points + " Points".localized
                cell.selectImg.image = #imageLiteral(resourceName: "group436")
                cell.selectionStyle = .none
                            
                return cell
            }
            else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "cardCell") as! cardTableViewCell
                cell.outView.layer.cornerRadius = 10
                cell.outView.layer.borderColor = UIColor().HexToColor(hexString: "97a8ab", alpha: 0.5).cgColor
                cell.outView.layer.borderWidth = 1
                submitPaymentOutlet.setTitle("SUBMIT".localized, for: .normal)
                cell.cardNumber.text = card_number
                
                cell.selectionStyle = .none
                return cell
            }
        }
    }
    
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        
        if tableView == itemTableView{
           
            itemName.text = itemList[indexPath.row].name
            ItemHeight.text = itemList[indexPath.row].height
            itemWidth.text = itemList[indexPath.row].width
            itemWeight.text = itemList[indexPath.row].weight
            itemDescriptiion.text = itemList[indexPath.row].description
            customTag = indexPath.row
            profilePicOutlet.setBackgroundImage(itemList[indexPath.row].item_image, for: .normal)
            addItemBtnOutlet.setTitle("UPDATE".localized, for: .normal)
            
            self.customView.isHidden = false
            self.addItemCustomView.isHidden = false
            self.customView.alpha = 0.0
            
            UIView.animate(withDuration: 1.0, delay: 0, options: .curveEaseInOut, animations: {
                self.customView.alpha = 1.0
            })
        }
        
        else{
            if indexPath.row == 0{
                let cell = tableView.cellForRow(at: indexPath) as! loyaltyTableViewCell
                
                if cell.selectImg.image == #imageLiteral(resourceName: "group436"){
                    cell.selectImg.image = nil
                    loyalty_used = false
                }
                else{
                   cell.selectImg.image = #imageLiteral(resourceName: "group436")
                    loyalty_used = true
                }
            }
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == itemTableView{
            return 124
        }
        else{
            return 100
        }
    }
    
    
    @objc func buttonPressed(_ sender: UIButton) {
        customTag = sender.tag
        
        self.showAlert(message: "Are you sure you want to remove this item ?".localized)
    }
    
    func deleteItem(){
        NetworkManager.RemoveItem(uiRef: self, Webservice: "remove-order-product", product_id:itemIDArr[customTag]){sJson in
            
            let sJSON = JSON(sJson)
            print(sJSON)
            
            if sJSON["status"].intValue == 200{
                self.view.endEditing(true)
                self.itemList.remove(at: self.customTag)
                self.itemConstraint.constant = CGFloat((124 * self.itemList.count) + 80)
                self.mainViewHeightConstraint.constant = self.mainViewHeightConstraint.constant - self.itemView.frame.height + self.itemConstraint.constant
                //            self.mainView.frame.size.height = self.mainView.frame.size.height + self.itemConstraint.constant
                
                self.itemTableView.reloadData()
            }
            else{
                CommonFunctions.showErrorAlert(uiRef: self, message: sJSON["message"].stringValue)
            }
        }
    }
    
    
    func showAlert( message: String) {
        let alertController = UIAlertController(title: "Message".localized, message:
            message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Yes".localized, style: .default, handler: {action in
            
            self.deleteItem()
            
        }))
        
        alertController.addAction(UIAlertAction(title: "No".localized, style: .default, handler: {action in
            
        }))
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    
    func showAlertPromo( message: String) {
        let alertController = UIAlertController(title: "Message".localized, message:
            message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: {action in
           
            self.customView.alpha = 1.0
            //          forgotEmailTxt.text = ""
            UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseInOut, animations: {
                self.customView.alpha = 0.0
                
            })
            
            self.customView.isHidden = true
            self.promoCustomView.isHidden = true
            self.promoCode = self.promoTxt.text!
            self.promoTxt.text = ""
        }))
               
        self.present(alertController, animated: true, completion: nil)
        
    }

    func showAlertSchedule( message: String) {
        let alertController = UIAlertController(title: "Message".localized, message:
            message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: {action in
           
            self.performSegue(withIdentifier: "Home", sender: nil)
        }))
               
        self.present(alertController, animated: true, completion: nil)
        
    }
    func showAlertBack( message: String) {
        let alertController = UIAlertController(title: "Message".localized, message:
            message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Yes".localized, style: .default, handler: {action in
           
            self.performSegue(withIdentifier: "Home", sender: nil)
        }))
        
        alertController.addAction(UIAlertAction(title: "No".localized, style: .default, handler: {action in
            self.dismiss(animated: true, completion: nil)
        }))
        self.present(alertController, animated: true, completion: nil)
        
    }
}

extension ItemSendViewController:UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        dismiss(animated: true, completion: nil)
        //        ANLoader.showLoading("Please wait", disableUI: false)
        print(info)
        
//                let pickedImg = info[.originalImage] as? UIImage
//
//              profilePicOutlet.setImage(pickedImg, for: .normal)
        
        if let pickedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
           
   
//            profileImage.image = pickedImage
            
            profilePicOutlet.layer.cornerRadius = 5
            profilePicOutlet.layer.borderColor = UIColor().HexToColor(hexString: "97a8ab", alpha: 0.5).cgColor
            profilePicOutlet.layer.borderWidth = 1
            
            profilePicOutlet.clipsToBounds = true
            
            
            profilePicOutlet.setBackgroundImage(pickedImage, for: .normal)
            
            

            
        }
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
//        ANLoader.hide()
    }
    
  
}
