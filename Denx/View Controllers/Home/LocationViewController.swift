//
//  LocationViewController.swift
//  Dnex
//
//  Created by Arka on 18/01/20.
//  Copyright © 2020 Arka. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import SwiftyJSON




class LocationViewController: UIViewController,CLLocationManagerDelegate,GMSMapViewDelegate,UITextFieldDelegate {

    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var customCollection: UICollectionView!
    @IBOutlet weak var addBtnOutlet: UIButton!
    @IBOutlet weak var submitBtnOutlet: UIButton!
    
    @IBOutlet weak var searchTxt: UITextField!
    
    
    
    var location: CLLocationCoordinate2D = CLLocationCoordinate2D()
    let locationManager = CLLocationManager()
    
    var contentArr:[String] = ["Home","Work"]
    var ImgArr:[String] = ["homeIcon","workNew"]
    let marker = GMSMarker()
    let geocoder = CLGeocoder()

    var Lat = ""
    var Long = ""
    var flag = ""
    var googleClient = GMSPlacesClient()
    var addressList:[item_address] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

        
        searchTxt.delegate = self
        
        mapView.delegate = self
//        mapView.isMyLocationEnabled = true
        
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
             
        searchView.setShadow()
     
        addBtnOutlet.layer.cornerRadius = addBtnOutlet.frame.height/2
        submitBtnOutlet.layer.cornerRadius = submitBtnOutlet.frame.height/2

        addBtnOutlet.setShadow()
        
        mapView.addSubview(searchView)
        mapView.addSubview(customCollection)
        mapView.addSubview(addBtnOutlet)
        mapView.addSubview(submitBtnOutlet)
        
        NotificationCenter.default.addObserver(self, selector: #selector(setToPeru), name: .location_address, object: nil)
        
          NotificationCenter.default.addObserver(self, selector: #selector(setToPeru), name: .fetch_address, object: nil)
        
        if flag != ""{
            self.customCollection.isHidden = true
            self.addBtnOutlet.isHidden = true
        }
        else{
            self.customCollection.isHidden = false
            self.addBtnOutlet.isHidden = false
        }
        
        NetworkManager.fetch_address(uiRef: self, Webservice: "user-address-list"){sJson in
            
            let sJSON = JSON(sJson)
            print(sJSON)
            CommonFunctions.delay(1.0){
                GIFHUD.shared.dismiss()
                if sJSON["status"].intValue == 200{
                    
                    for item in sJSON["data"].arrayValue{
                        
                        let Lat = item["latitude"].stringValue
                        let Long = item["longitude"].stringValue
                        let id = item["id"].stringValue
                        
                        let state = ", \(item["state"].stringValue)"
                        let city = ", \(item["city"].stringValue)"
                        let country = ", \(item["country"].stringValue)"
                        let pincode = ", \(item["pincode"].stringValue)"
                        let locality = item["address"].stringValue
                        let address_type = item["address_type"].stringValue
                        
                        let address = locality + city + state + country + pincode
                        
                        self.addressList.append(item_address.init(address_type: address_type, long: Long, lat: Lat, address: address, id: id))
                        
                    }
                    self.customCollection.reloadData()
                    
                }
                else{
                    CommonFunctions.showErrorAlert(uiRef: self, message: sJSON["message"].stringValue)
                }
            }
        }
        
        
        
        
    }
    
    
    @objc func setToPeru(){
        
        GIFHUD.shared.show(withOverlay: true, duration: 2)
        self.addressList.removeAll()
        NetworkManager.fetch_address(uiRef: self, Webservice: "user-address-list"){sJson in
            
            let sJSON = JSON(sJson)
            print(sJSON)
            CommonFunctions.delay(1.0){
                
                GIFHUD.shared.dismiss()
                if sJSON["status"].intValue == 200{
                    for item in sJSON["data"].arrayValue{
                        
                        let Lat = item["latitude"].stringValue
                        let Long = item["longitude"].stringValue
                        let id = item["id"].stringValue
                        
                        let state = ",\(item["state"].stringValue)"
                        let city = ",\(item["city"].stringValue)"
                        let country = ",\(item["country"].stringValue)"
                        let pincode = ",\(item["pincode"].stringValue)"
                        let locality = item["address"].stringValue
                        let address_type = item["address_type"].stringValue
                        
                        let address = locality + city + state + country + pincode
                        
                        self.addressList.append(item_address.init(address_type: address_type, long: Long, lat: Lat, address: address, id: id))
                        
                    }
                    self.customCollection.reloadData()
                }
                else{
                    CommonFunctions.showErrorAlert(uiRef: self, message: sJSON["message"].stringValue)
                }
            }
        }
    }
    
    @IBAction func backBtnClicked(_ sender: Any) {
        UserDefaults.standard.removeObject(forKey: "type")
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func submitBtnClicked(_ sender: Any) {
        
        Lat = String(marker.position.latitude)
        Long = String(marker.position.longitude)
        
        let pick = UserDefaults.standard.string(forKey: "type")
        
        if pick == "pickup"{
            let pickLat = String(marker.position.latitude)
            let pickLong = String(marker.position.longitude)
            
            UserDefaults.standard.set(pickLat, forKey: "pickLAT")
            UserDefaults.standard.set(pickLong, forKey: "pickLONG")
            
        }
        
        UserDefaults.standard.set(searchTxt.text!, forKey: "search")
        UserDefaults.standard.set(Lat, forKey: "LAT")
        UserDefaults.standard.set(Long, forKey: "LONG")
        
        
        if flag != ""{
             NotificationCenter.default.post(name: .address, object: nil)
        }
        else{
            NotificationCenter.default.post(name: .cart, object: nil)
        }
        self.dismiss(animated: true, completion: nil)
       
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        let acController = GMSAutocompleteViewController()
        acController.delegate = self
        
        present(acController, animated: true, completion: nil)
        
        return true
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations[0]
        locationManager.stopUpdatingLocation()
        
        
        //            let marker = GMSMarker()
        mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 8, bearing: 0, viewingAngle: 10)
        marker.position = location.coordinate
        //    //        marker.icon = UIImage (named: "orangePin")
        marker.isDraggable = true
        marker.map = mapView
        
        let LocationCL:CLLocation = CLLocation(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        
        
        geocoder.reverseGeocodeLocation(LocationCL, completionHandler: {(placemarks, error) in
            if (error != nil) {
                print("Error in reverseGeocode")
            }
            
            let placemark = placemarks! as [CLPlacemark]
            if placemark.count > 0 {
                let placemark = placemarks![0]
                let locality = placemark.name ?? ""
                let administrativeArea = placemark.administrativeArea ?? ""
                let country = placemark.country ?? ""
                
                let city = placemark.locality ?? ""
                let postalCode = placemark.postalCode ?? ""
                let userLocationString = "\(locality), \(administrativeArea), \(city), \(country)"
                
                print(userLocationString)
                
                if self.flag == "address"{
                    UserDefaults.standard.set(locality, forKey: "locality")
                    UserDefaults.standard.set(city, forKey: "city")
                    UserDefaults.standard.set(country, forKey: "country")
                    UserDefaults.standard.set(postalCode, forKey: "postal")
                    UserDefaults.standard.set(administrativeArea, forKey: "state")
                }
                self.searchTxt.text = userLocationString
                
                //                self.HouseNumberTxt.text = ""
                //                self.StreetTxt.text = ""
                //                self.CityTxt.text = ""
                //                self.CountryTxt.text = ""
            }
        })
        
        
    }

}


extension LocationViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
  
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return addressList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CELL", for: indexPath) as! locationCollectionViewCell
        
        cell.customView.layer.cornerRadius = cell.customView.frame.height/2
//        cell.img.image = UIImage (named: ImgArr[indexPath.row])
        cell.lbl.text = addressList[indexPath.row].address_type
        cell.customView.setShadow()

        
        
        if cell.lbl.text?.lowercased() == "  home"{
            cell.img.image = UIImage (named: "homeIcon")
        }
        else if cell.lbl.text?.lowercased() == "  work" || cell.lbl.text?.lowercased() == "  office"{
            cell.img.image = UIImage (named: "workNew")
        }
        else{
             cell.img.image = UIImage (named: "otherBlue")
        }
                
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let Lati = addressList[indexPath.row].lat
        let Longi = addressList[indexPath.row].long
        
        let LocationCL:CLLocation = CLLocation(latitude: Double(Lati)!, longitude: Double(Longi)!)
                
        self.searchTxt.text = addressList[indexPath.row].address
        
        mapView.camera = GMSCameraPosition(target: LocationCL.coordinate, zoom: 8, bearing: 0, viewingAngle: 10)
        marker.position = LocationCL.coordinate
//        marker.map = mapView
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
           return 5
       }
       
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        return CGSize(width:126, height:collectionView.frame.size.height);
    }
    
    
    
}


extension LocationViewController: GMSAutocompleteViewControllerDelegate {
    
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        print("Place name: \(String(describing: place.name))")
        print("Place address: .........................\(String(describing: place.formattedAddress))")
        print("Place coordinate: .........................\(String(describing: place.coordinate))")
        
        let LocationCL:CLLocation = CLLocation(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)

        
        Lat = String(LocationCL.coordinate.latitude)
        Long = String(LocationCL.coordinate.longitude)
        
        
        geocoder.reverseGeocodeLocation(LocationCL.self, completionHandler: {(placemarks, error) in
            if (error != nil) {
                print("Error in reverseGeocode")
            }
            
            let placemark = placemarks! as [CLPlacemark]
            if placemark.count > 0 {
                let placemark = placemarks![0]
                let locality = placemark.name ?? ""
                let administrativeArea = placemark.administrativeArea!
                let country = placemark.country ?? ""
                
                let city = placemark.locality ?? ""
                let postalCode = placemark.postalCode ?? ""
                let userLocationString = "\(locality), \(administrativeArea), \(city), \(country)"
                
                print(userLocationString)
                
                if self.flag == "address"{
                    UserDefaults.standard.set(locality, forKey: "locality")
                    UserDefaults.standard.set(city, forKey: "city")
                    UserDefaults.standard.set(country, forKey: "country")
                    UserDefaults.standard.set(postalCode, forKey: "postal")
                    UserDefaults.standard.set(administrativeArea, forKey: "state")
                }
                
                self.searchTxt.text = userLocationString
                
                //                self.HouseNumberTxt.text = ""
                //                self.StreetTxt.text = ""
                //                self.CityTxt.text = ""
                //                self.CountryTxt.text = ""
            }
        })
        
        
        searchTxt.text = place.formattedAddress
        
       
        mapView.camera = GMSCameraPosition(target: LocationCL.coordinate, zoom: 8, bearing: 0, viewingAngle: 10)
        marker.position = LocationCL.coordinate
        //        marker.icon = UIImage (named: "orangePin")
        marker.isDraggable = true
        marker.map = mapView
        
        
        print("Place attributions: \(String(describing: place.attributions))")
        dismiss(animated: true, completion: nil)
        
    }
    
    func mapView(_ mapView: GMSMapView, didEndDragging marker: GMSMarker) {
        print(marker.position)
        
        
        let LocationCL:CLLocation = CLLocation(latitude: marker.position.latitude, longitude: marker.position.longitude)
      
        Lat = String(LocationCL.coordinate.latitude)
        Long = String(LocationCL.coordinate.longitude)
        
        geocoder.reverseGeocodeLocation(LocationCL.self, completionHandler: {(placemarks, error) in
            if (error != nil) {
                print("Error in reverseGeocode")
            }
            
            let placemark = placemarks! as [CLPlacemark]
            if placemark.count > 0 {
                let placemark = placemarks![0]
                let locality = placemark.name!
                let administrativeArea = placemark.administrativeArea!
                let country = placemark.country ?? ""
                
                let city = placemark.locality ?? ""
                let postalCode = placemark.postalCode ?? ""
                let userLocationString = "\(locality), \(administrativeArea), \(city), \(country)"
                
                print(userLocationString)
                
                if self.flag == "address"{
                    UserDefaults.standard.set(locality, forKey: "locality")
                    UserDefaults.standard.set(city, forKey: "city")
                    UserDefaults.standard.set(country, forKey: "country")
                    UserDefaults.standard.set(postalCode, forKey: "postal")
                    UserDefaults.standard.set(administrativeArea, forKey: "state")
                }
                
                self.searchTxt.text = userLocationString
                
                //                self.HouseNumberTxt.text = ""
                //                self.StreetTxt.text = ""
                //                self.CityTxt.text = ""
                //                self.CountryTxt.text = ""
            }
        })
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Error: \(error)")
        dismiss(animated: true, completion: nil)
        
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        
        print("Autocomplete was cancelled.")
        dismiss(animated: true, completion: nil)
    }
    
    
//   func resultsController(_ resultsController:
//   GMSAutocompleteResultsViewController, didAutocompleteWith place:
//   GMSPlace)
//    {
//               resultArray.append(place.name)
//    }
    
}
extension Notification.Name {
    static let cart = Notification.Name("cartAdd")
    static let EnterLocation = Notification.Name("EnterLocation")
    static let noti = Notification.Name("noti")
    static let address = Notification.Name("address")
    static let fetch_address = Notification.Name("fetch_address")
    static let location_address = Notification.Name("location_address")
    static let card = Notification.Name("card")
    static let order = Notification.Name("order")
}
