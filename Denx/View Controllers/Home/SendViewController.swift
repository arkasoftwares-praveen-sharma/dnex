//
//  SendViewController.swift
//  Dnex
//
//  Created by Arka on 08/01/20.
//  Copyright © 2020 Arka. All rights reserved.
//

import UIKit
import CoreLocation
import ADCountryPicker
import SwiftyJSON
import Firebase


class SendViewController: UIViewController,CLLocationManagerDelegate,ADCountryPickerDelegate,UITextFieldDelegate {
    @IBOutlet weak var pickUpView: UIView!
    @IBOutlet weak var pickUpCustom: UIView!
    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var setDateView: UIView!
    @IBOutlet weak var scheduleView: UIView!
    @IBOutlet weak var receiverView: UIView!
    
    @IBOutlet weak var pickView: UILabel!
    @IBOutlet weak var addBtnOutlet: UIButton!
    @IBOutlet weak var closeBtnOutlet: UIButton!
    
    @IBOutlet weak var receiverNameLbl: UILabel!
    @IBOutlet weak var receiverPhoneLbl: UILabel!
    @IBOutlet weak var phoneImg: UIImageView!
    @IBOutlet weak var receiverInitialLbl: UILabel!
    @IBOutlet weak var nextBtnOutlet: UIButton!
    @IBOutlet weak var mapIvon: UIImageView!
    @IBOutlet weak var addressLbl: UILabel!
    
    
    @IBOutlet weak var nowBtnOutlet: UIButton!
    @IBOutlet weak var dateBtnOutlet: UIButton!
    
    @IBOutlet weak var selfScheduleOutlet: UIButton!
    @IBOutlet weak var receiverScheduleOutlet: UIButton!
    
    @IBOutlet weak var receiverLocView: UIView!
    @IBOutlet weak var customView: UIView!
    
    
    @IBOutlet weak var roundView: UILabel!
    @IBOutlet weak var AddOutlet: UIButton!
    
    
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var pickupConstraint: NSLayoutConstraint!
    @IBOutlet weak var dateTxt: UITextField!
    
    @IBOutlet weak var sendBtnOutlet: UIButton!
    
    @IBOutlet weak var sellBtnOutlet: UIButton!
    
    @IBOutlet weak var pickUpAddressTxt: UITextField!
    @IBOutlet weak var countryCode: ACFloatingTextfield!
    @IBOutlet weak var customReceiverView: UIView!
    @IBOutlet weak var receiverLoc: UITextField!
    
    
    
    @IBOutlet weak var addReceiverName: ACFloatingTextfield!
    @IBOutlet weak var addReceiverMobile: ACFloatingTextfield!
    @IBOutlet weak var receiverLocBtnOutlet: UIButton!
    
    @IBOutlet weak var sideBtn: UIButton!
    
    @IBOutlet weak var dropOffView: UIView!
    
    @IBOutlet weak var titleTxt: UILabel!
    
    @IBOutlet weak var senderName: UILabel!
    @IBOutlet weak var senderImage: UIButton!
    @IBOutlet weak var dropLocationTxt: UITextField!
    
    @IBOutlet weak var addDropBtnOutlet: UIButton!
    @IBOutlet weak var sendView: UIView!
    @IBOutlet weak var dropRoundIcon: UILabel!
    @IBOutlet weak var locationView: UIView!
    
    @IBOutlet weak var profileBtnOutlet: UIButton!
    
    let datePicker = UIDatePicker()
    let manager = CLLocationManager()
    let geocoder = CLGeocoder()
    
    var scheduleStr = "self"
    var typeStr = "Send"
    var deliveryTime = "now"
    var LatString = ""
    var LongString = ""
    
    var receiverLatString = ""
    var receiverLongString = ""
    
    @objc func setToPer(){
        
        if UserDefaults.standard.string(forKey: "type") == "pickup"{
            pickUpAddressTxt.text = UserDefaults.standard.string(forKey: "search")
        }
        else if UserDefaults.standard.string(forKey: "type") == "dropOff"{
            dropLocationTxt.text = UserDefaults.standard.string(forKey: "search")
        }
        else{
            receiverLoc.text = UserDefaults.standard.string(forKey: "search")
        }
        receiverLatString = UserDefaults.standard.string(forKey: "LAT") ?? ""
        receiverLongString = UserDefaults.standard.string(forKey: "LONG") ?? ""
        
        
//        UserDefaults.standard.removeObject(forKey: "search")
//        UserDefaults.standard.removeObject(forKey: "type")
        UserDefaults.standard.removeObject(forKey: "LAT")
        UserDefaults.standard.removeObject(forKey: "LONG")
        
    }
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        countryCode.delegate = self
        
        
//        let noti = UserDefaults.standard.string(forKey: "noti") ?? ""
//
//        if noti != ""{
//
//            UserDefaults.standard.
//            let NotificationVC = self.storyboard?.instantiateViewController(withIdentifier: "track") as! TrackOrderViewController
//
//            self.present(NotificationVC, animated: true, completion: nil)
//
//        }
        
        
        self.adjustImageAndTitleOffsetsForButton(button: sendBtnOutlet)
        self.adjustImageAndTitleOffsetsForButton(button: sellBtnOutlet)
        
        let device_token = Messaging.messaging().fcmToken ?? ""

        NetworkManager.update_device_id(uiRef: self, Webservice: "update-device-token", device_token: device_token){sJson in
            
            let sJSON = JSON(sJson)
            print(sJSON)
            
        }
        
        
     let image = UserDefaults.standard.string(forKey: "profile_pic") ?? ""
//     self.profileBtnOutlet.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "defaultProfile"))
       
        
        self.profileBtnOutlet.layer.cornerRadius = self.profileBtnOutlet.frame.height/2
        self.profileBtnOutlet.clipsToBounds = true
        
        
         URLSession.shared.dataTask(with: NSURL(string: image)! as URL, completionHandler: { (data, response, error) -> Void in
                    
                    if error != nil {
                        print(error ?? "No Error")
                        return
                    }
                    DispatchQueue.main.async(execute: { () -> Void in
                        let image = UIImage(data: data!)
        //                activityIndicator.removeFromSuperview()
                        self.profileBtnOutlet.setBackgroundImage(image, for: .normal)                    })
                    
                }).resume()

        
        NotificationCenter.default.addObserver(self, selector: #selector(setToPer), name: .cart, object: nil)
        
        
        GIFHUD.shared.setGif(named: "loader.gif")
        
        pickUpView.setShadow()
        dateView.setShadow()
        scheduleView.setShadow()
        receiverView.setShadow()
        
        pickView.layer.cornerRadius = pickView.frame.height/2
        pickView.clipsToBounds = true
        pickUpCustom.layer.cornerRadius = pickUpCustom.frame.height/2
        pickUpCustom.layer.borderColor = UIColor().HexToColor(hexString: "97a8ab", alpha: 0.5).cgColor
        pickUpCustom.layer.borderWidth = 1
        
        
        
        locationView.layer.cornerRadius = locationView.frame.height/2
        locationView.layer.borderColor = UIColor().HexToColor(hexString: "97a8ab", alpha: 0.5).cgColor
        locationView.layer.borderWidth = 1
        
        
        setDateView.layer.cornerRadius = setDateView.frame.height/2
        setDateView.layer.borderColor = UIColor().HexToColor(hexString: "97a8ab", alpha: 0.5).cgColor
        setDateView.layer.borderWidth = 1
        
        receiverInitialLbl.layer.cornerRadius = receiverInitialLbl.frame.height/2
        receiverInitialLbl.clipsToBounds = true
        addBtnOutlet.layer.cornerRadius = addBtnOutlet.frame.height/2
        nextBtnOutlet.layer.cornerRadius = nextBtnOutlet.frame.height/2
        
        receiverLocView.layer.cornerRadius = receiverLocView.frame.height/2
        receiverLocView.layer.borderColor = UIColor().HexToColor(hexString: "97a8ab", alpha: 0.5).cgColor
        receiverLocView.layer.borderWidth = 1
        
        dropRoundIcon.layer.cornerRadius = dropRoundIcon.frame.height/2
        dropRoundIcon.clipsToBounds = true
        sendView.layer.cornerRadius = 10
        sendView.layer.borderWidth = 1
        sendView.layer.borderColor = UIColor().HexToColor(hexString: "97a8ab", alpha: 0.5).cgColor
        
        
        addDropBtnOutlet.layer.cornerRadius = addDropBtnOutlet.frame.height/2
        
        
        roundView.layer.cornerRadius = roundView.frame.height/2
        roundView.clipsToBounds = true
        
        AddOutlet.layer.cornerRadius = AddOutlet.frame.height/2
        AddOutlet.clipsToBounds = true
        
        dateView.clipsToBounds = true
        //        receiverLocView.isHidden = true
        
        topConstraint.constant = self.view.frame.height - 468
        pickupConstraint.constant = 100
        
        showDatePicker()
        
        
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        if CLLocationManager.authorizationStatus() != .authorizedWhenInUse {
            manager.requestWhenInUseAuthorization()
        }
        
        manager.startUpdatingLocation()
        
        
        selfScheduleOutlet.setImage(UIImage (named: "radioselect"), for: .normal)
        nowBtnOutlet.setImage(UIImage (named: "radioselect"), for: .normal)
        
        
//        let countryDictionary = ["AF":"93", "AL":"355", "DZ":"213","AS":"1", "AD":"376", "AO":"244", "AI":"1","AG":"1","AR":"54","AM":"374","AW":"297","AU":"61","AT":"43","AZ":"994","BS":"1","BH":"973","BD":"880","BB":"1","BY":"375","BE":"32","BZ":"501","BJ":"229","BM":"1","BT":"975","BA":"387","BW":"267","BR":"55","IO":"246","BG":"359","BF":"226","BI":"257","KH":"855","CM":"237","CA":"1","CV":"238","KY":"345","CF":"236","TD":"235","CL":"56","CN":"86","CX":"61","CO":"57","KM":"269","CG":"242","CK":"682","CR":"506","HR":"385","CU":"53","CY":"537","CZ":"420","DK":"45","DJ":"253","DM":"1","DO":"1","EC":"593","EG":"20","SV":"503","GQ":"240","ER":"291","EE":"372","ET":"251","FO":"298","FJ":"679","FI":"358","FR":"33","GF":"594","PF":"689","GA":"241","GM":"220","GE":"995","DE":"49","GH":"233","GI":"350","GR":"30","GL":"299","GD":"1","GP":"590","GU":"1","GT":"502","GN":"224","GW":"245","GY":"595","HT":"509","HN":"504","HU":"36","IS":"354","IN":"91","ID":"62","IQ":"964","IE":"353","IL":"972","IT":"39","JM":"1","JP":"81","JO":"962","KZ":"77","KE":"254","KI":"686","KW":"965","KG":"996","LV":"371","LB":"961","LS":"266","LR":"231","LI":"423","LT":"370","LU":"352","MG":"261","MW":"265","MY":"60","MV":"960","ML":"223","MT":"356","MH":"692","MQ":"596","MR":"222","MU":"230","YT":"262","MX":"52","MC":"377","MN":"976","ME":"382","MS":"1","MA":"212","MM":"95","NA":"264","NR":"674","NP":"977","NL":"31","AN":"599","NC":"687","NZ":"64","NI":"505","NE":"227","NG":"234","NU":"683","NF":"672","MP":"1","NO":"47","OM":"968","PK":"92","PW":"680","PA":"507","PG":"675","PY":"595","PE":"51","PH":"63","PL":"48","PT":"351","PR":"1","QA":"974","RO":"40","RW":"250","WS":"685","SM":"378","SA":"966","SN":"221","RS":"381","SC":"248","SL":"232","SG":"65","SK":"421","SI":"386","SB":"677","ZA":"27","GS":"500","ES":"34","LK":"94","SD":"249","SR":"597","SZ":"268","SE":"46","CH":"41","TJ":"992","TH":"66","TG":"228","TK":"690","TO":"676","TT":"1","TN":"216","TR":"90","TM":"993","TC":"1","TV":"688","UG":"256","UA":"380","AE":"971","GB":"44","US":"1", "UY":"598","UZ":"998", "VU":"678", "WF":"681","YE":"967","ZM":"260","ZW":"263","BO":"591","BN":"673","CC":"61","CD":"243","CI":"225","FK":"500","GG":"44","VA":"379","HK":"852","IR":"98","IM":"44","JE":"44","KP":"850","KR":"82","LA":"856","LY":"218","MO":"853","MK":"389","FM":"691","MD":"373","MZ":"258","PS":"970","PN":"872","RE":"262","RU":"7","BL":"590","SH":"290","KN":"1","LC":"1","MF":"590","PM":"508","VC":"1","ST":"239","SO":"252","SJ":"47","SY":"963","TW":"886","TZ":"255","TL":"670","VE":"58","VN":"84","VG":"284","VI":"340"]
        
        
//        if let countryCodeGet = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
//            print(countryCodeGet)
            countryCode.text = "+" + "971"
//        }
        
        countryCode.isEnabled = false
        
        let dropoff = UserDefaults.standard.string(forKey: "order_id") ?? ""
        print(dropoff)
        if dropoff != ""{
            
            self.customView.isHidden = false
            self.dropOffView.isHidden = false
            self.customView.alpha = 0.0
            
            UIView.animate(withDuration: 1.0, delay: 0, options: .curveEaseInOut, animations: {
                self.customView.alpha = 1.0
            })
            
//            senderName.text = UserDefaults.standard.string(forKey: "sender_name")
            
            
            
            let dropoff = UserDefaults.standard.string(forKey: "order_id") ?? ""
                  GIFHUD.shared.show(withOverlay: true, duration: 2)
                  NetworkManager.fetchUser(uiRef: self, webservice: "get-order-user-details", order_id: dropoff){sJson in

                      let sJSON = JSON(sJson)
                      print(sJSON)
                      CommonFunctions.delay(1.0){
                          GIFHUD.shared.dismiss()
                          if sJSON["status"].intValue == 200{
                            
                            self.senderName.text = sJSON["data"]["get_user_details"]["name"].stringValue
                            self.titleTxt.text = "\(sJSON["data"]["get_user_details"]["name"].stringValue) wants to send an item for you, please set your drop off location.".localized
                            
                          }
                          else{
                              CommonFunctions.showErrorAlert(uiRef: self, message: sJSON["message"].stringValue)
                          }
                      }
                  }
        }
        else{
            self.dropOffView.isHidden = true
            if self.revealViewController() != nil {
                
                self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
                sideBtn.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
                self.revealViewController().rearViewRevealWidth = self.view.frame.width
            }
        }
        
        
        UserDefaults.standard.set("home", forKey: "sideIcon")
        
        
    }
    
    
    @IBAction func dropAddressBtn(_ sender: Any) {
        UserDefaults.standard.set("dropOff", forKey: "type")
    }
    
    
    @IBAction func addDropOff(_ sender: Any) {
        
        let dropoff = UserDefaults.standard.string(forKey: "order_id") ?? ""
        GIFHUD.shared.show(withOverlay: true, duration: 2)
        NetworkManager.addDropOff(uiRef: self, webservice: "order-update-receiver-address", order_id: dropoff, lat: receiverLatString, long: receiverLongString, address: dropLocationTxt.text!){sJson in
            
            let sJSON = JSON(sJson)
            print(sJSON)
            CommonFunctions.delay(1.0){
                GIFHUD.shared.dismiss()
                if sJSON["status"].intValue == 200{
                    
                    self.customView.alpha = 1.0
                    //          forgotEmailTxt.text = ""
                    UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseInOut, animations: {
                        self.customView.alpha = 0.0
                    })
                    
                    self.customView.isHidden = true
                    self.dropOffView.isHidden = true
                    
                    UserDefaults.standard.removeObject(forKey: "order_id")
                }
                else{
                    CommonFunctions.showErrorAlert(uiRef: self, message: sJSON["message"].stringValue)
                }
            }
        }
    }
    
    @IBAction func pickUpbtnClicked(_ sender: Any) {
        UserDefaults.standard.set("pickup", forKey: "type")
        self.view.endEditing(true)
    }
    
    @IBAction func receiverBtnClicked(_ sender: Any) {
        UserDefaults.standard.set("receiver", forKey: "type")
        self.view.endEditing(true)
    }
    
   private func adjustImageAndTitleOffsetsForButton (button: UIButton) {

        let spacing: CGFloat = 6.0

        let imageSize = button.imageView!.frame.size

    button.titleEdgeInsets = UIEdgeInsets(top: 0, left: -imageSize.width, bottom: -(imageSize.height + spacing), right: 0)

        let titleSize = button.titleLabel!.frame.size

    button.imageEdgeInsets = UIEdgeInsets(top: -(titleSize.height + spacing), left: 0, bottom: 0, right: -titleSize.width)
    }
    
    @IBAction func nextBtnClicked(_ sender: Any) {
        
        var dateStr = ""
        var LatRec = ""
        var LongRec = ""
        var address = ""
        
        if deliveryTime == "now"{
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy"
            formatter.locale = Locale(identifier: "en")
            dateStr = formatter.string(from: Date())
            print(dateStr)
        }
        else{
            dateStr = dateTxt.text!
        }
        
        /// Self Schedule
        
        if scheduleStr == "self"{
            LatRec = receiverLatString
            LongRec = receiverLongString
            address = addressLbl.text!
        }
        else{
            LatRec = ""
            LongRec = ""
            address = ""
        }
        
        
        if addBtnOutlet.isHidden == false{
            CommonFunctions.showErrorAlert(uiRef: self, message: "Enter receiver's details !!".localized)
        }
        else if deliveryTime == "later" && dateStr == ""{
            CommonFunctions.showErrorAlert(uiRef: self, message: "Select delivery date !!".localized)
        }
        else{
            
            var LAT = ""
            var LONG = ""
            
            
            let pickLat = UserDefaults.standard.string(forKey: "pickLAT") ?? ""
            let pickLong = UserDefaults.standard.string(forKey: "pickLONG") ?? ""
            
            if pickLat != ""{
                
                
                UserDefaults.standard.removeObject(forKey: "pickLAT")
                UserDefaults.standard.removeObject(forKey: "pickLONG")
//                UserDefaults.standard.set(pickLat, forKey: "pickLAT")
//                          UserDefaults.standard.set(pickLong, forKey: "pickLONG")
                
                
                LAT = pickLat
                LONG = pickLong
            }
            else{
                LAT = self.LatString
                LONG = self.LongString
            }
            
            GIFHUD.shared.show(withOverlay: true, duration: 2)
            NetworkManager.initiateOrder(uiRef: self, Webservice: "create-order", type: typeStr, pickup_lat: LAT, pickup_lng: LONG, pickup_address: pickUpAddressTxt.text!, pickup_date_time: dateStr, receiver_name: receiverNameLbl.text!, receiver_country_code:countryCode.text!, receiver_mobile: receiverPhoneLbl.text!,receiver_lat: LatRec,receiver_lng: LongRec,receiver_address: address){sJson in
                
                CommonFunctions.delay(1.0){
                    GIFHUD.shared.dismiss()
                    let sJSON = JSON(sJson)
                    print(sJSON)
                    
                    if sJSON["status"].intValue == 200{
                        UserDefaults.standard.set(sJSON["data"]["id"].stringValue, forKey: "id_order")
                        UserDefaults.standard.set(sJSON["data"]["normal_delivery_charge"].stringValue, forKey: "normal")
                        UserDefaults.standard.set(sJSON["data"]["fast_delivery_charge"].stringValue, forKey: "fast")
                        UserDefaults.standard.set(self.typeStr, forKey: "type")
                        self.performSegue(withIdentifier: "nextSegue", sender: nil)
                    }
                    else{
                        CommonFunctions.showErrorAlert(uiRef: self, message: sJSON["message"].stringValue)
                    }
                }
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "nextSegue"{
            let Object = segue.destination as! ItemSendViewController
            Object.schedule = scheduleStr
            Object.deliveryTime = deliveryTime
            Object.type = typeStr
        }
    }
    
    
    func countryPicker(_ picker: ADCountryPicker, didSelectCountryWithName name: String, code: String, dialCode: String) {
        print(name)
        countryCode.text = dialCode
        self.dismiss(animated: true, completion: nil)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
//        if textField == countryCode{
//            countryCode.endEditing(true)
//            let picker = ADCountryPicker()
//            picker.delegate = self
//
//            let pickerNavigationController = UINavigationController(rootViewController: picker)
//            self.present(pickerNavigationController, animated: true, completion: nil)
//        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations[0]
        manager.stopUpdatingLocation()
        
        CommonFunctions.delay(1.0){
            self.LatString = String(location.coordinate.latitude)
            self.LongString = String(location.coordinate.longitude)
            
        }
        
        
        geocoder.reverseGeocodeLocation(location, completionHandler: {(placemarks, error) in
            if (error != nil) {
                print("Error in reverseGeocode")
            }
            
            let placemark = placemarks! as [CLPlacemark]
            if placemark.count > 0 {
                let placemark = placemarks![0]
                let locality = placemark.name!
                let administrativeArea = placemark.administrativeArea!
                let country = placemark.country!
                
                let city = placemark.locality!
                
                let userLocationString = "\(locality), \(administrativeArea), \(city), \(country)"
                
                print(userLocationString)
                
                self.pickUpAddressTxt.text = userLocationString
                
                self.dropLocationTxt.text = userLocationString
                
                //                self.HouseNumberTxt.text = ""
                //                self.StreetTxt.text = ""
                //                self.CityTxt.text = ""
                //                self.CountryTxt.text = ""
                
            }
        })
    }
    
    
    @IBAction func sendBtnClicked(_ sender: Any) {
        
        sendBtnOutlet.setBackgroundImage(UIImage (named: "group341"), for: .normal)
        sellBtnOutlet.setBackgroundImage(UIImage (named: "group338"), for: .normal)
        typeStr = "Send"
    }
    
    
    @IBAction func sellBtnClicked(_ sender: Any) {
        
        sendBtnOutlet.setBackgroundImage(UIImage (named: "send-1"), for: .normal)
        sellBtnOutlet.setBackgroundImage(UIImage (named: "sell_active"), for: .normal)
        typeStr = "Sell"
    }
    
    
    @IBAction func nowOutlet(_ sender: Any) {
        
        nowBtnOutlet.setImage(UIImage (named: "radioselect"), for: .normal)
        dateBtnOutlet.setImage(UIImage (named: "radio"), for: .normal)
        pickupConstraint.constant = 100
        deliveryTime = "now"
    }
    
    @IBAction func dateBtnClicked(_ sender: Any) {
        
        nowBtnOutlet.setImage(UIImage (named: "radio"), for: .normal)
        dateBtnOutlet.setImage(UIImage (named: "radioselect"), for: .normal)
        pickupConstraint.constant = 174
        deliveryTime = "later"
    }
    
    @IBAction func selfScheduleBtnClicked(_ sender: Any) {
        
        selfScheduleOutlet.setImage(UIImage (named: "radioselect"), for: .normal)
        receiverScheduleOutlet.setImage(UIImage (named: "radio"), for: .normal)
        let height = self.view.frame.height - customReceiverView.frame.height
        print(self.view.frame.height)
        print(customReceiverView.frame.height)
        print(height)
        topConstraint.constant = self.view.frame.height - 468
        receiverLocView.isHidden = false
        
        scheduleStr = "self"
        
        receiverInitialLbl.isHidden = true
        receiverNameLbl.isHidden = true
        receiverPhoneLbl.isHidden = true
        phoneImg.isHidden = true
        
        mapIvon.isHidden = true
        addressLbl.isHidden = true
        
        closeBtnOutlet.isHidden = true
        addBtnOutlet.isHidden = false
 
    
    }
    @IBAction func receiverSchedulrBtnclicked(_ sender: Any) {
        
        selfScheduleOutlet.setImage(UIImage (named: "radio"), for: .normal)
        receiverScheduleOutlet.setImage(UIImage (named: "radioselect"), for: .normal)
        topConstraint.constant = self.view.frame.height - 400
        receiverLocView.isHidden = true
        scheduleStr = "receiver"
        
        receiverInitialLbl.isHidden = true
        receiverNameLbl.isHidden = true
        receiverPhoneLbl.isHidden = true
        phoneImg.isHidden = true
        
        mapIvon.isHidden = true
        addressLbl.isHidden = true
        
        closeBtnOutlet.isHidden = true
        addBtnOutlet.isHidden = false
        
    }
    
    
    @IBAction func addBtnClicked(_ sender: Any) {
        
        self.view.endEditing(true)

        self.customView.isHidden = false
        self.customView.alpha = 0.0
        
        UIView.animate(withDuration: 1.0, delay: 0, options: .curveEaseInOut, animations: {
            self.customView.alpha = 1.0
        })
    }
    
    
    @IBAction func removeBtnClicked(_ sender: Any) {
        
        receiverInitialLbl.isHidden = true
        receiverNameLbl.isHidden = true
        receiverPhoneLbl.isHidden = true
        phoneImg.isHidden = true
        
        mapIvon.isHidden = true
        addressLbl.isHidden = true
        
        closeBtnOutlet.isHidden = true
        addBtnOutlet.isHidden = false
        
    }
    
    @IBAction func cancelBtnClicked(_ sender: Any) {
        
        self.customView.alpha = 1.0
        //          forgotEmailTxt.text = ""
        UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseInOut, animations: {
            self.customView.alpha = 0.0
        })
        
        self.customView.isHidden = true
        
    }
    
    
    @IBAction func addUserInfoBtnClicked(_ sender: Any) {
        
        if addReceiverName.text == ""{
            CommonFunctions.showErrorAlert(uiRef: self, message: "Enter receiver's name !!".localized)
        }
        else if addReceiverMobile.text == ""{
            CommonFunctions.showErrorAlert(uiRef: self, message: "Enter receiver's mobile number !!".localized)
        }
        else if addReceiverMobile.text!.count < 4 || addReceiverMobile.text!.count > 12{
            CommonFunctions.showErrorAlert(uiRef: self,message: "Mobile number must be of 4 - 12 digits !!".localized)
        }
        else if receiverLoc.text == "" && scheduleStr == "self"{
            CommonFunctions.showErrorAlert(uiRef: self, message: "Enter receiver's location !!".localized)
        }
        else{
            
            receiverInitialLbl.isHidden = false
            receiverNameLbl.isHidden = false
            receiverPhoneLbl.isHidden = false
            phoneImg.isHidden = false
            
            if selfScheduleOutlet.image(for: .normal) == #imageLiteral(resourceName: "radioselect"){
                mapIvon.isHidden = false
                addressLbl.isHidden = false
            }
            else{
                mapIvon.isHidden = true
                addressLbl.isHidden = true
            }
            
            closeBtnOutlet.isHidden = false
            addBtnOutlet.isHidden = true
            
            customView.isHidden = true
            
            receiverNameLbl.text = addReceiverName.text!
            receiverPhoneLbl.text = addReceiverMobile.text!
            addressLbl.text = receiverLoc.text!
            
            let trimmedString = addReceiverName.text!.trimmingCharacters(in: .whitespacesAndNewlines)

            
            if trimmedString.contains(" "){
                
                let initials = addReceiverName.text!.components(separatedBy: " ").reduce("") { ($0 == "" ? "" : "\($0.first!)") + "\($1.first!)" }
                           
                           receiverInitialLbl.text = initials
            }
            else{
                let initials = String(addReceiverName.text!.prefix(1))
                           
                receiverInitialLbl.text = initials
            }
            
           
            
            addReceiverMobile.text = ""
            addReceiverName.text = ""
            receiverLoc.text = ""
        }
    }
    
    
    func showDatePicker(){
        //Formate Date
        
        var components = DateComponents()
        components.day = +1
        datePicker.datePickerMode = .date
        datePicker.minimumDate = Calendar.current.date(byAdding: components, to: Date())
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Cancel".localized, style: .plain, target: self, action: #selector(cancelDatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Done".localized, style: .plain, target: self, action: #selector(donedatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        dateTxt.inputAccessoryView = toolbar
        dateTxt.inputView = datePicker
        
    }
    
    @objc func donedatePicker(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        
        dateTxt.text = formatter.string(from: datePicker.date)
        
        self.view.endEditing(true)
        
    }
    
    @objc func cancelDatePicker(){
        
        self.view.endEditing(true)
        
    }
}
