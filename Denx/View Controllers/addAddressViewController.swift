//
//  addAddressViewController.swift
//  Dnex
//
//  Created by Arka on 18/03/20.
//  Copyright © 2020 Arka. All rights reserved.
//

import UIKit
import SwiftyJSON

class addAddressViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var addBtnOutlet: UIButton!
    
    @IBOutlet weak var addressTxt: ACFloatingTextfield!
    
    @IBOutlet weak var cityTxt: ACFloatingTextfield!
    
    @IBOutlet weak var stateTxt: ACFloatingTextfield!
    
    @IBOutlet weak var countryTxt: ACFloatingTextfield!
    
    @IBOutlet weak var zipCodeTxt: ACFloatingTextfield!
    
    @IBOutlet weak var addressType: ACFloatingTextfield!
    
    @IBOutlet weak var titleLbl: UILabel!
    var flag = ""
    
    var state = ""
    var city = ""
    var country = ""
    var pincode = ""
    var locality = ""
    var address_type = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        addressTxt.delegate = self
        
        addBtnOutlet.layer.cornerRadius = addBtnOutlet.frame.height/2
        NotificationCenter.default.addObserver(self, selector: #selector(setToPeru), name: .address, object: nil)
        
        
        if flag != ""{
            addBtnOutlet.setTitle("UPDATE".localized, for: .normal)
            titleLbl.text = "EDIT ADDRESS".localized
            
            
            city = city .replacingOccurrences(of: ", ", with: "")
            state = state .replacingOccurrences(of: ", ", with: "")
            country = country .replacingOccurrences(of: ", ", with: "")
            pincode = pincode .replacingOccurrences(of: ", ", with: "")
            
            
            cityTxt.text = city
            stateTxt.text = state
            countryTxt.text = country
            addressTxt.text = locality
            addressType.text = address_type
            zipCodeTxt.text = pincode
            
            
        }
        else{
            addBtnOutlet.setTitle("ADD".localized, for: .normal)
            titleLbl.text = "ADD NEW ADDRESS".localized
        }
        
        
    }
    
     @objc func setToPeru(){
//        print(addr)
        
//        UserDefaults.standard.set(locality, forKey: "locality")
//                           UserDefaults.standard.set(city, forKey: "city")
//                           UserDefaults.standard.set(country, forKey: "country")
//                           UserDefaults.standard.set(postalCode, forKey: "postal")
//                           UserDefaults.standard.set(administrativeArea, forKey: "state")
        
        let city = UserDefaults.standard.string(forKey: "city") ?? ""
        let state = UserDefaults.standard.string(forKey: "state") ?? ""
        let country = UserDefaults.standard.string(forKey: "country") ?? ""
        let zip = UserDefaults.standard.string(forKey: "postal") ?? ""
        let locality = UserDefaults.standard.string(forKey: "locality") ?? ""
        
        
        
        cityTxt.text = city
        stateTxt.text = state
        countryTxt.text = country
        zipCodeTxt.text = zip
        addressTxt.text = locality
        
        if city == ""{
            cityTxt.isEnabled = true
        }
        else{
            cityTxt.isEnabled = false
        }
        
        //// STATE
        if state == ""{
            stateTxt.isEnabled = true
        }
        else{
            stateTxt.isEnabled = false
        }
        
        //// COUNTRY
        if country == ""{
            countryTxt.isEnabled = true
        }
        else{
            countryTxt.isEnabled = false
        }
        
        
        //// ZIP
        if zip == ""{
            zipCodeTxt.isEnabled = true
        }
        else{
            zipCodeTxt.isEnabled = false
        }
        
    }
    
    
    @IBAction func backBtnClicked(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func addBtnClicked(_ sender: Any) {
        
        self.view.endEditing(true)
        
        
        //        @IBOutlet weak var addressTxt: ACFloatingTextfield!
        //
        //         @IBOutlet weak var cityTxt: ACFloatingTextfield!
        //
        //         @IBOutlet weak var stateTxt: ACFloatingTextfield!
        //
        //         @IBOutlet weak var countryTxt: ACFloatingTextfield!
        //
        //         @IBOutlet weak var zipCodeTxt: ACFloatingTextfield!
        //
        //         @IBOutlet weak var addressType: ACFloatingTextfield!
        
        
        if addressTxt.text == ""{
            CommonFunctions.showErrorAlert(uiRef: self, message: "Enter address !!".localized)
        }
        else if cityTxt.text == ""{
            CommonFunctions.showErrorAlert(uiRef: self, message: "Enter city !!".localized)
        }
        else if stateTxt.text == ""{
            CommonFunctions.showErrorAlert(uiRef: self, message: "Enter state !!".localized)
        }
        else if countryTxt.text == ""{
            CommonFunctions.showErrorAlert(uiRef: self, message: "Enter country !!".localized)
        }
        else if zipCodeTxt.text == ""{
            CommonFunctions.showErrorAlert(uiRef: self, message: "Enter postal code !!".localized)
        }
        else if addressType.text == ""{
            CommonFunctions.showErrorAlert(uiRef: self, message: "Enter address type !!".localized)
        }
        else{
            
            var apiName = ""
            
            if flag != ""{
                apiName = "update-user-address"
            }
            else{
                apiName = "add-user-address"
            }
            let lat = UserDefaults.standard.string(forKey: "LAT") ?? ""
            let long = UserDefaults.standard.string(forKey: "LONG") ?? ""
            
            NetworkManager.save_address(uiRef: self, Webservice: apiName, address_type: addressType.text!, address: addressTxt.text!, latitude: lat, longitude: long, city: cityTxt.text!, state: stateTxt.text!, country: countryTxt.text!, pincode: zipCodeTxt.text!,address_id: flag){sJson in
                
                let sJSON = JSON(sJson)
                print(sJSON)
                
                if sJSON["status"].intValue == 200{
                    if self.flag != ""{
                        self.showAlert(message: "Address updated successfully !!".localized)
                    }
                    else{
                        self.showAlert(message: "Address added successfully !!".localized)
                    }
                    
                }
                else{
                    CommonFunctions.showErrorAlert(uiRef: self, message: sJSON["message"].stringValue)
                }
            }
        }
    }
    func showAlert( message: String) {
        let alertController = UIAlertController(title: "Message".localized, message:
               message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: {action in
               
            self.dismiss(animated: true, completion: nil)
               NotificationCenter.default.post(name: .fetch_address, object: nil)
           }))
           
         
           self.present(alertController, animated: true, completion: nil)
           
       }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == addressTxt{
            self.view.endEditing(true)
            self.performSegue(withIdentifier: "map", sender: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "map"{
            let Object = segue.destination as! LocationViewController
            Object.flag = "address"
        }
    }
    
}
