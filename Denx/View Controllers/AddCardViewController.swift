//
//  AddCardViewController.swift
//  Dnex
//
//  Created by Arka on 08/01/20.
//  Copyright © 2020 Arka. All rights reserved.
//

import UIKit
import SwiftyJSON


class AddCardViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var addBtnOutlet: UIButton!
    
    @IBOutlet weak var skipBtnOutlet: UIButton!
    @IBOutlet weak var cardNumber: ACFloatingTextfield!
    @IBOutlet weak var CardHolderName: ACFloatingTextfield!
    
    @IBOutlet weak var expiryDate: ACFloatingTextfield!
    @IBOutlet weak var cvv: ACFloatingTextfield!
    @IBOutlet weak var sideBtn: UIButton!
    
    var card_type = ""
    var card_id = ""
    var flag = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
         GIFHUD.shared.setGif(named: "loader.gif")
        headerView.setShadow()
        
         addBtnOutlet.layer.cornerRadius = addBtnOutlet.frame.height/2
        
        expiryDate.delegate = self
        cardNumber.delegate = self
        CardHolderName.delegate = self
        cvv.delegate = self
        
        
        
        let home = UserDefaults.standard.string(forKey: "sideIcon") ?? ""
        
        if home == ""{
            sideBtn.isHidden = true
            skipBtnOutlet.isHidden = false
        }
        else{
            sideBtn.isHidden = false
            skipBtnOutlet.isHidden = true
        }
        
//        NetworkManager.getProfile(uiRef: self, Webservice: "get-profile"){sJson in
//
//            let sJSON = JSON(sJson)
//            print(sJSON)
//
//            if sJSON["status"].intValue == 200{
//
//
//
//            }
//            else{
//                CommonFunctions.showErrorAlert(uiRef: self, message: sJSON["message"].stringValue)
//            }
//        }
        
        
        
        
        GIFHUD.shared.show(withOverlay: true, duration: 2)
        NetworkManager.fetch_token(uiRef: self){sJson in
            
            let sJSON = JSON(sJson)
            
            print(sJSON)
            
            let access_token = sJSON["access_token"].stringValue
            
            if access_token != ""{
                UserDefaults.standard.set(access_token, forKey: "paypal_token")
                
                NetworkManager.fetch_card_id(uiRef: self, Webservice: "get-user-card-id"){sJson in
                    
                    let sJSON = JSON(sJson)
                    print(sJSON)
                    
                    if sJSON["status"].intValue == 200{
                        
                        CommonFunctions.delay(0.6){
                        for item in sJSON["data"].arrayValue{
                            self.card_id = item["card_id"].stringValue
                            
                         
                            NetworkManager.fetch_card(uiRef: self, card_id: self.card_id){sJson in
                               
                                    let sJSON = JSON(sJson)
                                    print(sJSON)
                                    
//                                    self.card_number = sJSON["number"].stringValue
                                    
                                if sJSON["number"].stringValue != ""{
                                    
                                    self.cardNumber.text = sJSON["number"].stringValue
                                    self.CardHolderName.text = sJSON["first_name"].stringValue + " " + sJSON["last_name"].stringValue
                                    self.expiryDate.text = sJSON["expire_month"].stringValue + "/" + sJSON["expire_year"].stringValue
                                    
                                    self.card_type = sJSON["type"].stringValue
                                }
                                   
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @IBAction func backBtnClicked(_ sender: Any) {
        if flag != ""{
            self.dismiss(animated: true, completion: nil)
        }
        else{
            self.performSegue(withIdentifier: "success", sender: nil)
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
       
        if textField == cardNumber{
           return true
        }
       else if textField == expiryDate{
            let maxLength = 7
            
            let currentString: NSString = textField.text as! NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            
            print(newString)
            
            if newString.length == 3 {
                if expiryDate.text!.contains("/"){
                    
                }
                else{
                    expiryDate.text = (currentString as String) + "/"
                }
            }
            return newString.length <= maxLength
        }
        else if textField == CardHolderName{
            return true
        }
       
        else{
            return textField.text!.count <= 2
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == cardNumber{
            let card = checkCardNumber(input: cardNumber.text!)
            print(card.type.rawValue)
            card_type = card.type.rawValue
            
            cardNumber.text = card.formatted
            
            if card.valid == false{
                CommonFunctions.showErrorAlert(uiRef: self, message: "Enter a valid card number !!".localized)
            }
            
        }
    }
    
    
    @IBAction func addBtnClicked(_ sender: Any) {
        
        self.view.endEditing(true)
        
        if cardNumber.text == ""{
            CommonFunctions.showErrorAlert(uiRef: self, message: "Enter card number !!".localized)
        }
        else if cardNumber.text!.contains("x"){
            CommonFunctions.showErrorAlert(uiRef: self, message: "Enter full card number to edit !!".localized)
        }
        else if CardHolderName.text == ""{
            CommonFunctions.showErrorAlert(uiRef: self, message: "Enter name on card !!".localized)
        }
        else if expiryDate.text == ""{
            CommonFunctions.showErrorAlert(uiRef: self, message: "Enter date of expiry !!".localized)
        }
        else if cvv.text == ""{
            CommonFunctions.showErrorAlert(uiRef: self, message: "Enter CVV !!".localized)
        }
            
        else{
            
            
//            let enteredDate = expiryDate.text ?? ""
//
//            let dateArr = enteredDate.components(separatedBy: "/")
//
//            let monStr = Int(dateArr[0])
//            let yearStr = Int(dateArr[1])
//
//            let now = Date()
//            let dateFormatter = DateFormatter()
//            dateFormatter.dateFormat = "LL"
//            let nameOfMonth = Int(dateFormatter.string(from: now))
//            print(nameOfMonth)
//
//
//
//            let dateFormatter1 = DateFormatter()
//            dateFormatter1.dateFormat = "yyyy"
//            let nameOfYear = Int(dateFormatter1.string(from: now))
//
//            print(nameOfYear)
//
            
            let dateFormatter2 = DateFormatter()
            dateFormatter2.dateFormat = "MM/yyyy"
            let enteredDate2 = dateFormatter2.date(from: expiryDate.text!)!
            let endOfMonth = Calendar.current.date(byAdding: .month, value: 1, to: enteredDate2)!
            let now2 = Date()
            if (endOfMonth < now2) {
                print("Expired - \(enteredDate2) - \(endOfMonth)")
                CommonFunctions.showErrorAlert(uiRef: self, message: "Invalid expiry date !!".localized)
                
            } else {
                // valid
                print("valid - now: \(now2) entered: \(enteredDate2)")
                
                GIFHUD.shared.show(withOverlay: true, duration: 2)
                NetworkManager.fetch_token(uiRef: self){sJson in
                    
                    let sJSON = JSON(sJson)
                    
                    print(sJSON)
                    
                    let access_token = sJSON["access_token"].stringValue
                    
                    if access_token != ""{
                        UserDefaults.standard.set(access_token, forKey: "paypal_token")
                        
                        let exp_arr = self.expiryDate.text?.components(separatedBy: "/")
                        let exp_mon = exp_arr![0]
                        let exp_year = exp_arr![1]
                        
                        let NameArr = self.CardHolderName.text?.components(separatedBy: " ")
                        var first = ""
                        var last = ""
                        
                        if NameArr?.count == 2{
                            first = NameArr![0]
                            last = NameArr![1]
                        }
                        else{
                            first = self.CardHolderName.text!
                            last = " "
                        }
                        
                        let cardNumber = self.cardNumber.text!.replacingOccurrences(of: " ", with: "")
                        
                        NetworkManager.save_card(uiRef: self, card_number: cardNumber, month: exp_mon, year: exp_year, cvv: self.cvv.text!,type: self.card_type.lowercased(),first_name: first,last_name: last){ sJson in
                            
                            let sJSON = JSON(sJson)
                            print(sJSON)
                            
                            print(sJSON["id"].stringValue)
                            
                            let card_id = sJSON["id"].stringValue
                            
                            if self.card_id != ""{
                                
                                NetworkManager.delete_card(uiRef: self, card_id: self.card_id){sJson in
                                    CommonFunctions.delay(0.6){
                                        let sJSON = JSON(sJson)
                                        print(sJSON)
                                        
                                        NetworkManager.save_card_id(uiRef: self,Webservice: "store-user-card-id", card_id: card_id){sJson in
                                            CommonFunctions.delay(1.0){
                                                let sJSON = JSON(sJson)
                                                print(sJSON)
                                                
                                                if sJSON["status"].intValue == 200{
                                                    if self.flag != ""{
                                                        NotificationCenter.default.post(name: .card, object: nil)
                                                        self.dismiss(animated: true, completion: nil)
                                                    }
                                                    else{
                                                        self.performSegue(withIdentifier: "success", sender: nil)
                                                    }
                                                }
                                                else{
                                                    CommonFunctions.showErrorAlert(uiRef: self, message: sJSON["message"].stringValue)
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else{
                                NetworkManager.save_card_id(uiRef: self,Webservice: "store-user-card-id", card_id:
                                card_id){sJson in
                                    CommonFunctions.delay(1.0){     let sJSON = JSON(sJson)
                                        print(sJSON)
                                        
                                        if sJSON["status"].intValue == 200{
                                            if self.flag != ""{
                                                NotificationCenter.default.post(name: .card, object: nil)
                                                self.dismiss(animated: true, completion: nil)
                                            }
                                            else{
                                                self.performSegue(withIdentifier: "success", sender: nil)
                                            }
                                        }
                                        else{
                                            CommonFunctions.showErrorAlert(uiRef: self, message: sJSON["message"].stringValue)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                
            }
            
//            if monStr! < nameOfMonth! || yearStr! < nameOfYear!{
//
//            }
//
//            else{
//
//                //                NetworkManager.BankDetails(uiRef: self, Webservice: "add-shipper-bank-detail", bank_details_id: "", bank_name: , account_holder_name: <#T##String#>, account_number: <#T##String#>, iban_number: <#T##String#>, completion: <#T##(JSON) -> Void#>)
//                //
//
//            }
        }
    }
    
    enum CardType: String {
        case Unknown, Amex, Visa, MasterCard, Diners, Discover, JCB, Elo, Hipercard, UnionPay
        
        static let allCards = [Amex, Visa, MasterCard, Diners, Discover, JCB, Elo, Hipercard, UnionPay]
        
        var regex : String {
            switch self {
            case .Amex:
                return "^3[47][0-9]{5,}$"
            case .Visa:
                return "^4[0-9]{6,}([0-9]{3})?$"
            case .MasterCard:
                return "^(5[1-5][0-9]{4}|677189)[0-9]{5,}$"
            case .Diners:
                return "^3(?:0[0-5]|[68][0-9])[0-9]{4,}$"
            case .Discover:
                return "^6(?:011|5[0-9]{2})[0-9]{3,}$"
            case .JCB:
                return "^(?:2131|1800|35[0-9]{3})[0-9]{3,}$"
            case .UnionPay:
                return "^(62|88)[0-9]{5,}$"
            case .Hipercard:
                return "^(606282|3841)[0-9]{5,}$"
            case .Elo:
                return "^((((636368)|(438935)|(504175)|(451416)|(636297))[0-9]{0,10})|((5067)|(4576)|(4011))[0-9]{0,12})$"
            default:
                return ""
            }
        }
    }
    
    
    func matchesRegex(regex: String!, text: String!) -> Bool {
        do {
            let regex = try NSRegularExpression(pattern: regex, options: [.caseInsensitive])
            let nsString = text as NSString
            let match = regex.firstMatch(in: text, options: [], range: NSMakeRange(0, nsString.length))
            return (match != nil)
        } catch {
            return false
        }
    }
    
    func luhnCheck(number: String) -> Bool {
        var sum = 0
        let digitStrings = (number.reversed()).map { String($0) }
        
        for tuple in digitStrings.enumerated() {
            guard let digit = Int(tuple.element) else { return false }
            let odd = tuple.offset % 2 == 1
            
            switch (odd, digit) {
            case (true, 9):
                sum += 9
            case (true, 0...8):
                sum += (digit * 2) % 9
            default:
                sum += digit
            }
        }
        
        return sum % 10 == 0
    }
    
    
    
    func checkCardNumber(input: String) -> (type: CardType, formatted: String, valid: Bool) {
        // Get only numbers from the input string
                
        let numberOnly = input.replacingOccurrences(of:"[^0-9]", with: "", options: .regularExpression)
        
        var type: CardType = .Unknown
        var formatted = ""
        var valid = false
        
        // detect card type
        for card in CardType.allCards {
            if (matchesRegex(regex: card.regex, text: numberOnly)) {
                type = card
                break
            }
        }
        
        // check validity
        valid = luhnCheck(number: numberOnly)
        
        // format
        var formatted4 = ""
        for char in numberOnly {
            if formatted4.count == 4 {
                formatted += formatted4 + " "
                formatted4 = ""
            }
            formatted4.append(char)
        }
        
        formatted += formatted4 // the rest
        
        // return the tuple
        return (type, formatted, valid)
    }

}
