//
//  CommonFunctions.swift
//  AL-Rawdeten
//
//  Created by Arka on 23/09/19.
//  Copyright © 2019 Arka. All rights reserved.
//

import UIKit

class CommonFunctions: NSObject {
    static func showErrorAlert(uiRef: UIViewController, message :String){
        //Alert Message Code
        let alert = UIAlertController(title: "Message".localized, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK".localized, style: UIAlertAction.Style.default, handler: {action in
            
        }))
        
        uiRef.present(alert, animated: true, completion: nil)
        //End Alert Message Code
    }
    
  //MARK: Delay func

  static func delay(_ delay:Double, closure:@escaping ()->()) {
      DispatchQueue.main.asyncAfter(
          deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
    GIFHUD.shared.dismiss()
  }
    
}
extension UITextField {
  open override func awakeFromNib() {
    super.awakeFromNib()
    
    let lang = UserDefaults.standard.string(forKey: "language") ?? ""

//    textAlignment = .left

    if lang == "ar" {
        if textAlignment == .natural {
            self.textAlignment = .right
            self.layer.sublayerTransform = CATransform3DMakeTranslation(-10, 0, 0);
        }
    }
    else{
        if textAlignment == .right {
            self.textAlignment = .natural
        }
    }
}
}

//public extension UITextField
//{
//    // ⚠️ Prefer english keyboards
//    //
//    override var textInputMode: UITextInputMode?
//    {
//
//        var lang = UserDefaults.standard.string(forKey: "language") ?? ""
//
//        if lang == ""{
//            lang = "en"
//        }
//
//        let locale = Locale(identifier: lang) // your preferred locale
//
//        return
//            UITextInputMode.activeInputModes.first(where: { $0.primaryLanguage == locale.languageCode })
//            ??
//            super.textInputMode
//    }
//}

